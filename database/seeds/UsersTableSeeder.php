<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
            [
                'name'      => 'Administrador',
                'email'     => 'admin@ajatoencomendas',
                'password'  => bcrypt('@jato59#$23'),
            ],
            [
                'name'      => 'Maynara de Lima Monteiro',
                'email'     => 'maynara@ajatoencomendas',
                'password'  => bcrypt('@jato82#$46'),
            ],
            [
                'name'      => 'Inglid Daiane',
                'email'     => 'inglid@ajatoencomendas',
                'password'  => bcrypt('@jato71#$39'),
            ]
        ];

        foreach ($usuarios as $key => $usuario) {
            $validador = User::whereEmail($usuario['email'])->count();

            if (!$validador) {
                User::create($usuario);
            }
        }
    }
}
