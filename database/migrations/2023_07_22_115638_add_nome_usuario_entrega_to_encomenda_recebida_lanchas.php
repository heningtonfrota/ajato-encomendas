<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNomeUsuarioEntregaToEncomendaRecebidaLanchas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('encomenda_recebida_lanchas', function (Blueprint $table) {
            $table->string('nome_usuario_entrega')->nullable()->after('data_entrega');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('encomenda_recebida_lanchas', function (Blueprint $table) {
            $table->dropColumn('nome_usuario_entrega');
        });
    }
}
