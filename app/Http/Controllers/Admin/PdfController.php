<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lancha;
use App\Models\Encomenda;
use Illuminate\Http\Request;
use App\Models\ResetEncomendas;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PdfController extends Controller
{
  public function todasAsLanchas(Request $req)
  {
    $form_data = $req->data;
    $form_lancha = 0;
    $user = Auth()->User();
    $encomenda = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->get();
    $reset_encomendas = ResetEncomendas::orderBy('reset_id', 'DESC')->where('data', $form_data)->get();
    $all_encomendas = $encomenda->mergeRecursive($reset_encomendas);
    $encomenda_total = $all_encomendas->sum('valor');
    $encomenda_pago = $all_encomendas->where('status', "Pago")->sum('valor');
    $encomenda_receber  = $all_encomendas->where('status', "FPG")->sum('valor');
    $ajato10  = $all_encomendas->sum('valor') * 0.1;
    $canoeiro = $all_encomendas->where('status', "Pago")->sum('valor') - $ajato10;
    $relatorio_data = \App\Models\Relatorio::where('data', $form_data)->value('data');
    $relatorio_nome = \App\Models\Relatorio::where('nome', 'Todas as Lanchas')->where('data', $form_data)->value('nome');

    if ($canoeiro < 0) {
        $canoeiro = $encomenda_receber - $ajato10;
    }
    if ($relatorio_data == $form_data) {
        DB::table('relatorios')->where('data', $form_data)->where('nome', 'Todas as Lanchas')->update([
            'nome' => 'Todas as Lanchas',
            'id_lancha' => NULL,
            'total_diario'  => $encomenda_total,
            'total_pago' => $encomenda_pago,
            'total_receber' => $encomenda_receber,
            'porcentagem' => $ajato10,
            'embarcacao' => $canoeiro,
            'data' => $form_data
        ]);
    } else {
        DB::table('relatorios')->insert([
            'nome' => 'Todas as Lanchas',
            'id_lancha' => NULL,
            'total_diario'  => $encomenda_total,
            'total_pago' => $encomenda_pago,
            'total_receber' => $encomenda_receber,
            'porcentagem' => $ajato10,
            'embarcacao' => $canoeiro,
            'data' => $form_data
        ]);
    }


    $form_lista_despesas = [];
    $total_lista_despesas = null;
    $total_lista_despesas_final = null;


    return \PDF::loadView(
        'sistema.pdf.modelo',
        compact(
            'user',
            'encomenda',
            'encomenda_total',
            'encomenda_pago',
            'encomenda_receber',
            'ajato10',
            'canoeiro',
            'form_data',
            'relatorio_nome',
            'form_lancha',
            'form_lista_despesas',
            'total_lista_despesas'
            )
        )
      ->setPaper('a4', 'landscape')
      ->download($form_data . '-relatorio-todas-as-lanchas-gerado.pdf');
  }

  public function relatorioIndividual(Request $req)
  {
    $form_data = $req->data;
    $form_lancha = $req->lancha;
    $form_lista_despesas = $req->listaDespesa;
    $user = Auth()->User();
    $lancha = Lancha::where('status', 'A')->get();
    $encomenda = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
    $reset_encomendas = ResetEncomendas::orderBy('reset_id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
    $all_encomendas = $encomenda->mergeRecursive($reset_encomendas);
    $encomenda_total = $all_encomendas->sum('valor');
    $encomenda_pago = $all_encomendas->where('status', "Pago")->sum('valor');
    $encomenda_receber  = $all_encomendas->where('status', "FPG")->sum('valor');
    $ajato10  = $all_encomendas->sum('valor') * 0.1;
    $canoeiro = $all_encomendas->where('status', "Pago")->sum('valor') - $ajato10;
    $nome_lancha = Lancha::where('id', $form_lancha)->value('nome');
    $relatorio_data = \App\Models\Relatorio::where('data', $form_data)->value('data');
    $relatorio_nome = \App\Models\Relatorio::where('id_lancha', $form_lancha)->where('data', $form_data)->value('nome');

    if ($form_lancha == 1) {
        $ajato10 = $encomenda_total * 0.05;
        $canoeiro = $encomenda_pago - $ajato10;
    }
    if ($canoeiro < 0) {
        $canoeiro = $encomenda_receber - $ajato10;
    }
    if (isset($relatorio_nome) && $relatorio_data == $form_data) {
        $tabela_registro_relatorio = DB::table('relatorios')
            ->where('data', $form_data)
            ->where('id_lancha', $form_lancha);

        $relatorio_id = $tabela_registro_relatorio->first()->id;

        $tabela_registro_relatorio->update([
                'nome' => $nome_lancha,
                'id_lancha' => $form_lancha,
                'total_diario'  => $encomenda_total,
                'total_pago' => $encomenda_pago,
                'total_receber' => $encomenda_receber,
                'porcentagem' => $ajato10,
                'embarcacao' => $canoeiro,
                'data' => $form_data
            ]);
    } else {
        $relatorio_id = DB::table('relatorios')
            ->insertGetId([
                'nome' => $nome_lancha,
                'id_lancha' => $form_lancha,
                'total_diario'  => $encomenda_total,
                'total_pago' => $encomenda_pago,
                'total_receber' => $encomenda_receber,
                'porcentagem' => $ajato10,
                'embarcacao' => $canoeiro,
                'data' => $form_data
            ]);
    }

    $total_lista_despesas = null;

    if ($form_lista_despesas) {
        foreach ($form_lista_despesas as $key => $value) {
            $item = (object)$value;
            $valor = floatval(str_replace(",", ".", str_replace(".", "", $item->valor)));
            $total_lista_despesas += $valor;

            DB::table('lista_despesas')
                ->updateOrInsert(
                    ['id' => $item->id],
                    [
                        'relatorio_id' => $relatorio_id,
                        'valor' => (double) $item->valor,
                        'descricao' => $item->descricao,
                    ]
                );
        }
    }

    $total_lista_despesas_final = ($canoeiro - $total_lista_despesas);

    return \PDF::loadView(
        'sistema.pdf.modelo',
        compact(
            'user',
            'encomenda',
            'lancha',
            'encomenda_total',
            'encomenda_pago',
            'encomenda_receber',
            'ajato10',
            'canoeiro',
            'form_lista_despesas',
            'form_data',
            'form_lancha',
            'nome_lancha',
            'total_lista_despesas',
            'total_lista_despesas_final'
            )
        )
      ->setPaper('a4', 'landscape')
      ->setWarnings(false)
      ->stream();
  }

  public function imprimirDestinosPdf(Request $req)
  {
    $user = Auth()->User();
    $lancha = Lancha::where('status', 'A')->get();
    $municipio = \App\Models\Municipio::where('status', 'A')->get();
    $form_data  = $req->data;
    $form_lancha = $req->lancha;
    $form_municipio  = $req->municipio;
    $encomenda_lancha = Encomenda::orderBy('id', 'DESC')->where('lancha_id', $form_lancha)->value('lancha_id');
    $encomenda_data  = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->value('data');
    $encomenda_municipio  = Encomenda::orderBy('id', 'DESC')->where('municipio_id', $form_municipio)->value('municipio_id');

    $reset_lancha = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('lancha_id');
    $reset_data  = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('data');
    $reset_municipio  = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('municipio_id');

    if ($form_municipio == $encomenda_municipio || $form_municipio == $reset_municipio) {
        $encomenda  = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->get();
        $reset = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->get();
        $all_encomenda = $encomenda->mergeRecursive($reset);
        $mensagem = false;
    } else {
        $all_encomenda = null;
        $encomenda = NULL;
        $mensagem = true;
    }

    $ref_lancha = Lancha::where('id', $form_lancha)->value('nome');
    $total_fpg = $all_encomenda;

    if ($total_fpg) {
        $total_resultado_fpg = collect($total_fpg)->where('status', 'FPG')->sum('valor');
    } else {
        $total_resultado_fpg = null;
    }

    return  \PDF::loadView('sistema.pdf.imprimir', compact('total_resultado_fpg', 'ref_lancha', 'form_data', 'all_encomenda'))->setPaper('a4', 'landscape')->stream('imprimir-destinos.pdf');

  }

  public function imprimirTodosDestinosPdf(Request $req)
  {
    $user = Auth()->User();
    $lancha = Lancha::where('status', 'A')->get();
    $form_data = $req->data;
    $form_lancha = $req->lancha;
    $encomenda_lancha = Encomenda::orderBy('id', 'DESC')->where('lancha_id', $form_lancha)->value('lancha_id');
    $encomenda_data  = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->value('data');

    $reset_lancha = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->value('lancha_id');
    $reset_data  = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->value('data');
    $reset_municipio  = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->value('municipio_id');

    if ($form_data == $encomenda_data) {
        $encomenda = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
        $reset = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
        $all_encomenda = $encomenda->mergeRecursive($reset);
        $mensagem = false;
    } else {
        $all_encomenda = null;
        $encomenda = NULL;
        $mensagem = true;
    }
    $ref_lancha = Lancha::where('id', $form_lancha)->value('nome');

    $total_fpg = $all_encomenda;

    if ($total_fpg) {
        $total_resultado_fpg = collect($total_fpg)->where('status', 'FPG')->sum('valor');
    } else {
        $total_resultado_fpg = null;
    }

    return  \PDF::loadView(
        'sistema.pdf.imprimir',
        compact(
            'total_resultado_fpg',
            'ref_lancha',
            'form_data',
            'all_encomenda'
        )
    )
    ->setPaper('a4', 'landscape')
    ->stream('imprimir-todos-destinos.pdf');
  }
}
