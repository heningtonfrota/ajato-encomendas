<?php

namespace App\Http\Controllers\Admin\Relatorio;

use App\Http\Controllers\Controller;
use App\Models\Inertia\Despesa;
use App\Models\Lancha;
use App\Models\PorLancha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class PorLanchaController extends Controller
{
    private $lanchas;

    public function __construct()
    {
        $this->lanchas = Lancha::get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($relatorio_atual = [], $lista_atual = [])
    {
        $dados = DB::table('por_lanchas as pl')
            ->join('lanchas as l', 'l.id', 'pl.lancha_id')
            ->leftJoin('despesas as d', 'd.por_lancha_id', 'pl.id')
            ->selectRaw("
                pl.id, 
                pl.lancha_id, 
                pl.porcentagem_ajato, 
                pl.porcentagem_lancha, 
                pl.valor_total, 
                pl.valor_pago, 
                pl.valor_fpg, 
                pl.data,
                l.nome as nome_lancha,
                coalesce (pl.porcentagem_lancha - sum(d.valor), pl.porcentagem_lancha) as porcentagem_lancha_despesas
            ")
            ->groupByRaw('
                pl.id, 
                pl.lancha_id, 
                pl.porcentagem_ajato, 
                pl.porcentagem_lancha, 
                pl.valor_total, 
                pl.valor_pago, 
                pl.valor_fpg, 
                pl.data,
                l.nome
            ')
            ->orderByDesc('pl.data')
            ->get();

        return Inertia::render('Relatorio/PorLancha/Index', [
            'usuario' => auth()->user(),
            'lanchas' => $this->lanchas,
            'relatorios_lanchas' => $dados,
            'relatorio_atual' => $relatorio_atual,
            'lista_atual' => $lista_atual
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = "
            select 
                q3.*,
                (q3.valor_pago - q3.porcentagem_ajato) as porcentagem_lancha
            from (
                select 
                    l.nome,
                    q2.*, 
                    (q2.valor_total * (case when q2.lancha_id = 1 then 5 else 10 end) / 100) as porcentagem_ajato
                from (
                    select 
                        q1.lancha_id,
                        q1.`data`,
                        sum(q1.valor) as valor_total,
                        sum(case when q1.status = 'FPG' then q1.valor else null end) as valor_fpg,
                        coalesce(sum(case when q1.status = 'Pago' then q1.valor else null end), 0) as valor_pago 
                    from (
                        select
                            e.lancha_id,
                            e.`data`,
                            e.valor,
                            e.status,
                            'E' as tipo
                        from encomendas e
                        where e.lancha_id = {$request->lancha_id} and e.`data` = '{$request->data}'
                        union all
                        select
                            re.lancha_id,
                            re.`data`,
                            re.valor,
                            re.status,
                            'RE' as tipo
                        from reset_encomendas re
                        where re.lancha_id = {$request->lancha_id} and re.`data` = '{$request->data}'
                    ) as q1
                    group by 
                        lancha_id, 
                        `data`
                ) as q2
                join lanchas l on l.id = q2.lancha_id
            ) as q3
        ";

        $relatorio_atual = DB::selectOne($query);

        if (!$relatorio_atual) {
            return Redirect::back()->withErrors(['errors' => 'Combinação de lancha e data não existe!'])->withInput();
        }

        PorLancha::updateOrCreate(
            [
                'lancha_id' => $relatorio_atual->lancha_id,
                'data' => $relatorio_atual->data
            ],
            [
                'valor_total' => $relatorio_atual->valor_total,
                'valor_fpg' => $relatorio_atual->valor_fpg,
                'valor_pago' => $relatorio_atual->valor_pago,
                'porcentagem_ajato' => $relatorio_atual->porcentagem_ajato,
                'porcentagem_lancha' => $relatorio_atual->porcentagem_lancha
            ]
        );

        return $this->index($relatorio_atual);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function gerarRelatorio($id)
    {
        $totais = PorLancha::find($id);
        $lancha = Lancha::where('id', $totais->lancha_id)->first();
        $lista_despesas = Despesa::where('por_lancha_id', $id)->get();
        $total_lista_despesas = DB::table('despesas AS d')
            ->wherePorLanchaId($id)
            ->selectRaw("sum(d.valor) as total_lista")
            ->first()
            ->total_lista;
        $total_lista_despesas_final = ($totais->porcentagem_lancha - $total_lista_despesas);

        return \PDF::loadView('relatorios.pdf.por_lancha.index', [
            'user' => auth()->user(),
            'lancha' => $lancha,
            'totais' => $totais,
            'lista_despesas' => $lista_despesas,
            'total_lista_despesas' => $total_lista_despesas,
            'total_lista_despesas_final' => $total_lista_despesas_final
        ])
        ->setPaper('a4', 'landscape')
        ->setWarnings(false)
        ->stream();
    }
}
