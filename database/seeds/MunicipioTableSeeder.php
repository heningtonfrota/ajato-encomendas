<?php

use Illuminate\Database\Seeder;
use App\Models\Municipio;

class MunicipioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Municipio::create([
            'nome'   => 'MANAUS',
        ]);

        Municipio::create([
            'nome'   => 'CODAJÁS',
        ]);

        Municipio::create([
            'nome'   => 'COARI',
        ]);

        Municipio::create([
            'nome'   => 'TEFÉ',
        ]);

        Municipio::create([
            'nome'   => 'JURUÁ',
        ]);

        Municipio::create([
            'nome'   => 'CARAUARI',
        ]);

        Municipio::create([
            'nome'   => 'NOVA OLINDA',
        ]);

        Municipio::create([
            'nome'   => 'BORBA',
        ]);

        Municipio::create([
            'nome'   => 'NOVO ARIPUANÃ',
        ]);

        Municipio::create([
            'nome'   => 'MANICORÉ',
        ]);

        Municipio::create([
            'nome'   => 'PARINTINS',
        ]);

        Municipio::create([
            'nome'   => 'JURUTI',
        ]);

        Municipio::create([
            'nome'   => 'ORIXIMINÁ',
        ]);

        Municipio::create([
            'nome'   => 'FONTE BOA',
        ]);

        Municipio::create([
            'nome'   => 'JUTAÍ',
        ]);

        Municipio::create([
            'nome'   => 'TONANTINS',
        ]);

        Municipio::create([
            'nome'   => 'SANTO ANTONIO DO IÇA',
        ]);

        Municipio::create([
            'nome'   => 'AMATURÁ',
        ]);

        Municipio::create([
            'nome'   => 'SÃO PAULO DE OLIVENÇA',
        ]);

        Municipio::create([
            'nome'   => 'BENJAMIN CONSTANT',
        ]);

        Municipio::create([
            'nome'   => 'TABATINGA',
        ]);

        Municipio::create([
            'nome'   => 'ALVARÃES',
        ]);

        Municipio::create([
            'nome'   => 'UARINI',
        ]);

        Municipio::create([
            'nome'   => 'MARAÃ',
        ]);

        Municipio::create([
            'nome'   => 'JAPURÁ',
        ]);
    }
}
