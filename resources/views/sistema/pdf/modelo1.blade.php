<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    @include('sistema.layout.head')
  </head>
  <body>
    <div class="container col-sm-10">
      <div class="row">
        <div class="col-sm-4">
            <img src="{{ asset('img/logo.png')}}" class="rounded" style="width: 100px;">
        </div>

        <div class="col-sm-8 mx-auto">
          <h1 class="text-center">Relatorio: {{$relatorio_nome}}</h1>
          <h1 class="text-center">Viagem Dia: {{\Carbon\Carbon::parse( $form_data )->format('d/m/Y')}}</h1>
        </div>
      </div>
    </div>
    <div class="container col-sm-10">
      <table class="table table-responsive-sm table-sm table-bordered table-striped">
        <thead class="table-active">
          <tr class="thead-dark text-center">
            <th>Valor Total</th>
            <th>A Receber (FPG)</th>
            <th>Pago</th>
            <th>Ajato 10%</th>
            <th>Valor da Embarcação</th>
          </tr>
        </thead>
        <tbody>
          <tr class="text-center">
            <td>R$ {{ number_format( $encomenda_total, 2) }}</td>
            <td>R$ {{ number_format( $encomenda_receber, 2) }}</td>
            <td>R$ {{ number_format( $encomenda_pago,    2) }}</td>
            <td>R$ {{ number_format( $ajato10,      2) }}</td>
            <td>R$ {{ number_format( $canoeiro,     2) }}</td>
          </tr>
        </tbody>
      </table>

      <h5>Relatorio Emitido Por: {{Auth::user()->name}}</h5>

      <hr>

      @if(isset($form_lancha))
        <div class="d-flex justify-content-center py-2">
          <h3 class="text-center text-uppercase">Relatorio referente as Encomendas da Lancha: {{$lancha->nome}} na Data: {{ \Carbon\Carbon::parse( $form_data )->format('d/m/Y') }}</h3>
        </div>
      @else
        <div class="d-flex justify-content-center py-2">
          <h3 class="text-center text-uppercase">Relatorio referente as Encomendas de Todas as Lanchas na Data: {{ \Carbon\Carbon::parse( $form_data )->format('d/m/Y') }}</h3>
        </div>
      @endif
    </div>
  </body>
</html>
