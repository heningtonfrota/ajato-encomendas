 <!DOCTYPE html>
<html lang="pt-br" dir="ltr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="utf-8">

        <link rel="stylesheet" href="css/cupom.css">
        <link rel="sortcut icon" href="img/ajato_mini.png" type="image/x-icon" />

        <title>Etiqueta nº {{$encomenda->reset_id ? $encomenda->reset_id : $encomenda->id}}</title>
    </head>
    <body>
        @php
            $lancha = $encomenda->find($encomenda->id)->relLancha;
            $municipio = $encomenda->find($encomenda->id)->relMunicipio;
        @endphp
        <!-- INFORMÇÕES DO TOPO DO CUPOM -->
        <div class="box" id="cabecalho">
            <img src="img/ajato_etiqueta.png" alt="">
            <h1>AJATO ENCOMENDAS</h1>
            <h1>CNPJ: 40.105.601/0001-83</h1>
            <!--<h1>CNPJ: XX.XXX.XXX/XXXX-XX</h1>-->
            <h1>FONE: (92) 99434-1616</h1>
        </div>
        <div id="info-cadastro">
            <h1>CADASTRO DE ENCOMENDAS</h1>
        </div>
        <div id="info-dest-n">
            <h1>ENCOMENDA NÚMERO: {{$encomenda->reset_id ? $encomenda->reset_id : $encomenda->id}}<br>DEST: {{$encomenda->destinatario}}</h1>
        </div>
        <div id="info">
            <h2>LANCHA: {{$lancha->nome}}</h2>
            <h2>SAÍDA DA LANCHA: {{\Carbon\Carbon::parse($encomenda->data)->format('d/m/Y')}}</h2>
            <h2>DESTINO: {{$municipio->nome}}</h2>
            <h2>DESCRIÇÃO: {{$encomenda->descricao}}</h2>
            <h2>REMETENTE: {{$encomenda->remetente}}</h2>
            <h2>CADASTRO: {{\Carbon\Carbon::parse( $encomenda->data_criacao ?? $encomenda->created_at )->format('d/m/Y H:i:s')}}</h2>
            <h2>SETOR DE ENCOMENDAS - TERMINAL AJATO</h2>
            <h3>REGISTRADO POR: {{$encomenda->relUser->name ?? ''}}</h3>
        </div>
        <div id="info-valor-status">
            <h1>VALOR: R$ {{ number_format( $encomenda->valor, 2) }}</h1>
            <h1>STATUS: {{$encomenda->status}}</h1>
        </div>
        <div id="rodape">
            <h1>EMISSÃO: {{\Carbon\Carbon::parse( $diaHora )->format('d/m/Y')}} HORA: {{\Carbon\Carbon::parse( $diaHora )->format('H:i:s')}}</h1>
            <h1>* Taxa de cancelamento/retirada de encomenda após a postagem é de R$ 10,00 reais por volume.</h1>
            <h1>* Após o prazo de 30 dias corridos a contar da data de emissão desse comprovante, encomendas esquecidas serão descartadas após esse período. Sem aviso prévio</h1>
            <h1>Desenvolva seu sistema: contato@codenoxus.com</h1>
        </div>
    </body>
</html>
