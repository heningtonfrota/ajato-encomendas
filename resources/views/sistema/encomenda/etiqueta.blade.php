<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <link rel="stylesheet" href="css/estilo.css">
    <link rel="sortcut icon" href="img/ajato_mini.png" type="image/x-icon" />

    <title>Etiqueta nº {{$encomenda->id}}</title>
  </head>
  <body>
      @php
          $lancha = $encomenda->find($encomenda->id)->relLancha;
          $municipio = $encomenda->find($encomenda->id)->relMunicipio;
      @endphp
      <div class="clearfix box0">
          <img src="img/ajato_etiqueta.png" alt="">
      </div>
      <!-- INFORMÇÕES DO TOPO DA ETIQUETA -->
      <div class="clearfix">
        @if ($encomenda->lancha_id == 0)
          <div class="box letter-4" id="agatha-fernanda">
            <h1 class="em4-2">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif ($encomenda->lancha_id == 1)
          <div class="box letter-4 line-height" id="ajato-2000">
            <h1 class="em5-7">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 2)
          <div class="box letter-4 line-height" id="crystal">
            <h1 class="em6-3">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 3)
          <div class="box letter-4 line-height" id="gloria-de-deus-3">
            <h1 class="em4-4">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 4)
          <div class="box letter-4 line-height" id="madame-cris">
            <h1 class="em5-1">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 5)
          <div class="box letter-4 line-height" id="missone">
            <h1 class="em6-4">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 6)
          <div class="box letter-4 line-height" id="oriximina">
            <h1 class="em6">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 7)
          <div class="box letter-4 line-height" id="soberana">
            <h1 class="em4-7">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 8)
          <div class="box letter-4 line-height" id="tais-holanda">
            <h1 class="em5">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h4 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h4>
          </div>
        @elseif($encomenda->lancha_id == 9)
          <div class="box letter-4 line-height" id="ze-holanda">
            <h1 class="em5-4">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h4 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h4>
          </div>
        @elseif($encomenda->lancha_id == 10)
          <div class="box letter-4 line-height" id="lima-de-abreu">
            <h1 class="em4-9">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h4 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h4>
          </div>
        @elseif($encomenda->lancha_id == 11)
          <div class="box letter-4 line-height" id="belissima">
            <h1 class="em5-9">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 12)
          <div class="box letter-4 line-height" id="perola">
            <h1 class="em6-6">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 13)
          <div class="box letter-4 line-height" id="noiva-2">
            <h1 class="em6-2">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 14)
          <div class="box letter-4 line-height" id="esmeralda">
            <h1 class="em5-4">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 15)
          <div class="box letter-4 line-height" id="diamante">
            <h1 class="em6">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
        @elseif($encomenda->lancha_id == 16)
          <div class="box letter-4 line-height" id="puma">
            <h1 class="em7-6">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h1 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h1>
          </div>
          @elseif($encomenda->lancha_id == 17)
          <div class="box letter-4 line-height" id="hellen-soares">
            <h1 class="em4-7">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h4 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h4>
          </div>
          @elseif($encomenda->lancha_id == 18)
          <div class="box letter-4 line-height" id="gloria-de-deus-4">
            <h1 class="em4-4">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h4 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h4>
          </div>
           @elseif($encomenda->lancha_id == 19)
          <div class="box letter-4 line-height" id="joao-pedro">
            <h1 class="em5-4">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h4 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h4>
          </div>
          @elseif($encomenda->lancha_id == 20)
          <div class="box letter-4 line-height" id="sasha-maria">
            <h1 class="em5-2">LANCHA: {{$lancha->nome}}</h1>
            <h1 class="destinatario">DEST: {{$encomenda->destinatario}}</h1>
            <h4 class="data">DATA: {{\Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y')}}</h4>
          </div>
        @endif
      </div>
      <!-- INFORMAÇÕES DO CONTEUDO DA ETIQUETA -->
      <div class="clearfix">
        <div class="box2 borda">
          <div>
            <h5 class="txt-up">nº encomenda</h5>
            <h1 id="encomenda" class="txt-up">{{$encomenda->id}}</h1>
          </div>
        </div>
        <div class="box3 borda">
          @if ($encomenda->status == "Pago")
            <div>
              <h5>Status</h5>
              <h3 id="pago" class="txt-up">{{$encomenda->status}}</h3>
            </div>
          @else
            <div>
              <h3 id="fpg" class="txt-up">{{$encomenda->status}}</h3>
              <h3 id="valor" class="txt-up">R$ {{ number_format( $encomenda->valor, 2) }}</h3>
            </div>
          @endif
        </div>
      </div>
      <div class="clearfix">
        <div class="box1 borda">
          <h5 class="txt-down">Volume</h5>
          <h1 id="volume" class="txt-down">{{$encomenda->volume}}</h1>
        </div>
        <div class="box4 borda">
          <h5 class="txt-down">cidade</h5>
          <h4 id="cidade" class="txt-down">{{$municipio->nome}}</h4>
        </div>
      </div>
  </body>
