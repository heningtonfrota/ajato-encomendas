<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    @include('sistema.layout.head')
    <style>
      table{
          margin: 0px;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <img src="{{ asset('img/ajato_laranja.png')}}" class="rounded mx-auto d-block" style="height: 80px;">
        </div>

        <div class="col">
          <h3 class="text-center">{{$ref_lancha}}</h3>
          <h3 class="text-center">Viagem Dia: {{\Carbon\Carbon::parse( $form_data )->format('d/m/Y')}}</h3>
        </div>


      </div>
      <table class="table table-sm table-bordered text-center mt-4" style="font-size: 11pt;">
          <thead class="table-active">
              <tr>
                  <th class="align-middle">ID</th>
                  <th class="align-middle">Descrição</th>
                  <th class="align-middle">Remetente</th>
                  <th class="align-middle">Destinatário</th>
                  <th class="align-middle">Destino</th>
                  <th class="align-middle">Valor</th>
                  <th class="align-middle">Status</th>
              </tr>
          </thead>
          <tbody>
          @foreach($all_encomenda as $encomendas)
              @php
                  $lancha     = $encomendas->find($encomendas->id)->relLancha;
                  $municipio = $encomendas->find($encomendas->id)->relMunicipio;
              @endphp
              <tr>
                  <td class="align-middle">{{ $encomendas->reset_id ? $encomendas->reset_id : $encomendas->id }}</td>
                  <td class="align-middle">{{ $encomendas->descricao }}</td>
                  <td class="align-middle">{{ $encomendas->remetente }}</td>
                  <td class="align-middle">{{ $encomendas->destinatario }}</td>
                  <td class="align-middle">{{ $municipio->nome }}</td>
                  <td class="align-middle">R${{ number_format( $encomendas->valor, 2) }}</td>
                  <td class="align-middle">{{ $encomendas->status }}</td>
              </tr>
          @endforeach

          </tbody>
      </table>
      <h4>Relatório Emitido Por: {{Auth::user()->name}}</h4>
        @if (isset($total_resultado_fpg))
      <h4>Total FPG:
            R$ {{number_format($total_resultado_fpg, 2) }}
      </h4>
        @endif
    </div>
  </body>
</html>
