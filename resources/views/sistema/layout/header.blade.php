<div class="p-0">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" style="margin: 0px; width: 60px;" href="{{route('sistema.home')}}">
      <img src="{{asset('img/ajato_mini.png')}}" class="rounded img-fluid" alt="Ajato Encomendas">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Alterna navegação">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="{{route('encomendas_recebidas_lancha.index')}}"><i class="fas fa-crown"></i> Versão 2.0</a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link" href="{{route('sistema.home')}}"><i class="fas fa-home"></i> Home</a>
        </li> --}}
        <li class="nav-item">
          <a class="nav-link" href="{{route('sistema.encomenda.cadastro')}}"><i class="fas fa-plus-circle"></i> Cadastro</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('sistema.encomenda.lista')}}"><i class="fas fa-clipboard-list"></i> Lista</a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link" href="{{route('sistema.lancha.index')}}"><i class="fas fa-ship"></i> Lancha</a>
        </li> --}}
        <li class="nav-item">
          <a class="nav-link" href="{{route('sistema.relatorio.index')}}"><i class="fas fa-hand-holding-usd"></i> Relatório</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('sistema.relatorio.historico')}}"><i class="fas fa-history"></i> Historico de Relatórios Gerados</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('sistema.encomendas_deletadas.index')}}"><i class="fas fa-trash"></i> Deletados</a>
        </li>
      </ul>

      @if ($user->is_admin == 'S')
        <div class="btn-group mr-2">
          <button type="button" style="background-color:rgb(144, 179, 255);" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-gear"></i> Configurações
          </button>

          <div class="dropdown-menu">
            <a class="dropdown-item text-left" href="{{ route('sistema.configuracoes.usuarios') }}">
              <i class="fas fa-users"></i>
              Usuários
            </a>
            <a class="dropdown-item text-left" href="{{ route('sistema.configuracoes.lanchas') }}">
              <i class="fas fa-ship"></i>
              Lanchas
            </a>
            <a class="dropdown-item text-left" href="{{ route('sistema.configuracoes.municipios') }}">
              <i class="fas fa-map"></i> 
              Municipios
            </a>
          </div>
        </div>
      @endif

      <div class="btn-group mr-2">
        <button type="button" style="background-color:red;" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-exclamation-circle"></i> Resets 
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item text-left" href="{{route('sistema.reset.reset-encomenda')}}" onclick="return confirm('Deseja Resetar Encomendas?');"><i class="fas fa-exclamation-triangle"></i> Resetar Encomendas</a>
          <a class="dropdown-item text-left" href="{{route('sistema.reset.index')}}"><i class="fas fa-list-ol"></i> Listar Resets</a>
        </div>
      </div>

      <div class="btn-group">
        <button type="button" style="background-color:orange;" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ $user->name }} 
        </button>

        <div class="dropdown-menu">
          <a class="dropdown-item text-center" href="{{route('sistema.sair')}}">
            <i class="fas fa-sign-out-alt"></i> 
            Sair
          </a>
        </div>
      </div>
    </div>
  </nav>
</div>
