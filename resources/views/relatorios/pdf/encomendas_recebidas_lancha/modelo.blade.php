<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
<head>
    @include('sistema.layout.head')
    <style>
        img {
            max-height: 60px;
        }
        .margin-titulo-tabela {
            margin-bottom: -40px;
        }
        @page{
            margin-top: 75px;
            margin-bottom: 75px;
        }
        header{
            position: fixed;
            left: 0px;
            right: 0px;
            height: 50px;
            margin-top: -60px;
        }
        table {
            margin: 0 !important;
            padding: 0 !important;
        }
        footer{
            position: fixed;
            left: 0px;
            right: 0px;
            height: 25;
            bottom: 0px;
            margin-bottom: -60px;
        }
        td {
            margin: 0 !important;
            padding: 1 3 !important;
            font-size: 8pt;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <header>
        <div class="d-flex flex-row">
            <img src="{{ asset('img/ajato_laranja.png')}}" class="img-fluid">
            <p class="text-center h2">LANCHA: {{$nome_lancha}} - Viagem: {{ $data_entrada }}</p>
        </div>
    </header>

    <footer>
        <div class="h6" style="width: 105%; margin-left: -30px">
            <div class="float-left pr-2 border-right">
                TOTAL ENTREGUE: <u>R$ {{ number_format($totais->total_entregue, 2, ',', '.') }}</u>
            </div>
            <div class="float-left px-2 border-right">
                AJATO(%): <u>R$ {{ number_format($totais->porcentagem_ajato, 2, ',', '.') }}</u>
            </div>
            <div class="float-left pl-2">
                LANCHA(%): <u>R$ {{ number_format($totais->diferenca, 2, ',', '.') }}</u>
            </div>
    
            <div class="float-right">ASSINATURA: ______________________</div>
            <div class="float-right pr-1">DATA: __ /__ /____</div>
            <div style="clear: both;"></div>
        </div>
    </footer>
    
    <main>    
        <table class="table table-sm table-bordered">
            <thead class="thead-light">
                <tr class="text-center">
                    <th>ID</th>
                    <th>Descrição</th>
                    <th>Destinatario</th>
                    <th>Origem</th>
                    <th>Valor</th>
                    <th>Entregue</th>
                    <th>Status</th>
                    <th>Recebedor</th>
                    <th>Contato</th>                   
                    <th>Criado Por</th>                
                </tr>
            </thead>
            <tbody>
                @foreach($dados as $dado)
                    <tr class="">
                        <td class="text-center">{{ $dado->id }}</td>
                        <td class="text-center">{{ $dado->descricao }}</td>
                        <td class="text-center">{{ $dado->destinatario }}</td>
                        <td class="text-center">{{ $dado->nome_municipio }}</td>
                        <td class="text-right">R$ {{ number_format($dado->valor, 2) }}</td>
                        <td class="text-center">{{ $dado->entregue_formatado }}</td>
                        <td class="text-center">{{ $dado->status }}</td>
                        <td class="text-center">{{ $dado->recebedor_nome }}</td>
                        <td class="text-center">{{ $dado->recebedor_contato }}</td>
                        <td class="text-center">{{ $dado->nome_usuario }}</td>
                    </tr>
              @endforeach
            </tbody>
        </table>
    </main>
</body>
</html>
