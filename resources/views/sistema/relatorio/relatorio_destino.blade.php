@extends('sistema.layout.index')

@section('titulo', 'Relatorio')

@section('conteudo')
  <div class="card container col-sm-8">
      <h1 class="card-header text-center">Encomendas Por Destino</h1>

      <form class="pt-2" method="post" action="{{route('relatorio.relatorio_destino_view')}}">
        @csrf
        <div class="form-row">
          <div class="col-sm-3 mt-2">
            <input type="date" name="data" class="form-control" placeholder="Data">
          </div>
          <div class="col-sm-4 mt-2">
            <select class="form-control" name="lancha" id="validationCustom08" required>
                <option selected disabled value="">Selecione a Lancha...</option>
                @foreach($lancha as $lanchas)
                    <option value="{{ $lanchas->id }}">{{ $lanchas->nome }}</option>
                @endforeach
            </select>
          </div>
          <div class="col-sm-4 mt-2">
            <select class="form-control" name="municipio" id="validationCustom08" required>
                <option selected disabled value="">Selecione o Destino...</option>
                @foreach($municipio as $municipios)
                    <option value="{{ $municipios->id }}">{{ $municipios->nome }}</option>
                @endforeach
            </select>
          </div>
          <div class="col-sm-1 text-center mt-2">
            <button class="btn btn-warning" type="submit"><i class="fas fa-search"></i></button>
          </div>

        </div>
      </form>

      @if(isset($all_encomenda))
        @foreach($all_encomenda as $encomendas)
            @php
                $lancha     = $encomendas->find($encomendas->id)->relLancha;
                $municipio  = $encomendas->find($encomendas->id)->relMunicipio;
            @endphp
        @endforeach
        <hr>
        <h4 class="text-center">Lancha: {{ $lancha->nome }}</h4>
        <h4 class="text-center">Data: {{ \Carbon\Carbon::parse( $form_data )->format('d/m/Y') }}</h4>
        <h4 class="text-center">Destino: {{ $municipio->nome }}</h4>
      @else
        <hr>
      @endif
      @if(isset($all_encomenda))
        <hr>
        <div class="table-responsive">
            <table class="table table-sm table-bordered table-striped table-dark">
                <thead>
                    <tr class="text-center">
                        <th scope="col">ID</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Remetente</th>
                        <th scope="col">Destinatário</th>
                        <th scope="col">Destino</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($all_encomenda as $encomendas)
                    @php
                        $lancha = $encomendas->find($encomendas->id)->relLancha;
                        $municipio = $encomendas->find($encomendas->id)->relMunicipio;
                    @endphp
                    <tr class="text-center">
                        <td>{{ $encomendas->reset_id ? $encomendas->reset_id : $encomendas->id }}</td>
                        <td>{{ $encomendas->descricao}}</td>
                        <td>{{ $encomendas->remetente }}</td>
                        <td>{{ $encomendas->destinatario }}</td>
                        <td>{{ $municipio->nome }}</td>
                        <td>{{ number_format( $encomendas->valor, 2) }}</td>
                        <td>{{ $encomendas->status }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
      @else
        @if(isset($mensagem))
          <h4 class="text-center bg-danger text-white p-2 mb-1">Sua seleção de filtros não contém resultados!</h4>
        @endif
          <h6 class="text-center"><i>Aguardando a seleção de "Data", "Lancha" e "Destino" no menu de pesquisa acima.</i></h6>
      @endif
      <hr>
      <ul class="nav justify-content-center py-2">
        <li><a href="{{ route('sistema.relatorio.index') }}" class="btn btn-danger btn-lg mx-4"><i class="fas fa-arrow-left"></i> Voltar</a></li>
        @if(isset($all_encomenda))
          <li>
              <form method="get" target="_blank" action="{{route('pdf.imprimir_destino_pdf')}}">
                @csrf
                <input type="hidden" name="data"        value="{{$form_data}}">
                <input type="hidden" name="lancha"      value="{{$form_lancha}}">
                <input type="hidden" name="municipio"   value="{{$form_municipio}}">
                <button type="submit" class="btn btn-info btn-lg mx-4 text-white">Imprimir PDF</button>
              </form>
          </li>
        @endif
      </ul>
    </div>

@endsection
