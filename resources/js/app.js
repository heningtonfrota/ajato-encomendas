require('./bootstrap');

import Vue from 'vue'
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue'
import vuetify from './plugins/vuetify'
import store from './plugins/vuex'
import axios from 'axios'
import { ZiggyVue } from 'ziggy'
import { Ziggy } from './ziggy'

Vue.use(ZiggyVue, Ziggy)

// axios.defaults.baseURL = 'http://ajato-encomendas.test:8080'
axios.defaults.baseURL = 'http://teste-ajato-encomendas.codenoxus.com'
axios.defaults.withCredentials = true
Vue.use(InertiaPlugin)

const app = document.getElementById('app')

new Vue({
  vuetify,
  store,
  render: (h) =>
    h(InertiaApp, {
      props: {
        initialPage: JSON.parse(app.dataset.page),
        resolveComponent: (name) => import(`./Pages/${name}`).then((module) => module.default),
      },
    }),
}).$mount(app)
