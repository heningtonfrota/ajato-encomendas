<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relatorio extends Model
{
    protected $fillable = ['total_diario', 'total_pago', 'total_receber', 'porcentagem', 'embarcacao', 'data'];
}
