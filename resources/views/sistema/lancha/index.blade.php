@extends('sistema.layout.index')

@section('titulo', 'Ajato Encomendas - Lanchas')

@section('conteudo')
  <div class="container">
    <h1 class="text-center">Lanchas</h1>

    <div class="d-flex justify-content-center">
      <table class="table table-sm table-striped table-dark col-sm-6">
        <thead class="thead-dark">
          <tr class="text-center">
            <th>Nome</th>
            <th colspan="2">Ação</th>
          </tr>
        </thead>
        <tbody class="border">
          <tr>
            <td><h4 class="card-title text-center">Ajato 2000</h4></td>
            <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.ajato2000')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.ajato2000')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Crystal</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.crystal')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.crystal')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Gloria de Deus III</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.gloria_de_deus')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.gloria_de_deus')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Madame Cris</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.madame_cris')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.madame_cris')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Missone</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.missone')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.missone')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Oriximiná</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.oriximina')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.oriximina')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Soberana</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.soberana')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.soberana')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Tais Holanda</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.tais_holanda')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.tais_holanda')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Ze Holanda</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.ze_holanda')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.ze_holanda')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Lima de Abreu</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.lima_de_abreu')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.lima_de_abreu')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Belíssima</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.belissima')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.belissima')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
          <tr>
            <td><h4 class="card-title text-center">Peróla</h4></td>
  	        <td colspan="2" class="text-center">
              <a href="{{route('sistema.lancha.perola')}}" class="btn btn-warning"><i class="far fa-clock"></i></a>
              <a href="{{route('sistema.lancha.encomenda.perola')}}" class="btn btn-warning"><i class="fas fa-list"></i></i></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <h6 class="text-center"><i><small>Obs: os botões de ação permitira a vizualização dos horários das lanchas e o histórico referente.</small></i></h6>
  </div>
@endsection
