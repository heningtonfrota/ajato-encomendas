<?php

use App\Models\Lancha;
use App\Models\Municipio;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEncomendaRecebidaLanchasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encomenda_recebida_lanchas', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Lancha::class);
            $table->date('data_entrada');
            $table->text('descricao');
            $table->string('destinatario');
            $table->foreignIdFor(Municipio::class)->nullable();
            $table->float('valor');
            $table->char('status', 3)->nullable();
            $table->boolean('entregue')->default(false);
            $table->string('recebedor_nome')->nullable();
            $table->string('recebedor_contato')->nullable();
            $table->boolean('pago_lancha')->default(false);
            $table->foreignIdFor(User::class);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encomenda_recebida_lanchas');
    }
}
