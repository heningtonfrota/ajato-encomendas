<?php

use App\Models\Lancha;
use App\Models\Municipio;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndividualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('por_destino_individual', function (Blueprint $table) {
            $table->id();
            $table->date('data');
            $table->foreignIdFor(Lancha::class);
            $table->foreignIdFor(Municipio::class);
            $table->foreignIdFor(User::class);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('por_destino_individual');
    }
}
