 <!DOCTYPE html>
 <html lang="pt-br" dir="ltr">

 <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <link rel="stylesheet" href="css/declaracao_conteudo.css">
    <link rel="sortcut icon" href="img/ajato_mini.png" type="image/x-icon" />

    <title>Etiqueta nº {{ $encomenda->reset_id ? $encomenda->reset_id : $encomenda->id }}</title>
 </head>

 <body>
    @php
        $lancha = $encomenda->find($encomenda->id)->relLancha;
        $municipio = $encomenda->find($encomenda->id)->relMunicipio;
    @endphp
    <!-- INFORMÇÕES DO TOPO DO CUPOM -->
    <div class="box" id="cabecalho">
        <img src="img/ajato_etiqueta.png" alt="">
        <h1>AJATO ENCOMENDAS</h1>
        <h1>CNPJ: 40.105.601/0001-83</h1>
        <h1>FONE: (92) 99434-1616</h1>
    </div>
    <div id="info-cadastro">
        <h1>DECLARAÇÃO DE CONTEÚDO</h1>
    </div>
    <div id="info">
        <h2>
            Eu <span class="identificador-informacoes">{{ $encomenda->remetente }}</span>,
            com o nº de CPF/CNPJ: <span class="identificador-informacoes">{{ $encomenda->cpf == 0 ? "Não Informado" : $encomenda->cpf }}</span>,
            estou enviando a encomenda nº: <span class="identificador-informacoes">{{ $encomenda->reset_id ? $encomenda->reset_id : $encomenda->id }}</span>
            na lancha: <span class="identificador-informacoes">{{ $lancha->nome }}</span>
            viagem dia: <span class="identificador-informacoes">{{ \Carbon\Carbon::parse($encomenda->data)->format('d/m/Y') }}</span>
            contendo os itens a seguir:
        </h2>
        <h2>
            {{ $encomenda->descricao }}
        </h2>
        <h3>
            Declaro, que sou o/a único (a) responsável por eventuais penalidades ou danos decorrentes de informações inverídicas.
        </h3>
    </div>
    <div id="info-valor-status">
        <h2 class="linha-assinatura"></h2>
        <p>Assinatura Remetente</p>
    </div>
    <div id="rodape">
        <h1>
            Registrador por: {{ $encomenda->relUser->name }}
        </h1>
        <h1>
            EMISSÃO: {{ \Carbon\Carbon::parse($diaHora)->format('d/m/Y') }}
            HORA: {{ \Carbon\Carbon::parse($diaHora)->format('H:i:s') }}
        </h1>
    </div>
 </body>

 </html>
