<?php

namespace App\Models\Inertia\PorDestino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Geral extends Model
{
    use HasFactory;

    protected $table = 'por_destino_geral';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
