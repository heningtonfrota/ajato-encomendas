<?php

use Illuminate\Database\Seeder;
use App\Models\Lancha;

class LanchaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lancha::create([
            'nome'   => 'AJATO 2000',
        ]);
        Lancha::create([
            'nome'   => 'CRYSTAL',
        ]);
        Lancha::create([
            'nome'   => 'GLORIA DE DEUS III',
        ]);
        Lancha::create([
            'nome'   => 'MADAME CRIS',
        ]);
        Lancha::create([
            'nome'   => 'MISSONE',
        ]);
        Lancha::create([
            'nome'   => 'ORIXIMINÁ',
        ]);
        Lancha::create([
            'nome'   => 'SOBERANA',
        ]);
        Lancha::create([
            'nome'   => 'TAIS HOLANDA',
        ]);
        Lancha::create([
            'nome'   => 'ZÉ HOLANDA',
        ]);
         Lancha::create([
            'nome'   => 'LIMA DE ABREU',
        ]);
          Lancha::create([
            'nome'   => 'BELISSIMA',
        ]);
           Lancha::create([
            'nome'   => 'PEROLA',
        ]);
	}
}
