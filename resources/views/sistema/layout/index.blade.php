<!DOCTYPE html>
<html lang="pt-br">

  @include('sistema.layout.head')

  <body>

    @include('sistema.layout.header')

    <div class="p-3">
      @if (session('message') || session('error'))
        <div class="bg-dark text-white position-fixed p-3 mb-2 mr-2 w-50" style="z-index: 5; right: 0; bottom: 0; border-radius: 10px;">
          <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="false" data-delay="2000">
            <div class="toast-header">
              <strong class="mr-auto">Erros Encontrados!</strong>
            </div>

            <hr>
            
            <div class="toast-body">
              <ul>
                @if (session('message'))
                  @foreach (session('message') as $item)
                    <li>{{ $item }}</li>
                  @endforeach
                @else
                  @foreach (session('error') as $item)
                    <li>{{ $item }}</li>
                  @endforeach
                @endif
              </ul>
            </div>
          </div>
        </div>
      @endif

      @yield('conteudo')
    </div>

    @include('sistema.layout.java')

    @include('sistema.layout.footer')

    @stack('js')

  </body>
</html>
