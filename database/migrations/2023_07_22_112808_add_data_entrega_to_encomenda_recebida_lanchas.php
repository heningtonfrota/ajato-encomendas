<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataEntregaToEncomendaRecebidaLanchas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('encomenda_recebida_lanchas', function (Blueprint $table) {
            $table->timestamp('data_entrega')->nullable()->after('entregue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('encomenda_recebida_lanchas', function (Blueprint $table) {
            $table->dropColumn('data_entrega');
        });
    }
}
