@extends('sistema.layout.index')

@section('titulo', 'Ajato Encomendas - Home')

@section('conteudo')
	<div class="container">
	    <div class="col">
	        <h1 class="text-center mb-0">Bem vindo(a) {{ $user->name }}!</h1>
	        <img class="rounded mx-auto d-block mt-0" src="{{ asset('img/logo.png') }}" width="50%">
	    </div>
	</div>
@endsection
