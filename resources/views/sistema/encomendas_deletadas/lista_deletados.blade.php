@extends('sistema.layout.index')

@section('titulo', 'Lista de Encomendas')

@section('conteudo')    
    <div class="container-fluid">
        <h1 class="text-center">Lista de Encomendas Deletadas</h1>

        <form action="{{route('encomenda_deletada.pesquisar')}}" method="post">
            @csrf
            <div class="form-row my-3">
              <div class="col-sm-2">
                <input type="text" class="form-control" name="id" placeholder="ID">
              </div>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="id_encomenda" placeholder="ID Encomenda">
              </div>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="destinatario" placeholder="Destinatario">
              </div>
              <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
            </div>
          </form>

        <div class="table-responsive">
            <table class="table table-sm table-bordered table-striped table-dark">
                <thead>
                    <tr class="text-center">
                        <th scope="col">ID</th>
                        <th scope="col">Deletada Em</th>
                        <th scope="col">Deletada Por</th>
                        <th scope="col">Nº</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Destinatario</th>
                        <th scope="col">Lancha</th>
                        <th scope="col">Destino</th>
                        <th scope="col">Status</th>
                        <th scope="col">Data</th>
                        <th scope="col" colspan="5">Ação</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($encomendas_deletadas as $encomenda)
                    @php
                        $lancha     = $encomenda->find($encomenda->id)->relLancha;
                        $municipio = $encomenda->find($encomenda->id)->relMunicipio;
                        $user_encomenda = $encomenda->find($encomenda->id)->relUserDelete;
                    @endphp
                    <tr class="text-center">
                        <td>{{ $encomenda->id }}</td>
                        <td>{{ \Carbon\Carbon::parse($encomenda->deleted_at)->format('d/m/Y H:i:s') }}</td>
                        <td>{{ $user_encomenda->name }}</td>
                        <td>{{ $encomenda->id_deletado }}</td>
                        <td>R$ {{ number_format( $encomenda->valor, 2) }}</td>
                        <td>{{ $encomenda->destinatario }}</td>
                        <td>{{ $lancha->nome }}</td>
                        <td>{{ $municipio->nome }}</td>
                        <td>{{ $encomenda->status }}</td>
                        <td>{{ \Carbon\Carbon::parse( $encomenda->data )->format('d/m/Y') }}</td>
                        <td>
                            <a
                                class="text-warning"
                                href="{{route('sistema.encomendas_deletadas.visualizar', $encomenda->id)}}"
                                title="Ver"
                            >
                                <i class="fas fa-eye"></i>
                            </a>
                        </td>

                        <td>
                            <a
                                class="text-success"
                                href="{{route('sistema.encomendas_deletadas.cupom', $encomenda->id)}}"
                                target="_blank"
                                title="Cupom"
                            >
                                <i class="fas fa-file"></i>
                            </a>
                        </td>

                        <td>
                            <a
                                class="text-info"
                                href="{{route('sistema.encomendas_deletadas.recibo', $encomenda->id)}}"
                                target="_blank"
                                title="Recibo"
                            >
                                <i class="fas fa-file-alt"></i>
                            </a>
                        </td>

                        <td>
                            <a
                                class="text-danger"
                                href="{{route('sistema.encomendas_deletadas.retornar_encomenda', $encomenda->id)}}"
                                title="Retornar Encomenda Deletada"
                            >
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

    <div class="d-flex justify-content-center">
        {{ $encomendas_deletadas->links() }}
    </div>

    <div class="d-flex justify-content-center">
        <a href="{{route('sistema.home')}}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar </a>
    </div>
@endsection
