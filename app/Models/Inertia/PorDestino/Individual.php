<?php

namespace App\Models\Inertia\PorDestino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Individual extends Model
{
    use HasFactory;

    protected $table = 'por_destino_individual';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
