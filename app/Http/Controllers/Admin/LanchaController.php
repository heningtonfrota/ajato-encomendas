<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Horario;
use App\Models\Encomenda;
use App\Models\Lancha;
use Inertia\Inertia;

class LanchaController extends Controller
{
    private $objHorario;
    private $objEncomenda;

    public function __construct()
    {
        $this->objHorario = new Horario();
        $this->objEncomenda = new Encomenda();
    }
    
    public function ListaDeLanchas(Request $request) {
      return Inertia::render($request->component, ['lanchas' => Lancha::whereStatus('A')->get()]); 
    }

    public function index()
    {
      $user = Auth()->User();
      return view('sistema.lancha.index', compact('user'));
    }

    public function ajato2000()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.ajato2000', compact('user','horario'));
    }
    public function encomendaAjato2000()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.ajato2000', compact('user','encomenda'));
    }
    public function crystal()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.crystal', compact('user','horario'));
    }
    public function encomendaCrystal()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.crystal', compact('user','encomenda'));
    }
    public function gloriaDeDeus()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.gloria_de_deus', compact('user','horario'));
    }
    public function encomendaGloriaDeDeus()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.gloria_de_deus', compact('user','encomenda'));
    }
    public function madameCris()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.madame_cris', compact('user','horario'));
    }
    public function encomendaMadameCris()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.madame_cris', compact('user','encomenda'));
    }
    public function missone()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.missone', compact('user','horario'));
    }
    public function encomendaMissone()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.missone', compact('user','encomenda'));
    }
    public function oriximina()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.oriximina', compact('user','horario'));
    }
    public function encomendaOriximina()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.oriximina', compact('user','encomenda'));
    }
    public function soberana()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.soberana', compact('user','horario'));
    }
    public function encomendaSoberana()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.soberana', compact('user','encomenda'));
    }
    public function taisHolanda()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.tais_holanda', compact('user','horario'));
    }
    public function encomendaTaisHolanda()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.tais_holanda', compact('user','encomenda'));
    }
    public function zeHolanda()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.ze_holanda', compact('user','horario'));
    }
    public function encomendaZeHolanda()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.ze_holanda', compact('user','encomenda'));
    }
    public function limaDeAbreu()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.lima_de_abreu', compact('user','horario'));
    }
    public function encomendaLimaDeAbreu()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.lima_de_abreu', compact('user','encomenda'));
    }
    public function belissima()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.belissima', compact('user','horario'));
    }
    public function encomendaBelissima()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.belissima', compact('user','encomenda'));
    }
    public function perola()
    {
      $user = Auth()->User();
      $horario = $this->objHorario->all();
      return view('sistema.lancha.perola', compact('user','horario'));
    }
    public function encomendaPerola()
    {
      $user = Auth()->User();
      $encomenda =  Encomenda::orderBy('id', 'DESC')->paginate(1000);
      return view('sistema.lancha.encomenda.perola', compact('user','encomenda'));
    }
}
