<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
    <head>
        @include('sistema.layout.head')
        <style>
            table{
                margin: 0px;
                font-size: 10pt;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            @php $first_key = key($dados) @endphp 
            @foreach ($dados as $chave => $item) 
                <div style="{{ $first_key == $chave ? '' : 'page-break-before: always;'}}">
                    <div class="row">
                        <div class="col">
                            <img src="{{ asset('img/ajato_laranja.png') }}" class="rounded mx-auto" style="height: 80px;">
                        </div>

                        <div class="col">
                            <h3 class="text-center">{{ $lancha->nome }}</h3>
                            <h3 class="text-center">Viagem Dia: {{\Carbon\Carbon::parse($data)->format('d/m/Y')}}</h3>
                        </div>
                    </div>

                    <table class="table table-sm table-bordered text-center mt-4">
                        <thead class="table-active">
                            <tr>
                                <th class="align-middle">ID</th>
                                <th class="align-middle">Descrição</th>
                                <th class="align-middle">Remetente</th>
                                <th class="align-middle">Destinatário</th>
                                <th class="align-middle">Destino</th>
                                <th class="align-middle">Valor</th>
                                <th class="align-middle">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($item as $encomendas)
                                <tr>
                                    @if ($encomendas->id)
                                        <td class="align-middle">{{ $encomendas->id }}</td>
                                        <td class="align-middle">{{ $encomendas->descricao }}</td>
                                        <td class="align-middle">{{ $encomendas->remetente }}</td>
                                        <td class="align-middle">{{ $encomendas->destinatario }}</td>
                                        <td class="align-middle">{{ $encomendas->municipio_nome }}</td>
                                        <td class="align-middle">R${{ number_format( $encomendas->valor, 2) }}</td>
                                        <td colspan="1" class="align-middle">{{ $encomendas->status }}</td>
                                    @else
                                        <td class="font-weight-bold text-right" colspan="4"></td>
                                        <td class="font-weight-bold" colspan="3">TOTAL FPG: R${{ number_format( $encomendas->valor, 2) }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </body>
</html>
