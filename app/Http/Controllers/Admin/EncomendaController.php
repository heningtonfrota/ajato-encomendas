<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Models\Encomenda;
use App\Models\EncomendaDeletada;
use App\Models\Lancha;
use App\Models\Municipio;
use App\Models\Relatorio;
use App\Models\ResetEncomendas;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Barryvdh\DomPDF\Facade as PDF;

use Dompdf\Dompdf;
use Dompdf\Options;

class EncomendaController extends Controller
{
    private $objEncomenda;
    private $objRelatorio;
    private $objMunicipio;
    private $objLancha;

    public function __construct(Lancha $lancha, Municipio $municipio)
    {
        $this->objEncomenda = new Encomenda();
        $this->objLancha = $lancha->where('status', 'A');
        $this->objMunicipio = $municipio->where('status', 'A');
        $this->objRelatorio = new Relatorio();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth()->User();
        $encomenda = Encomenda::orderBy('id', 'DESC')->paginate(25);

        return view('sistema.encomenda.lista', compact('user', 'encomenda'));
    }

    public function imprimirEtiqueta($id, $reset_id = null)
    {
        if ($reset_id) {
            $encomenda = ResetEncomendas::where('reset_id', $reset_id)->first();
            $encomenda->id = preg_replace("/[^0-9]/", "", $encomenda->reset_id);
        } else {
            $encomenda = Encomenda::find($id);
        }

        $encomenda->nome_lancha = $encomenda->relLancha->nome;
        $encomenda->nome_municipio = $encomenda->relMunicipio->nome;
        $encomenda->valor = number_format($encomenda->valor, 2);
        $encomenda->data = Carbon::parse($encomenda->data)->format('d/m/Y');

        return \PDF::loadView('sistema.encomenda.nova_etiqueta', ['dados' => $encomenda])->setPaper('a4', 'landscape')->stream('etiqueta.pdf');
    }

    public function imprimirDeclaracaoConteudo($id, $reset_id = null)
    {
        $user = Auth()->User();
        if ($reset_id) {
            $diaHora = date("Y-m-d H:i:s");
            $newId = ResetEncomendas::where('reset_id', $reset_id)->value('id');
            $encomenda = ResetEncomendas::find($newId);
            $encomenda->reset_id = preg_replace("/[^0-9]/", "", $encomenda->reset_id);
        } else {
            $diaHora = date("Y-m-d H:i:s");
            $encomenda = Encomenda::find($id);
        }

        return \PDF::loadView('sistema.encomenda.declaracao_conteudo', compact('user', 'encomenda', 'diaHora'))
            ->setPaper([0, 0, 300, 435])
            ->stream('declaracao_conteudo.pdf');
    }

    public function imprimirCupom($id, $reset_id = null)
    {
        $user = Auth()->User();
        if ($reset_id) {
            $diaHora = date("Y-m-d H:i:s");
            $newId = ResetEncomendas::where('reset_id', $reset_id)->value('id');
            $encomenda = ResetEncomendas::find($newId);
            $encomenda->reset_id = preg_replace("/[^0-9]/", "", $encomenda->reset_id);
        } else {
            $diaHora = date("Y-m-d H:i:s");
            $encomenda = Encomenda::find($id);
        }

        $encomenda->nome_lancha = $encomenda->relLancha->nome;
        $encomenda->nome_municipio = $encomenda->relMunicipio->nome;
        $encomenda->dia = Carbon::parse($diaHora)->format('d/m/Y');
        $encomenda->hora = Carbon::parse($diaHora)->format('H:i:s');
        $encomenda->usuario = $encomenda->relUser->name;
        $encomenda->saida_lancha = Carbon::parse($encomenda->data)->format('d/m/Y');
        $encomenda->data_cadastro = Carbon::parse($encomenda->data_criacao ?? $encomenda->created_at)->format('d/m/Y H:i:s');
        $encomenda->valor_formatado = number_format($encomenda->valor, 2);

        $altura_pdf = 520;

        $pdf = PDF::loadView('sistema.encomenda.novo_cupom', compact('encomenda'));
        $pdf->setPaper([20, 0, 300, $altura_pdf]);
        
        $dompdf = $pdf->getDomPDF();
        $dompdf->render();
        $canvas = $dompdf->getCanvas();
        $count_page = $canvas->get_page_number();

        if ($count_page > 1) {
            $altura_pdf += 35;
        }

        $pdf = PDF::loadView('sistema.encomenda.novo_cupom', compact('encomenda'));
        $pdf->setPaper([20, 0, 300, $altura_pdf]);        
        return $pdf->stream('cupom.pdf');
    }

    public function imprimirRecibo($id, $reset_id = null)
    {
        $user = Auth()->User();
        if ($reset_id) {
            $newId = ResetEncomendas::where('reset_id', $reset_id)->value('id');
            $encomenda = ResetEncomendas::find($newId);
            $encomenda->reset_id = preg_replace("/[^0-9]/", "", $encomenda->reset_id);
            $value = $encomenda->valor;
        } else {
            $encomenda = Encomenda::find($id);
            $value = $encomenda->valor;
        }
        $uppercase = 0;
        if (strpos($value, ",") > 0) {
            $value = str_replace(".", "", $value);
            $value = str_replace(",", ".", $value);
        }
        $singular = ["centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão"];
        $plural = ["centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões"];

        $c = ["", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
        $d = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"];
        $d10 = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"];
        $u = ["", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove"];

        $z = 0;

        $value = number_format($value, 2, ".", ".");
        $integer = explode(".", $value);
        $cont = count($integer);
        for ($i = 0; $i < $cont; $i++)
            for ($ii = strlen($integer[$i]); $ii < 3; $ii++)
                $integer[$i] = "0" . $integer[$i];

        $fim = $cont - ($integer[$cont - 1] > 0 ? 1 : 2);
        $rt = '';
        for ($i = 0; $i < $cont; $i++) {
            $value = $integer[$i];
            $rc = (($value > 100) && ($value < 200)) ? "cento" : $c[$value[0]];
            $rd = ($value[1] < 2) ? "" : $d[$value[1]];
            $ru = ($value > 0) ? (($value[1] == 1) ? $d10[$value[2]] : $u[$value[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                $ru) ? " e " : "") . $ru;
            $t = $cont - 1 - $i;
            $r .= $r ? " " . ($value > 1 ? $plural[$t] : $singular[$t]) : "";
            if (
                $value == "000"
            )
                $z++;
            elseif ($z > 0)
                $z--;
            if (($t == 1) && ($z > 0) && ($integer[0] > 0))
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                    ($integer[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        if (!$uppercase) {
            $extenso = trim($rt ? $rt : "zero");
        } elseif ($uppercase == "2") {
            $extenso = trim(strtoupper($rt) ? strtoupper(strtoupper($rt)) : "Zero");
        } else {
            $extenso = trim(ucwords($rt) ? ucwords($rt) : "Zero");
        }

        return \PDF::loadView('sistema.encomenda.recibo', compact('user', 'encomenda', 'extenso'))->setPaper('a4')->stream('recibo.pdf');
    }

    public function relatorioView(Request $req)
    {
        $form_data = $req->data;
        $form_lancha = $req->lancha;
        $user = Auth()->User();
        $lancha = $this->objLancha->get();
        $encomenda          = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
        $reset_encomendas   = ResetEncomendas::orderBy('reset_id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
        $all_encomendas     = $encomenda->mergeRecursive($reset_encomendas);
        $encomenda_total    = $all_encomendas->sum('valor');
        $encomenda_pago     = $all_encomendas->where('status', "Pago")->sum('valor');
        $encomenda_receber  = $all_encomendas->where('status', "FPG")->sum('valor');
        $ajato10            = $all_encomendas->sum('valor') * 0.1;
        $canoeiro           = $all_encomendas->where('status', "Pago")->sum('valor') - $ajato10;

        if ($form_lancha == 1) {
            if ($canoeiro < 0) {
                $ajato10 = $encomenda_total * 0.05;
                $canoeiro = $encomenda_receber - $ajato10;
            } else {
                $ajato10 = $encomenda_total * 0.05;
                $canoeiro = $encomenda_pago - $ajato10;
            }
        }

        if ($canoeiro < 0) {
            $canoeiro = $encomenda_receber - $ajato10;
        }

        $tabela_registro_relatorio = DB::table('relatorios')
            ->where('data', $form_data)
            ->where('id_lancha', $form_lancha)
            ->first();

            // dd($tabela_registro_relatorio);
        if (!$tabela_registro_relatorio) {
            $despesas = json_encode([]);
            $relatorio = json_encode([]);
        } else {
            $despesas = json_encode(DB::select("SELECT * FROM lista_despesas WHERE relatorio_id = $tabela_registro_relatorio->id"));
            $relatorio = json_encode($tabela_registro_relatorio);
        }

        return view(
            'sistema.encomenda.relatorio',
            compact(
                'user',
                'all_encomendas',
                'lancha',
                'encomenda_total',
                'encomenda_pago',
                'encomenda_receber',
                'ajato10',
                'canoeiro',
                'form_data',
                'form_lancha',
                'despesas',
                'relatorio'
            )
        );
    }
    public function relatorio()
    {
        return view(
            'sistema.encomenda.relatorio',
            [
                'user' => Auth()->User(),
                'encomenda' => Encomenda::orderBy('id', 'DESC')->paginate(15),
                'lancha' => $this->objLancha->get(),
                'encomenda_total' => 0,
                'encomenda_pago' => 0,
                'encomenda_receber' => 0,
                'ajato10' => 0,
                'canoeiro' => 0,
                'despesas' => json_encode([]),
                'relatorio' => json_encode([])
            ]
        );
    }

    public function cadastro()
    {
        //return view('ajato.encomenda.cadastro');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth()->User();

        $lancha = $this->objLancha->where('status', 'A')->get();
        $municipio = $this->objMunicipio->where('status', 'A')->get();
        $msg_cadastro = 0;
        return view('sistema.encomenda.cadastro', compact('user', 'lancha', 'municipio', 'msg_cadastro'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth()->User();

        $diaAtual = date("Y/m/d");
        $encomenda = Encomenda::where('data', $diaAtual)->orderBy('id', 'DESC')->paginate(15);
        $total_diario = Encomenda::where('data', $diaAtual)->sum('valor');
        $data_pago = Encomenda::where('status', "Pago")->where('data', $diaAtual)->sum('valor');
        $data_receber = Encomenda::where('status', "FPG")->where('data', $diaAtual)->sum('valor');
        $ajato10 = Encomenda::select('porcentagem')->where('data', $diaAtual)->sum('valor');
        $canoeiro = Encomenda::select('porcentagem')->where('data', $diaAtual)->sum('valor');

        $data = [
            'descricao' => $request->descricao,
            'volume' => $request->volume,
            'cpf' => $request->cpf,
            'remetente' => $request->remetente,
            'tel_remetente' => $request->tel_remetente,
            'cpf_destinatario' => $request->cpf_destinatario,
            'tel_destinatario' => $request->tel_destinatario,
            'destinatario' => $request->destinatario,
            'valor' => $request->valor,
            'lancha_id' => $request->lancha,
            'municipio_id' => $request->cidade_destino,
            'user_id' => $user->id,
            'status' => $request->status,
            'data' => $request->data,
        ];

        $this->objEncomenda->create($data);

        $encomenda = Encomenda::orderBy('id', 'DESC')->paginate(15);
        return view('sistema.encomenda.lista', compact('user', 'encomenda'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth()->User();

        $encomenda = Encomenda::find($id);
        $lancha = $this->objLancha->where('status', 'A')->get();
        $lanchaTodas = $this->objLancha->where('status', 'A')->get();
        $municipio = $this->objMunicipio->where('status', 'A')->get();
        $municipioTodos = $this->objMunicipio->where('status', 'A')->get();

        return view('sistema.encomenda.vizualizar', compact('user', 'encomenda', 'lancha', 'lanchaTodas', 'municipio', 'municipioTodos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth()->User();

        $encomenda = Encomenda::find($id);
        $lancha = $this->objLancha->where('status', 'A')->get();
        $lanchaTodas = $this->objLancha->where('status', 'A')->get();
        $municipio = $this->objMunicipio->where('status', 'A')->get();
        $municipioTodos = $this->objMunicipio->where('status', 'A')->get();
        $request['valor'] = str_replace(',', '', $request->valor);
        $request['user_id'] = Auth()->User()->id;
        $encomenda->update($request->all());

        return view('sistema.encomenda.vizualizar', compact('user', 'encomenda', 'lancha', 'lanchaTodas', 'municipio', 'municipioTodos'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $encomenda = Encomenda::find($id);

        $insert = collect($encomenda)->except(['created_at', 'updated_at'])->toArray();

        $insert['id_deletado'] = $encomenda->id;
        $insert['user_id_deleted'] = auth()->user()->id;
        $insert['data_criacao'] = $encomenda->created_at;
        $insert['usuario_criacao'] = $encomenda->user_id;
        $insert['deleted_at'] = Carbon::now();

        EncomendaDeletada::create($insert);

        $encomenda->delete();

        return redirect()->route('sistema.encomenda.lista');
    }

    public function pesquisarListaEncomenda(Request $req)
    {
        $filtro = $req->filtro;
        $user = Auth()->User();
        $filtros = $req->except('_token');
        $encomenda = $this->objEncomenda->pesquisar($req->all());
        return view('sistema.encomenda.lista', compact('user', 'encomenda', 'filtros'));
    }
    public function pesquisarCadastro(Request $request)
    {
        $filtered_request = collect($request->except('_token'))->map(function ($item_map) {
            return array_filter($item_map, function ($item_filter) {
                return !is_null($item_filter);
            });
        });
        
        if (count($filtered_request['remetente']) > 0) {
            $remetente = Encomenda::where($filtered_request['remetente'])->orderByDesc('id')->first() ?? ResetEncomendas::where($filtered_request['remetente'])->orderByDesc('id')->first();
        }

        if (count($filtered_request['destinatario']) > 0) {
            $destinatario = Encomenda::where($filtered_request['destinatario'])->orderByDesc('id')->first() ?? ResetEncomendas::where($filtered_request['destinatario'])->orderByDesc('id')->first();
        }

        return view('sistema.encomenda.cadastro', [
            'user' => Auth()->User(),
            'lancha' => $this->objLancha->get(),
            'municipio' => $this->objMunicipio->get(),
            'remetente' => $remetente ?? null,
            'destinatario' => $destinatario ?? null
        ]);
    }
}
