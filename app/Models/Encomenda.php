<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Encomenda extends Model
{
  protected $fillable = ['lancha_id', 'municipio_id', 'user_id', 'descricao', 'volume', 'remetente', 'cpf', 'tel_remetente', 'cpf_destinatario', 'destinatario', 'tel_destinatario', 'valor', 'status', 'data', 'created_at'];

  public function relLancha()
  {
    return $this->hasOne('App\Models\Lancha', 'id', 'lancha_id');
  }

  public function relMunicipio()
  {
    return $this->hasOne('App\Models\Municipio', 'id', 'municipio_id');
  }

  public function relUser()
  {
    return $this->hasOne('App\User', 'id', 'user_id');
  }

  public function pesquisar(Array $filtro)
  {
    //dd($filtro);
    $resultado = $this->where(function ($query) use ($filtro) {
      if (isset($filtro['id'])) {
        $query->where('id', 'LIKE', "%{$filtro['id']}%");
      }
      if (isset($filtro['destinatario'])) {
        $query->where('destinatario', 'LIKE', "%{$filtro['destinatario']}%");
      }
    })//->toSql();dd($resultado);
    ->paginate();
    return $resultado;


  }

}
