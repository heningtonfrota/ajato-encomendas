@extends('sistema.layout.index')

@section('titulo', 'Lista de Encomendas Resetadas')

@section('conteudo')
    <div class="container-fluid">
        <h1 class="text-center">Lista de Encomendas Resetadas</h1>

        <form action="{{route('reset.pesquisar')}}" method="post">
          @csrf
          <div class="form-row my-3">
            <div class="col-sm-2">
              <input type="text" class="form-control" name="id" placeholder="ID">
            </div>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="destinatario" placeholder="Destinatario">
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
          </div>
        </form>


        <div class="table-responsive">
            <table class="table table-sm table-bordered table-striped table-dark">
                <thead>
                    <tr class="text-center">
                        <th scope="col">ID</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Destinatario</th>
                        <th scope="col">Lancha</th>
                        <th scope="col">Destino</th>
                        <th scope="col">Status</th>
                        <th scope="col">Data</th>
                        <th scope="col" colspan="5">Ação</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($encomenda as $encomendas)
                    @php
                        $lancha     = $encomendas->find($encomendas->id)->relLancha;
                        $municipio = $encomendas->find($encomendas->id)->relMunicipio;
                    @endphp
                    <tr class="text-center">
                        <td>{{ $encomendas->reset_id }}</td>
                        <td>R$ {{ number_format( $encomendas->valor, 2) }}</td>
                        <td>{{$encomendas->destinatario}}</td>
                        <td>{{ $lancha->nome }}</td>
                        <td>{{ $municipio->nome }}</td>
                        <td>{{ $encomendas->status }}</td>
                        <td>{{ \Carbon\Carbon::parse( $encomendas->data )->format('d/m/Y') }}</td>
                        <td> <a class="text-warning" href="{{route('sistema.reset.vizualizar', $encomendas->id)}}"><i class="fas fa-edit"></i></a></td>
                        <td> <a class="text-primary" href="{{route('sistema.encomenda.etiqueta', [$encomendas->id, $encomendas->reset_id])}}" target="_blank"><i class="fas fa-print"></i></a></td>
                        <td> <a class="text-success" href="{{route('sistema.encomenda.cupom', [$encomendas->id, $encomendas->reset_id])}}" target="_blank"><i class="fas fa-file"></i></a></td>
                        <td> <a class="text-info" href="{{route('sistema.encomenda.recibo', [$encomendas->id, $encomendas->reset_id])}}" target="_blank"><i class="fas fa-file-alt"></i></a></td>
                        <td> <a class="text-danger" href="{{route('sistema.reset.deletar', $encomendas->id)}}"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
	
    <div class="d-flex justify-content-center">
        @if(isset($filtros))
          	{{ $encomenda->appends($filtros)->links() }}
        @else
          	{{ $encomenda->links() }}
        @endif
    </div>

    <div class="d-flex justify-content-center">
        <a href="{{route('sistema.home')}}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar </a>
    </div>
@endsection