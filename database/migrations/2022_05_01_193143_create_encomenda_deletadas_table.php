<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEncomendaDeletadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encomenda_deletadas', function (Blueprint $table) {
            $table->id();

            $table->string('id_deletado');

            $table->unsignedBigInteger('lancha_id')->length(10)->unsigned();
            $table->foreign('lancha_id')->references('id')->on('lanchas');

            $table->unsignedBigInteger('municipio_id')->length(10)->unsigned();
            $table->foreign('municipio_id')->references('id')->on('municipios');

            $table->unsignedBigInteger('user_id')->length(10)->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('user_id_deleted')->length(10)->unsigned();
            $table->foreign('user_id_deleted')->references('id')->on('users');

            $table->string('descricao');
            $table->string('remetente');
            $table->string('cpf')->length(14);
            $table->string('volume');
            $table->string('tel_remetente');
            $table->string('destinatario');
            $table->double('valor', 100,2);
            $table->string('status');
            $table->date('data');

            $table->timestamp('data_criacao');

            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encomenda_deletadas');
    }
}
