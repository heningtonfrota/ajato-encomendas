<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="utf-8">

        <link rel="sortcut icon" href="img/ajato_mini.png" type="image/x-icon" />

        <title>Etiqueta nº {{ $dados->id }}</title>

        <style>
            body { 
                margin: -25;
                font-family: sans-serif;
                font-weight: bolder;
                text-transform: uppercase;
            }
            p {
                margin: 0;
                padding: 0;
                white-space: nowrap;
            }

            .float-left { float: left; }
            .floar-right { float: right; }
            .clear-both::after { clear: both; }
            .ml-10 { margin-left: 10px; }
            .mt-65 { margin-top: 65px; }
            .letter-n10 { letter-spacing: -10; }
            .letter-n8 { letter-spacing: -8; }
            .letter-n3 { letter-spacing: -3; }
            .line-height-1 { line-height: 1; }
            .line-height-08 { line-height: 0.8; }
            .line-height-09 { line-height: 0.9; }
            .font-25-pt { font-size: 25pt; }
            .font-35-pt { font-size: 35pt; }
            .font-50-pt { font-size: 50pt; }
            .font-60-pt { font-size: 60pt; }
            .font-80-pt { font-size: 80pt; }
            .font-90-pt { font-size: 90pt; }
            .font-100-pt { font-size: 100pt; }
            .font-120-pt { font-size: 120pt; }
            .width-20 { width: 20%; }
            .width-30 { width: 30%; }
            .width-70 { width: 70%; }
            .width-80 { width: 80%; }
            .text-center { text-align: center; }
            .text-underline { text-decoration: underline; }
        </style>
    </head>
    <body>
        <!-- INFORMÇÕES DO TOPO DA ETIQUETA -->
        <div>
            <div class="float-left">
                <img src="img/ajato_etiqueta.png" alt="logo-ajato-encomendas" width="200">
            </div>

            <div class="float-left ml-10">
                <p class="font-50-pt line-height-1 letter-n3">LANCHA: {{ $dados->nome_lancha }}</p>
                <p class="font-50-pt line-height-09 letter-n3">DEST: {{ $dados->destinatario }}</p>
                <p class="font-60-pt line-height-09">DATA: {{ $dados->data }}</p>
            </div>
        </div>

        <div class="clear-both"></div>

        <!-- INFORMAÇÕES DO CONTEUDO DA ETIQUETA -->
        <div class="mt-65">
            <div class="float-left width-30">
                <p class="text-center text-underline font-25-pt">nº encomenda</p>
                <p class="text-center font-120-pt letter-n10 line-height-1">{{ $dados->id }}</p>
            </div>
            
            <div class="float-left width-70">
                @if ($dados->status == "Pago")
                    <p class="text-center text-underline font-25-pt">Status</p>
                    <p class="text-center font-120-pt letter-n3 line-height-1">{{ $dados->status }}</p>
                @else
                    <p class="text-center font-90-pt letter-n3 line-height-09">{{ $dados->status }}</p>
                    <p class="text-center font-90-pt letter-n3 line-height-08">R$ {{ $dados->valor }}</p>
                @endif
            </div>

            <div class="clear-both"></div>
        </div>

        <div class="mt-65">
            <div class="float-left width-20">
                <p class="text-center text-underline font-25-pt">Volume</p>
                <p class="text-center line-height-1 letter-n3 font-120-pt">{{ $dados->volume }}</p>
            </div>
            <div class="float-left width-80">
                <p class="text-center text-underline font-25-pt">Cidade</p>
                <p class="text-center line-height-1 letter-n8 font-100-pt">{{ $dados->nome_municipio }}</p>
            </div>
        </div>
    </body>
</html>
