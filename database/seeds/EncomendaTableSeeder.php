<?php

use Illuminate\Database\Seeder;
use App\Models\Encomenda;

class EncomendaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Encomenda::create([
            'lancha_id'         => '1',
            'municipio_id'      => '1',
            'pagamento_id'      => '1',
            'descricao'   		=> 'Computador de mesa',
            'remetente'   		=> 'José Silva',
            'cpf'    			=> '741258963',
            'tel_remetente'		=> '980801212',
            'destinatario'		=> 'Maria Francisca',
            //'cidade_destino'	=> 'Parintins',
            'valor'				=> '150.45',
            'data'              => '2020/03/22'
            //'lancha'			=> 'Gloria de Deus III',
            //'status'			=> 'Pagamento na Entrega',
        ]);

        Encomenda::create([
            'lancha_id'         => '2',
            'municipio_id'      => '2',
            'pagamento_id'      => '1',
            'descricao'   		=> 'Computador de mesa',
            'remetente'   		=> 'José Silva',
            'cpf'    			=> '741258963',
            'tel_remetente'		=> '980801212',
            'destinatario'		=> 'Maria Francisca',
            //'cidade_destino'	=> 'Parintins',
            'valor'				=> '150.45',
            'data'              => '2020/03/22'
            //'lancha'			=> 'Gloria de Deus III',
            //'status'			=> 'Pago',
        ]);

        Encomenda::create([
            'lancha_id'         => '3',
            'municipio_id'      => '3',
            'pagamento_id'      => '2',
            'descricao'   		=> 'Computador de mesa',
            'remetente'   		=> 'José Silva',
            'cpf'    			=> '741258963',
            'tel_remetente'		=> '980801212',
            'destinatario'		=> 'Maria Francisca',
            //'cidade_destino'	=> 'Parintins',
            'valor'				=> '150.45',
            'data'              => '2020/03/22'
            //'lancha'			=> 'Gloria de Deus III',
            //'status'			=> 'Pagamento na Entrega',
        ]);

        Encomenda::create([
            'lancha_id'         => '4',
            'municipio_id'      => '4',
            'pagamento_id'      => '1',
            'descricao'         => 'Computador de mesa',
            'remetente'         => 'José Silva',
            'cpf'               => '741258963',
            'tel_remetente'     => '980801212',
            'destinatario'      => 'Maria Francisca',
            //'cidade_destino'    => 'Parintins',
            'valor'             => '150.45',
            'data'              => '2020/03/22'
            //'lancha'            => 'Gloria de Deus III',
            //'status'            => 'Pagamento na Entrega',
        ]);

        Encomenda::create([
            'lancha_id'         => '5',
            'municipio_id'      => '4',
            'pagamento_id'      => '1',
            'descricao'         => 'Computador de mesa',
            'remetente'         => 'José Silva',
            'cpf'               => '741258963',
            'tel_remetente'     => '980801212',
            'destinatario'      => 'Maria Francisca',
            //'cidade_destino'    => 'Parintins',
            'valor'             => '150.45',
            'data'              => '2020/03/22'
            //'lancha'            => 'Gloria de Deus III',
            //'status'            => 'Pago',
        ]);

        Encomenda::create([
            'lancha_id'         => '6',
            'municipio_id'      => '3',
            'pagamento_id'      => '1',
            'descricao'         => 'Computador de mesa',
            'remetente'         => 'José Silva',
            'cpf'               => '741258963',
            'tel_remetente'     => '980801212',
            'destinatario'      => 'Maria Francisca',
            //'cidade_destino'    => 'Parintins',
            'valor'             => '150.45',
            'data'              => '2020/03/22'
            //'lancha'            => 'Gloria de Deus III',
            //'status'            => 'Pagamento na Entrega',
        ]);

        Encomenda::create([
            'lancha_id'         => '7',
            'municipio_id'      => '2',
            'pagamento_id'      => '2',
            'descricao'         => 'Computador de mesa',
            'remetente'         => 'José Silva',
            'cpf'               => '741258963',
            'tel_remetente'     => '980801212',
            'destinatario'      => 'Maria Francisca',
            //'cidade_destino'    => 'Parintins',
            'valor'             => '150.45',
            'data'              => '2020/03/22'
            //'lancha'            => 'Gloria de Deus III',
            //'status'            => 'Pagamento na Entrega',
        ]);

        Encomenda::create([
            'lancha_id'         => '8',
            'municipio_id'      => '1',
            'pagamento_id'      => '1',
            'descricao'         => 'Computador de mesa',
            'remetente'         => 'José Silva',
            'cpf'               => '741258963',
            'tel_remetente'     => '980801212',
            'destinatario'      => 'Maria Francisca',
            //'cidade_destino'    => 'Parintins',
            'valor'             => '150.45',
            'data'              => '2020/03/22'
            //'lancha'            => 'Gloria de Deus III',
            //'status'            => 'Pago',
        ]);

        Encomenda::create([
            'lancha_id'         => '9',
            'municipio_id'      => '3',
            'pagamento_id'      => '1',
            'descricao'         => 'Computador de mesa',
            'remetente'         => 'José Silva',
            'cpf'               => '741258963',
            'tel_remetente'     => '980801212',
            'destinatario'      => 'Maria Francisca',
            //'cidade_destino'    => 'Parintins',
            'valor'             => '150.45',
            'data'              => '2020/03/22'
            //'lancha'            => 'Gloria de Deus III',
            //'status'            => 'Pagamento na Entrega',
        ]);
    }
}
