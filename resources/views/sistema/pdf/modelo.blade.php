<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
<head>
    @include('sistema.layout.head')
</head>
<body>
    <div class="container-fluid">
        <div class="col-12">
            <img src="{{ asset('img/ajato_laranja.png')}}" class="rounded mx-auto d-block" style="height: 80px;">
            @if(isset($nome_lancha))
                <h3 class="text-center">{{$nome_lancha}}</h3>
            @else
                <h3 class="text-center">Todas as Lanchas</h3>
            @endif
            <h3 class="text-center">Viagem Dia: {{\Carbon\Carbon::parse( $form_data )->format('d/m/Y')}}</h3>
        </div>

        <div class="col-12">
            <table class="table table-bordered text-center mt-4">
                <thead class="table-active">
                    <tr>
                        <th class="align-middle">Valor Total</th>
                        <th class="align-middle">A Receber (FPG)</th>
                        <th class="align-middle">Pago</th>
                        @if($form_lancha == 1)
                            <th class="align-middle">Ajato 5%</th>
                        @else
                            <th class="align-middle">Ajato 10%</th>
                        @endif
                        <th class="align-middle">Valor da Embarcação</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="align-middle">R$ {{ number_format( $encomenda_total, 2) }}</td>
                        <td class="align-middle">R$ {{ number_format( $encomenda_receber, 2) }}</td>
                        <td class="align-middle">R$ {{ number_format( $encomenda_pago,    2) }}</td>
                        <td class="align-middle">R$ {{ number_format( $ajato10,      2) }}</td>
                        <td class="align-middle">R$ {{ number_format( $canoeiro,     2) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        @if ($form_lista_despesas)
            <div class="col-12">
                <div class="col-6 mx-auto">
                    <h2 class="text-center">Lista de Despesas</h2>
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>Valor</th>
                                <th>Descrição</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($form_lista_despesas as $item)
                                <tr>
                                    <td>R$ {{ $item['valor'] }}</td>
                                    <td>{{ $item['descricao'] }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2" class="text-right">
                                    <strong>TOTAL: </strong>
                                    R$ {{ number_format($total_lista_despesas, 2, ',', '.') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-12">
                <div class="col-6 mx-auto">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th><h2 class="text-center">Valor Final</h2></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2" class="text-center">
                                    <strong>R$ {{ number_format($total_lista_despesas_final, 2, ',', '.') }}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

        <h4> Relatório Emitido Por: {{Auth::user()->name}}</h4>
    </div>
</body>
</html>
