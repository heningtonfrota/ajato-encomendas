<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    @include('sistema.layout.head')
  </head>
  <body>
    <div class="container">
      <h1 class="text-center">Relatório Diário</h1>
    </div>

    <div class="container col-sm-8">
      <table class="table table-responsive-sm table-sm table-bordered table-striped">
        <thead>
          <tr class="thead-dark text-center">
            <th>Valor Total Diario</th>
            <th>A Receber (FPG)</th>
            <th>Pago</th>
            <th>Ajato 10%</th>
            <th>Valor da Embarcação</th>
          </tr>
        </thead>
        <tbody>
          <tr class="text-center">
            <td>R$ {{ number_format( $total_diario, 2) }}</td>
            <td>R$ {{ number_format( $data_receber, 2) }}</td>
            <td>R$ {{ number_format( $data_pago,    2) }}</td>
            <td>R$ {{ number_format( $ajato10,      2) }}</td>
            <td>R$ {{ number_format( $canoeiro,     2) }}</td>
          </tr>
        </tbody>
      </table>

      <h5>Relatorio Emitido Por: {{Auth::user()->name}}</h5>

      <hr>

      <table class="table table-responsive-sm table-sm table-bordered table-striped">
          <thead>
              <tr class="thead-dark text-center">
                  <th>ID</th>
                  <th>Valor</th>
                  <th>Lancha</th>
                  <th>Status</th>
                  <th>Data</th>
                </tr>
          </thead>
          <tbody>

          @foreach($encomenda as $encomendas)

              @php
                  $lancha = $encomendas->find($encomendas->id)->relLancha;
              @endphp

              <tr class="text-center">
                  <td>{{ $encomendas->id }}</td>
                  <td>R$ {{ number_format( $encomendas->valor, 2) }}</td>
                  <td>{{ $lancha->nome }}</td>
                  <td>{{ $encomendas->status }}</td>
                  <td>{{ \Carbon\Carbon::parse( $encomendas->data )->format('d/m/Y') }}</td>
              </tr>
          @endforeach
          </tbody>
      </table>
      <div class="d-flex justify-content-center py-2">
        <h3 class="text-center text-uppercase">Relatorio referente as Encomendas de Todas as Lanchas na Data: <i>{{\Carbon\Carbon::parse( $diaAtual )->format('d/m/Y')}}</i></h3>
      </div>
    </div>
  </body>
</html>
