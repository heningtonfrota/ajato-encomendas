@extends('sistema.layout.index')

@section('titulo', 'Ajato Encomendas - Relatorio')

@section('conteudo')
<div class="container">
    <h1 class="text-center">Relatórios</h1>
    <hr>
    <div class="row justify-content-center">
        <div class="card col-sm-5 text-center text-white bg-dark mr-3">
            <div class="card-body">
                <h4 class="card-title">Lanchas Individuais</h4>
                <hr>
                <p class="card-text">Aqui você pode gerar o relatório individual filtrados atraves de data e lancha.</p>
                <hr>
                <a href="{{route('sistema.encomenda.relatorio')}}" class="btn btn-warning"><i class="fas fa-file-alt"></i></a>
            </div>
        </div>

        <div class="card col-sm-5 text-center text-white bg-dark">
            <div class="card-body">
                <h4 class="card-title">Destinos</h4>
                <hr>
                <p class="card-text">Aqui você pode vizualizar a listagem de encomendas filtradas atraves de destino, data e lancha.</p>
                <hr>
                <a href="{{route('sistema.relatorio.relatorio_destino')}}" class="btn btn-warning">Individual <i class="fas fa-file-alt"></i></a>
                <a href="{{route('sistema.relatorio.relatorio_destino_todos')}}" class="btn btn-warning">Todos <i class="fas fa-file-alt"></i></a>
            </div>
        </div>

        <div class="card col-sm-5 text-center text-white bg-dark mt-3 mr-3">
            <div class="card-body">
                <h4 class="card-title">Todas as Lanchas</h4>
                <hr>
                <p class="card-text mb-3">Aqui você pode gerar o relatório de todas as lanchas filtrados apenas por data.</p>
                <hr>
                <a href="{{route('sistema.relatorio.relatorio')}}" class="btn btn-warning"><i class="fas fa-file-alt"></i></a>
            </div>
        </div>

        @if ($user->id <= 2)
            <div class="card col-sm-5 text-center text-white bg-secondary mt-3">
                <div class="card-body">
                    <h4 class="card-title">Valores Recebidos</h4>
                    <hr>
                    <p class="card-text">Aqui você pode gerar o relatório individual filtrados atraves de usuário e datas.</p>
                    <hr>
                    <a href="{{ route('sistema.relatorio.valores_recebidos_index') }}" class="btn btn-warning"><i class="fas fa-file-alt"></i></a>
                </div>
            </div>
        @endif
    </div>
    <h6 class="text-center mt-2"><i><small>Obs: o botão de ação permitira a vizualização do relatório referente.</small></i></h6>
</div>
@endsection
