<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListaDespesas extends Model
{
    use HasFactory;

    public $timestamps = true;
    public $guarded = ['id', 'created_at', 'updated_at'];
}
