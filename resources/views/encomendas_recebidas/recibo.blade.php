 <!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <link rel="stylesheet" href="css/recibo.css">
    <link rel="sortcut icon" href="img/ajato_mini.png" type="image/x-icon" />

    <title>Etiqueta nº {{ $dados->id }}</title>
  </head>
  <body>
    <div>
      <h2 id="remetente">{{ Str::limit($dados->recebedor, 24) }}</h2>
      <h2 id="cpf">{{ $dados->cpf }}</h2>
      <h2 id="pg">R$ {{ number_format( $dados->valor, 2) }}</h2>
      <h2 id="envio">ENVIO DE ENCOMENDA Nº {{ $dados->id }}</h2>
      <h2 id="envio2">NA LANCHA {{$dados->lancha}} - ORIGEM: {{ $dados->municipio }}</h2>
      <h2 id="extenso">{{ $dados->extenso }}</h2>
    </div>
  </body>
</html>
