import Vue from 'vue'
import Vuex from 'vuex'

import Alert from '../store/Alert'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    Alert
  }
})

export default store 