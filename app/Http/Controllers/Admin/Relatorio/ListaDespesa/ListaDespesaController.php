<?php

namespace App\Http\Controllers\Admin\Relatorio\ListaDespesa;

use App\Http\Controllers\Admin\Relatorio\PorLanchaController;
use App\Http\Controllers\Controller;
use App\Models\Inertia\Despesa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ListaDespesaController extends Controller
{
    const REGRAS = [
        'por_lancha_id' => ['required'],
        'descricao' => ['required'],
        'valor' => ['required']
    ];

    const MENSAGENS = [
        'required' => 'O campo :attribute é obrigatorio!'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lista = Despesa::where('por_lancha_id', $request->id)->get();
        $por_lancha = new PorLanchaController();
        return $por_lancha->index(null, $lista);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), self::REGRAS, self::MENSAGENS);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Despesa::create($request->all());

        return Redirect::route('lista_despesa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Despesa::find($id)->delete();
        return Redirect::route('lista_despesa.index');
    }
}
