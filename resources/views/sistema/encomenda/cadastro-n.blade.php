@extends('sistema.layout.index')

@section('titulo', 'Cadastro de Encomendas')

@section('conteudo')
<div class="container-fluid d-flex justify-content-center">
    <div class="card container col-sm bg-dark">
        <h2 class="card-header text-white text-center">Registro de Encomenda</h2>
        <div class="card-body">
            <form name="formCad" id="formCad" method="post" action="{{route('encomenda.store')}}">
                @csrf
                <div class="">
                  <span class="text-white">Informações da Encomenda</span>
                  <input type="text" name="descricao" class="form-control mb-2" id="validationCustom01" placeholder="Descrição" required>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <span class="text-white">Volume</span>
                        <input type="text" name="volume" class="form-control mb-2" id="validationCustom08" placeholder="" required>
                    </div>
                    <div class="col-sm">
                        <span class="text-white">C.P.F</span>
                        <input type="text" name="cpf" class="form-control mb-2" id="validationCustom07" placeholder="" required>
                    </div>
                    <div class="col-sm">
                        <span class="text-white">Tel. Remetente</span>
                        <input type="text" name="tel_remetente" class="form-control mb-2" id="validationCustom05"placeholder="" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <span class="text-white">Nome do Remetente</span>
                        <input type="text" name="remetente" class="form-control mb-2" id="validationCustom02" placeholder="" required>
                    </div>
                    <div class="col-sm">
                        <span class="text-white">Nome do Destinatário</span>
                        <input type="text" name="destinatario" class="form-control mb-2" id="validationCustom05" maxlength="" placeholder="Recomendado o Uso de até 18 Caracteres" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                      <span class="text-white">Lancha de Transporte</span>
                      <select class="form-control mb-2" name="lancha" id="validationCustom08" required>
                            <option selected disabled value="">Selecione a Lancha...</option>
                            @foreach($lanchas as $lancha)
                                <option value="{{ $lancha->id }}">{{ $lancha->nome }}</option>
                            @endforeach
                      </select>
                    </div>
                    <div class="col-sm">
                      <span class="text-white">Destino da Lancha</span>
                      <select class="form-control mb-2" name="cidade_destino" id="validationCustom08" required>
                          <option selected disabled value="">Selecione o Destino</option>
                          @foreach($municipio as $municipios)
                              <option value="{{ $municipios->id }}">{{ $municipios->nome }}</option>
                          @endforeach
                      </select>
                    </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <span class="text-white">Forma do Pagamento</span>
                    <select class="form-control mb-2" name="status" id="validationCustom08" required>
                        <option selected disabled value="">Selecione o Pagamento...</option>
                          <option value="Pago">Pago</option>
                          <option value="FPG">FPG</option>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <span class="text-white">Valor da Encomenda</span>
                    <input type="text" name="valor" class="form-control mb-2" id="validationCustom07" placeholder="Valor. Ex: 1,200.55" required>
                  </div>
                  <div class="col-sm-4  pb-3">
                    <span class="text-white">Data do Envio</span>
                    <input type="date" name="data" class="form-control mb-2" id="validationCustom05" placeholder="Data" required>
                  </div>
                </div>
                <button class="btn btn-warning btn-lg btn-block" type="submit">Registrar <i class="fas fa-arrow-circle-right"></i></button>
            </form>
        </div>
        <small class="text-center text-white pb-3">Todos os campos são obrigatorios!</small>
    </div>
</div>


@endsection
