@extends('sistema.layout.index')

@section('titulo','Relatório Diário')

@section('conteudo')
  <div class="container">
    <h1 class="text-center">Histórico de Relatórios Gerados</h1>
  </div>

  <div class="container col-sm-8">

    <form class="border rounded border-dark p-3">
      <div class="row text-center" style="font-size: 20px;">
        <div class="col-sm-4">
          <label class="font-weight-bold">Valor Total</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $relatorio->total_diario, 2 ) }}" readonly>
        </div>
        <div class="col-sm-4">
          <label class="font-weight-bold">Pagamento na Entrega</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $relatorio->total_receber, 2 ) }}" readonly>
        </div>
        <div class="col-sm-4">
          <label class="font-weight-bold">Pago</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $relatorio->total_pago, 2 ) }}" readonly>
        </div>
        <div class="col-sm-4">
          <label class="font-weight-bold">Faturamento</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="" readonly>
        </div>
        <div class="col-sm-4">
          <label class="font-weight-bold">Ajato 10%</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $relatorio->porcentagem, 2 ) }}" readonly>
        </div>
        <div class="col-sm-4">
          <label class="font-weight-bold">Valor das Embarcações</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $relatorio->embarcacao, 2) }}" readonly>
        </div>
      </div>
    </form>

    <h5>Relatorio Emitido Por: {{Auth::user()->name}}</h5>

    <hr>

    <table class="table table-responsive-sm table-sm table-bordered table-striped table-dark text-center">
      <thead class="table-active">
        <tr>
            <th class="align-middle">ID</th>
            <th class="align-middle">Valor</th>
            <th class="align-middle">Descrição</th>
            <th class="align-middle">Remetente</th>
            <th class="align-middle">Destino</th>
            <th class="align-middle">Destinatário</th>
            <th class="align-middle">Status</th>
        </tr>
    </thead>
    <tbody>
    @foreach($encomenda as $encomendas)
        @php
            $lancha     = $encomendas->find($encomendas->id)->relLancha;
            $municipio = $encomendas->find($encomendas->id)->relMunicipio;
        @endphp
        <tr>
            <td class="align-middle">{{ $encomendas->id }}</td>
            <td class="align-middle">R${{ number_format( $encomendas->valor, 2) }}</td>
            <td class="align-middle">{{ $encomendas->descricao }}</td>
            <td class="align-middle">{{ $encomendas->remetente }}</td>
            <td class="align-middle">{{ $municipio->nome }}</td>
            <td class="align-middle">{{ $encomendas->destinatario }}</td>
            <td class="align-middle">{{ $encomendas->status }}</td>
        </tr>
    @endforeach

    </tbody>
    </table>
  </div>
  <div class="d-flex justify-content-center py-2">
    @if(isset($id_lancha))
      <h3 class="col-sm-6 text-center text-uppercase">Relatório referente as Encomendas da Lancha <u>{{$nome_lancha}}</u> na Data: <i>{{\Carbon\Carbon::parse( $dia )->format('d/m/Y')}}</i></h3>
    @else
      <h3 class="col-sm-6 text-center text-uppercase">Relatório referente as Encomendas de Todas as Lanchas na Data: <i>{{\Carbon\Carbon::parse( $dia )->format('d/m/Y')}}</i></h3>
    @endif
  </div>
  <ul class="nav justify-content-center">
    <li><a href="{{ route('sistema.relatorio.historico') }}" class="btn btn-danger btn-lg mr-4"><i class="fas fa-arrow-left"></i> Voltar</a></li>
  </ul>
@endsection
