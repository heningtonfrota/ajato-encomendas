<?php

namespace App\Http\Controllers\Admin\Configuracoes;

use App\Http\Controllers\Controller;
use App\Models\Encomenda;
use App\Models\Lancha;
use App\Models\Municipio;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ConfiguracoesController extends Controller
{
    private $model_usuarios;
    private $model_lanchas;
    private $model_municipios;

    public function __construct()
    {
        $this->model_usuarios = new User();
        $this->model_lanchas = new Lancha();
        $this->model_municipios = new Municipio();
    }

    /**
     * Area de Usuarios.
     */
    public function usuarios()
    {
        return view('sistema.configuracoes.usuarios.index', [
            'user' => Auth()->User(),
            'usuarios' => User::where('id', '<>', 1)->orderBy('is_admin', 'desc')->orderBy('status', 'asc')->paginate(9)
        ]);
    }

    public function novoUsuario(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'is_admin' => 'nullable',
            'status' => 'nullable'
        ], [
            'name.required' => 'O campo nome é obrigatório',
            'email.required' => 'O campo email é obrigatório',
            'email.email' => 'O email informado é invalido',
            'password.required' => 'O campo senha é obrigatório'
        ]);
        
        if ($validator->fails()) return redirect()->back()->with('error', $validator->errors()->all());

        DB::beginTransaction();

        $usuario = new User();
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);
        $usuario->is_admin = !empty($request->is_admin) ? 'S' : 'N';
        $usuario->status = !empty($request->status) ? 'A' : 'I';
        $usuario->save();

        if (User::where('status', 'A')->count() > 6) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Limite de 5 usuários ativos atingido');
        }
        
        DB::commit();
        return redirect()->back()->with('success', 'Usuario criado com sucesso!');
    }

    public function editarUsuario(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'user_id' => 'required|not_in:1',
            'password' => 'nullable|min:6|max:15',
            'is_admin' => 'nullable',
            'status' => 'nullable'
        ], [
            'name.required' => 'O campo nome é obrigatório',
            'email.required' => 'O campo email é obrigatório',
            'email.email' => 'O email informado é invalido',
            'user_id.not_in' => 'O que está tentando fazer?',
            'password.required' => 'O campo senha é obrigatório',
            'password.min' => 'O campo senha deve ter no minimo 6 caracteres',
        ]);

        if ($validator->fails()) return redirect()->back()->with('error', $validator->errors()->all());
        
        DB::beginTransaction();
        $usuario = User::find($request->user_id);
        
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        if (!empty($request->password)) $usuario->password = bcrypt($request->password);
        $usuario->is_admin = !empty($request->is_admin) ? 'S' : 'N';
        $usuario->status = !empty($request->status) ? 'A' : 'I';
        $usuario->save();

        if (User::where('status', 'A')->count() > 6) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Limite de 5 usuários ativos atingido');
        }
        
        DB::commit();
        return redirect()->back()->with('success', 'Usuario Atualizado com sucesso!');
    }

    /**
     * Area de Lanchas.
     */
    public function lanchas()
    {
        $user = Auth()->User();

        $lanchas = $this->model_lanchas->paginate();

        return view('sistema.configuracoes.lanchas', compact('user', 'lanchas'));
    }

    public function alterarStatusLancha($id_lancha)
    {
        $lancha = $this->model_lanchas->find($id_lancha);

        switch ($lancha->status) {
            case 'A':
                $lancha->status = 'I';
                break;
            
            default:
                $lancha->status = 'A';
                break;
        }

        $lancha->save();

        return redirect()->back();
    }

    /**
     * Area de Municipios.
     */
    public function municipios()
    {
        $user = Auth()->User();

        $municipios = $this->model_municipios->orderBy('ordem')->paginate();

        return view('sistema.configuracoes.municipios', compact('user', 'municipios'));
    }

    public function alterarStatusMunicipio($id_municipio)
    {
        $municipio = $this->model_municipios->find($id_municipio);

        switch ($municipio->status) {
            case 'A':
                $municipio->status = 'I';
                break;
            
            default:
                $municipio->status = 'A';
                break;
        }

        $municipio->save();

        return redirect()->back();
    }
}
