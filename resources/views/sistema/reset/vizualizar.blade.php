@extends('sistema.layout.index')

@section('titulo', 'Registro de Encomenda')

@section('conteudo')
<div class="card container-fluid bg-dark text-white col-sm">
    <h2 class="card-header text-center">Registro de Encomenda <u>Nº {{$encomenda->reset_id}}</u></h2>
    <div class="card-body">
        <form name="formCad" id="formCad" method="post" action="{{ route('sistema.reset.update', $encomenda->id)}}">
            @method('PUT')
            @csrf
            @php
            $lancha = $encomenda->find($encomenda->id)->relLancha;
            $municipio = $encomenda->find($encomenda->id)->relMunicipio;
            @endphp
            <div class="row text-center">
                <div class="col-sm-10 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Descrição</h4></strong></label>
                    <input type="text" class="form-control text-center text-uppercase" name="descricao" style="font-size: 25px" value="{{$encomenda->descricao}}">
                </div>
                <div class="col-sm-2 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Volume</h4></strong></label>
                    <input type="text" class="form-control text-center" name="volume" style="font-size: 25px" value="{{$encomenda->volume}}">
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Remetente</h4></strong></label>
                    <input type="text" class="form-control text-center text-uppercase" name="remetente" style="font-size: 25px" value="{{$encomenda->remetente}}">
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>C.P.F</h4></strong></label>
                    <input type="text" class="form-control text-center" name="cpf" style="font-size: 25px" value="{{$encomenda->cpf}}">
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Telefone do Remetente</h4></strong></label>
                    <input type="text" class="form-control text-center" name="tel_remetente" style="font-size: 25px" value="{{$encomenda->tel_remetente}}">
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Destinatario</h4></strong></label>
                    <input type="text" class="form-control text-center text-uppercase" name="destinatario" style="font-size: 25px" value="{{$encomenda->destinatario}}">
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Lancha</h4></strong></label>
                    <select class="form-control py-0" name="lancha_id" id="validationCustom08" style="font-size: 25px">
                        <option selected disabled value="$lancha->id">Atual: {{ $lancha->nome }}</option>
                        @foreach($lanchaTodas as $lanchas)
                            <option value="{{ $lanchas->id }}">{{ $lanchas->nome }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Cidade de Destino</h4></strong></label>
                    <select class="form-control py-0" name="municipio_id" id="validationCustom08" style="font-size: 25px">
                        <option selected disabled value="{{$municipio->id}}">Atual: {{$municipio->nome}}</option>
                        @foreach($municipioTodos as $municipio)
                            <option value="{{ $municipio->id }}">{{ $municipio->nome }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Data</h4></strong></label>
                    <input type="text" class="form-control text-center" name="data" style="font-size: 25px" value="{{\Carbon\Carbon::parse( $encomenda->data )->format('Y/m/d')}}">
                </div>
                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Valor</h4></strong></label>
                    <input type="text" class="form-control text-center" name="valor" style="font-size: 25px" value="{{ number_format( $encomenda->valor, 2) }}">
                </div>

                <div class="col-sm-4 pb-3">
                    <label for="formGroupExampleInput"><strong><h4>Status</h4></strong></label>
                    <select class="form-control py-0" name="status" id="validationCustom08" style="font-size: 25px">
                        <option selected value="{{$encomenda->status}}">Atual: {{$encomenda->status}}</option>
                            <option value="Pago">Pago</option>
                            <option value="FPG">FPG</option>
                    </select>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-success mr-3">Atualizar <i class="fa fa-refresh" aria-hidden="true"></i></button>
                <a class="btn btn-primary text-white p-2 mr-3" href="{{route('sistema.encomenda.etiqueta', $encomenda->id)}}" target="_blank"><i class="fas fa-print"></i> Etiqueta</a>
                <a class="btn btn-warning text-white p-2 mr-3" href="{{route('sistema.encomenda.recibo', $encomenda->id)}}" target="_blank"><i class="fas fa-file"></i> Recibo</a>
                <a class="btn btn-info text-white p-2 mr-3" href="{{route('sistema.encomenda.cupom', $encomenda->id)}}"><i class="fas fa-file-alt"></i> Cupom</a>
                <a href="{{route('sistema.reset.index')}}" class="btn btn-danger p-2 mr-3"><i class="fas fa-arrow-left"></i> Voltar </a>
            </div>
        </form>
    </div>
</div>

@endsection