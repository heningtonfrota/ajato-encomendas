<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lancha;
use App\Models\Encomenda;
use App\Models\Municipio;
use Illuminate\Http\Request;
use App\Models\ResetEncomendas;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ResetEncomendasController extends Controller
{
    public function index()
    {
        try {
            $user = Auth()->User();
            $encomenda = ResetEncomendas::orderBy('id', 'DESC')->paginate(15);
            return view('sistema.reset.index', [
                'user' => $user,
                'encomenda' => $encomenda
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth()->User();
        $encomenda = ResetEncomendas::find($id);
        $lancha = Lancha::where('status', 'A')->get();
        $lanchaTodas = Lancha::where('status', 'A')->get();
        $municipio = Municipio::where('status', 'A')->get();
        $municipioTodos = Municipio::where('status', 'A')->get();

        return view('sistema.reset.vizualizar', compact('user', 'encomenda', 'lancha', 'lanchaTodas', 'municipio', 'municipioTodos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth()->User();

        $encomenda = ResetEncomendas::find(intval($id));
        $lancha = Lancha::where('status', 'A')->get();
        $lanchaTodas = Lancha::where('status', 'A')->get();
        $municipio = Municipio::where('status', 'A')->get();
        $municipioTodos = Municipio::where('status', 'A')->get();
        $request['valor'] = str_replace(',', '', $request->valor);
        $encomenda->update($request->all());

        return view('sistema.reset.vizualizar', compact('user', 'encomenda', 'lancha', 'lanchaTodas', 'municipio', 'municipioTodos'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ResetEncomendas::find($id)->delete();
        return redirect()->route('sistema.reset.index');
    }

    public function resetEncomendas()
    {
        DB::beginTransaction();
        try {
            $encomenda_atual = Encomenda::orderBy('id', 'DESC')->first();
            $letra = ResetEncomendas::orderBy('created_at', 'desc')->first();
            if ($letra == null) {
                $letra = 'A';
            } else {
                $letra = explode("@", $letra->reset_id);
                $letra[0]++;
            }
            if ($encomenda_atual->id >= 1) {
                $encomenda_reset = Encomenda::get();
                foreach ($encomenda_reset as $e) {
                    ResetEncomendas::create([
                        'reset_id' => $letra[0] . '@' . $e->id,
                        'lancha_id' => $e->lancha_id,
                        'user_id' => $e->user_id,
                        'municipio_id' => $e->municipio_id,
                        'volume' => $e->volume,
                        'remetente' => $e->remetente,
                        'cpf' => $e->cpf,
                        'tel_remetente' => $e->tel_remetente,
                        'destinatario' => $e->destinatario,
                        'valor' => $e->valor,
                        'status' => $e->status,
                        'data' => $e->data,
                        'descricao' => $e->descricao
                    ]);
                }
                DB::table('encomendas')->truncate();
                DB::commit();
                return redirect()->route('sistema.reset.index')
                    ->with('message', 'Copiado em Listar Resets e Reiniciado Encomendas!');
            }
        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
            return redirect()->back()->with('message', $th);
        }
    }

    public function pesquisarListaReset(Request $req)
    {
        $filtro = $req->filtro;
        $user = Auth()->User();
        $filtros = $req->except('_token');
        $encomenda = ResetEncomendas::pesquisar($req->all());
        return view('sistema.reset.index', compact('user', 'encomenda', 'filtros'));
    }
}
