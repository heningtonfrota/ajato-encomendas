<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
    <head>
        @include('sistema.layout.head')
        <style>
            table{
                margin: 0px;
                padding: 0px;
            }
            td {
                margin: 0px;
                padding: 0px;
            }
        </style>
    </head>
    <body>
        <div>
            <div class="row">
                <div class="col">
                    <img src="{{ asset('img/ajato_laranja.png')}}" class="rounded mx-auto d-block" style="height: 80px;">
                </div>

                <div class="col">
                    <h3 class="text-center">{{ $dados->nome_lancha }}</h3>
                    <h3 class="text-center">Viagem Dia: {{\Carbon\Carbon::parse($dados->data)->format('d/m/Y')}}</h3>
                </div>
            </div>

            <table class="table table-sm table-bordered text-center mt-4" style="font-size: 9pt;">
                <thead class="table-active">
                    <tr>
                        <th class="align-middle">ID</th>
                        <th class="align-middle">Descrição</th>
                        <th class="align-middle">Remetente</th>
                        <th class="align-middle">CPF Remetente</th>
                        <th class="align-middle">Tel. Remetente</th>
                        <th class="align-middle">Destinatário</th>
                        <th class="align-middle">CPF Destinatário</th>
                        <th class="align-middle">Tel. Destinatário</th>
                        <th class="align-middle">Destino</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($relatorios as $r)
                        <tr class="">
                            <td class="align-middle">{{ $r->id }}</td>
                            <td class="align-middle">{{ $r->descricao }}</td>
                            <td class="align-middle">{{ $r->remetente }}</td>
                            <td class="align-middle">{{ $r->cpf }}</td>
                            <td class="align-middle">{{ $r->tel_remetente }}</td>
                            <td class="align-middle">{{ $r->destinatario }}</td>
                            <td class="align-middle">{{ $r->cpf_destinatario }}</td>
                            <td class="align-middle">{{ $r->tel_destinatario }}</td>
                            <td class="align-middle">{{ $r->nome_municipio }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
            <h4>Relatório Emitido Por: {{Auth::user()->name}}</h4>
        </div>
    </body>
</html>
