@extends('sistema.layout.index')

@section('titulo','Relatório Diário')

@section('conteudo')
<div class="card container col-sm-8">
    <h1 class="card-header text-center">Relatório - Todas as Lanchas</h1>

    <form name="formCad" class="pt-3 m-auto" id="formCad" method="post" action="{{route('relatorio.todas_lancha')}}">
      @csrf
      <div class="form-row">
        <div class="col-sm-10">
          <input type="date" name="data" class="form-control" placeholder="Data">
        </div>
        <div class="col-sm-2">
            <button class="btn btn-warning" type="submit"><i class="fas fa-search"></i></button>
        </div>
      </div>
    </form>
    @if($encomenda_total != 0)
    <hr>
    <form>
      <div class="row text-center">
        <div class="col-sm-12">
          <label class="font-weight-bold h5">Valor Total</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $encomenda_total, 2) }}" readonly>
        </div>
        <div class="col-sm-6">
          <label class="font-weight-bold h5">Pagamento na Entrega</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $encomenda_receber, 2) }}" readonly>
        </div>
        <div class="col-sm-6">
          <label class="font-weight-bold h5">Pago</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $encomenda_pago, 2) }}" readonly>
        </div>
        <div class="col-sm-6">
          <label class="font-weight-bold h5">Ajato 10%</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $ajato10, 2) }}" readonly>
        </div>
        <div class="col-sm-6">
          <label class="font-weight-bold h5">Valor das Embarcações</label>
          <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg" value="R$ {{ number_format( $canoeiro, 2) }}" readonly>
        </div>
      </div>
    </form>
    <hr>
    <h5 class="m-0">Relatorio Emitido Por: <strong>{{Auth::user()->name}}</strong></h5>
    <hr>
    @elseif(isset($form_data) && $encomenda_total == 0)
      <h3 class="text-center bg-danger my-3 p-2">Seleção não retornou resultado!</h3>
    @else
      <h3 class="text-center bg-light my-3 p-2">Selecione a Data no campo acima!</h3>
    @endif

    <ul class="nav justify-content-center py-2">
      <li><a href="{{ route('sistema.relatorio.index') }}" class="btn btn-danger btn-lg mx-4"><i class="fas fa-arrow-left"></i> Voltar</a></li>
      @if(isset($form_data))
        <li>
            <form method="get" action="{{route('pdf.todas_as_lanchas')}}">
              @csrf
              <input type="hidden" name="data"    value="{{$form_data}}">
              @if(isset($form_data) && $encomenda_total != 0)
                <button class="btn btn-info btn-lg mx-4" type="submit">Gerar Relatório <i class="far fa-file-pdf"></i></button>
              @endif
            </form>
        </li>
      @endif
    </ul>
  </div>
  @if(isset($form_data) && $encomenda_total != 0)
    <div class="d-flex justify-content-center py-2">
      <h3 class="text-center text-uppercase col-sm-6">Relatorio referente as Encomendas de Todas as Lanchas na Data: <i>{{\Carbon\Carbon::parse( $form_data )->format('d/m/Y') }}</i></h3>
    </div>
  @else

  @endif
@endsection
