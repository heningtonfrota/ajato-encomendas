<?php

namespace App\Http\Controllers\Admin\Relatorio\PorDestino;

use App\Http\Controllers\Controller;
use App\Models\Encomenda;
use App\Models\Inertia\PorDestino\Individual;
use App\Models\Lancha;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class IndividualController extends Controller
{
    private $lanchas, $municipios;

    public function __construct()
    {
        $this->lanchas = Lancha::whereStatus('A')->get();
        $this->municipios = Municipio::whereStatus('A')->get();
    }
    
    public function index($lista_de_encomendas = [])
    {        
        $relatorio = DB::table('por_destino_individual as pdi')
            ->join('lanchas as l', 'l.id', 'pdi.lancha_id')
            ->join('municipios as m', 'm.id', 'pdi.municipio_id')
            ->selectRaw("
                pdi.*,
                l.nome as nome_lancha,
                m.nome as nome_municipio
            ")
            ->orderByDesc('id')
            ->get();

        return Inertia::render('Relatorio/PorDestino/Individual/Index', [
            'usuario' => auth()->user(),
            'lanchas' => $this->lanchas,
            'municipios' => $this->municipios,
            'relatorio_por_destino_individual' => $relatorio,
            'lista_de_encomendas' => $lista_de_encomendas
        ]);
    }

    public function store(Request $request)
    {
        $request['user_id'] = auth()->user()->id;

        $where = [
            ['lancha_id', $request->lancha_id],
            ['municipio_id', $request->municipio_id],
            ['data', $request->data]
        ];

        $verificar_registros_encomenda = Encomenda::where($where);
        $verificar_existe_relatorio = Individual::where($where);

        if ($verificar_existe_relatorio->count() > 0) {
            return Redirect::back()->withErrors(['errors' => 'Relatório existe!'])->withInput();
        }

        if ($verificar_registros_encomenda->count() == 0) {
            return Redirect::back()->withErrors(['errors' => 'Combinação de lancha, municipio e data não existe!'])->withInput();
        }

        Individual::create($request->all());

        return $this->index();
    }

    public function getListaDeEncomendas(Request $request)
    {
        $lista_de_encomendas = DB::table('encomendas as e')
            ->join('municipios as m', 'm.id', 'e.municipio_id')
            ->where($request->all())
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                m.nome as nome_municipio
            ");

        $lista_de_encomendas_resetadas = DB::table('reset_encomendas as e')
            ->join('municipios as m', 'm.id', 'e.municipio_id')
            ->where($request->all())
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                m.nome as nome_municipio
            ");

        $listas = $lista_de_encomendas->union($lista_de_encomendas_resetadas)->get();
        
        return $this->index($listas);
    }

    public function gerarRelatorio($id)
    {
        $dados = DB::table('por_destino_individual as pdi')
            ->join('lanchas as l', 'l.id', 'pdi.lancha_id') 
            ->where('pdi.id', $id)
            ->select('l.nome as nome_lancha', 'pdi.data', 'pdi.lancha_id', 'pdi.municipio_id')
            ->first();

        $relatorios_encomenda = DB::table('por_destino_individual as pdi')
            ->join('encomendas as e', function($join) {
                $join->on('e.data', 'pdi.data')
                    ->on('e.lancha_id', 'pdi.lancha_id')
                    ->on('e.municipio_id', 'pdi.municipio_id');
            })
            ->join('municipios as m', 'm.id', 'pdi.municipio_id')
            ->where([
                'pdi.data' => $dados->data,
                'pdi.lancha_id' => $dados->lancha_id,
                'pdi.municipio_id' => $dados->municipio_id
            ])
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                m.nome as nome_municipio
            ");

        $relatorios_reset_encomendas = DB::table('por_destino_individual as pdi')
            ->join('reset_encomendas as e', function($join) {
                $join->on('e.data', 'pdi.data')
                    ->on('e.lancha_id', 'pdi.lancha_id')
                    ->on('e.municipio_id', 'pdi.municipio_id');
            })
            ->join('municipios as m', 'm.id', 'pdi.municipio_id')
            ->where([
                'pdi.data' => $dados->data,
                'pdi.lancha_id' => $dados->lancha_id,
                'pdi.municipio_id' => $dados->municipio_id
            ])
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                m.nome as nome_municipio
            ");

        $relatorios = $relatorios_encomenda->union($relatorios_reset_encomendas)->get();

        return \PDF::loadView('relatorios.pdf.por_destino.individual', compact('relatorios', 'dados'))->setPaper('a4', 'landscape')->stream('imprimir-destinos.pdf');
    }
}
