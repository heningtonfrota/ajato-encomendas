<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEncomendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encomendas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lancha_id')->length(10)->unsigned();
            $table->foreign('lancha_id')->references('id')->on('lanchas');
            $table->unsignedBigInteger('municipio_id')->length(10)->unsigned();
            $table->foreign('municipio_id')->references('id')->on('municipios');
            $table->string('descricao');
            $table->string('remetente');
            $table->string('cpf')->length(11);
            $table->string('tel_remetente');
            $table->string('destinatario');
            $table->double('valor', 100,2);
            $table->string('status');
            $table->date('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encomendas');
    }
}
