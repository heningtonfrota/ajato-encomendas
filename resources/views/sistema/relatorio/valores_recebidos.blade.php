@extends('sistema.layout.index')

@section('titulo','Relatório - Valor Recebido')

@section('conteudo')
    <div class="container">
        <div class="card text-center">
            <div class="card-header">
                <h1>Relatório Valor Recebido</h1>
            </div>
            <div class="card-body">
                <form action="{{ route('sistema.relatorio.valores_recebidos') }}" method="post">
                    @csrf
                    <div class="form-row">
                        <label for="data-inicial" class="col-form-label text-right col-md-4">Data Inicial</label>
                        <div class="form-group col-md-6">
                            <input type="date" class="form-control" name="data_inicial" id="data-inicial" required>
                        </div>

                        <label for="data-final" class="col-form-label text-right col-md-4">Data Final</label>
                        <div class="form-group col-md-6">
                            <input type="date" class="form-control" name="data_final" id="data-final">
                        </div>

                        <label for="usuario" class="col-form-label text-right col-md-4">Usuário</label>
                        <div class="form-group col-md-6">
                            <select class="form-control mb-2" name="usuario" id="usuario" required>
                                <option selected disabled value="">Selecione o usuário...</option>
                                @foreach($usuarios as $usuario)
                                    <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <label for="lancha" class="col-form-label text-right col-md-4">Lancha</label>
                        <div class="form-group col-md-6">
                            <select class="form-control mb-2" name="lancha" id="lancha">
                                <option selected disabled value="">Selecione a Lancha...</option>
                                @foreach($lanchas as $lancha)
                                    <option value="{{ $lancha->id }}">{{ $lancha->nome }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary justify-content-center"><i class="fas fa-search"></i> Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @if(isset($dados_encomendas) || isset($dados_reset))
            <span class="h2">
                Relatorio do usuário: {{ $dados->usuario }}
                no periodo: {{ $dados->periodo_inicial }}
                @isset($dados->periodo_final)
                    ~ {{ $dados->periodo_final}}
                @endisset
            </span>

            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Valor</th>
                        <th>Fonte</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($dados_encomendas as $de)
                        <tr>
                            <td>{{ $de->status }}</td>
                            <td>R$ {{ number_format($de->valor, 2, ',', '.') }}</td>
                            <td>Tabela Principal</td>
                        </tr>
                    @endforeach

                    @foreach ($dados_reset as $dr)
                        <tr>
                            <td>{{ $dr->status }}</td>
                            <td>R$ {{ number_format($dr->valor, 2, ',', '.') }}</td>
                            <td>Tabela Reset</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
@endsection
