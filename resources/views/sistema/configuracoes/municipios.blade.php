@extends('sistema.layout.index')

@section('titulo', 'Configuração dos Municipios')

@section('conteudo')
<div class="container">
  <h1 class="text-center">Lista dos Municipios</h1>
  <div class="table-responsive">
    <table class="table table-sm table-bordered table-striped table-dark">
      <thead>
        <tr class="text-center">
          <th scope="col">ID</th>
          <th scope="col">Nome</th>
          <th scope="col">Status</th>
          <th scope="col">Ordem</th>
          <th scope="col">Ações</th>
        </tr>
      </thead>
      <tbody>
      @foreach($municipios as $municipio)
        <tr class="text-center">
          <td>{{ $municipio->id }}</td>
          <td>{{ $municipio->nome }}</td>
          <td>{{ $municipio->status == 'A' ? 'Ativo' : 'Inativo' }}</td>
          <td>{{ $municipio->ordem }}</td>
          <td>
            <a class="text-warning" href="{{ route('sistema.configuracoes.municipio.alterar_status', $municipio->id) }}" title="Editar">
              <i class="fas fa-edit"></i>
            </a>
          </td>
        </tr>
      @endforeach

      </tbody>
    </table>
  </div>
</div>
<div class="d-flex justify-content-center">
  @if(isset($filtros))
    {{ $municipios->appends($filtros)->links() }}
  @else
    {{ $municipios->links() }}
  @endif

</div>
<div class="d-flex justify-content-center">
  <a href="{{route('sistema.home')}}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar </a>
</div>
@endsection