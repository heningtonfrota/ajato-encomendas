<?php

namespace App\Http\Controllers\Admin\Relatorio\PorDestino;

use App\Http\Controllers\Controller;
use App\Models\Encomenda;
use App\Models\Inertia\PorDestino\Geral;
use App\Models\Lancha;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class GeralController extends Controller
{
    private $lanchas;

    public function __construct()
    {
        $this->lanchas = Lancha::whereStatus('A')->get();
    }
    
    public function index($lista_de_encomendas = [])
    {        
        $relatorio = DB::table('por_destino_geral as pdg')
            ->join('lanchas as l', 'l.id', 'pdg.lancha_id')
            ->selectRaw("
                pdg.*,
                l.nome as nome_lancha
            ")
            ->orderByDesc('id')
            ->get();

        return Inertia::render('Relatorio/PorDestino/Geral/Index', [
            'usuario' => auth()->user(),
            'lanchas' => $this->lanchas,
            'relatorio_por_destino_geral' => $relatorio,
            'lista_de_encomendas' => $lista_de_encomendas
        ]);
    }

    public function store(Request $request)
    {
        $request['user_id'] = auth()->user()->id;
        
        $where = [
            ['lancha_id', $request->lancha_id],
            ['data', $request->data]
        ];
        
        $verificar_registros_encomenda = Encomenda::where($where);
        $verificar_existe_relatorio = Geral::where($where);
        
        if ($verificar_existe_relatorio->count() > 0) {
            return Redirect::back()->withErrors(['errors' => 'Relatório existe!'])->withInput();
        }

        if ($verificar_registros_encomenda->count() == 0) {
            return Redirect::back()->withErrors(['errors' => 'Combinação de lancha, municipio e data não existe!'])->withInput();
        }

        Geral::create($request->all());

        return $this->index();
    }

    public function getListaDeEncomendas(Request $request)
    {
        $lista_de_encomendas = DB::table('encomendas as e')
            ->join('municipios as m', 'm.id', 'e.municipio_id')
            ->join('lanchas as l', 'l.id', 'e.lancha_id')
            ->where($request->all())
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                l.nome as nome_lancha,
                m.nome as nome_municipio
            ");

        $lista_de_encomendas_resetadas = DB::table('reset_encomendas as e')
            ->join('municipios as m', 'm.id', 'e.municipio_id')
            ->join('lanchas as l', 'l.id', 'e.lancha_id')
            ->where($request->all())
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                l.nome as nome_lancha,
                m.nome as nome_municipio
            ");

        $listas = $lista_de_encomendas->union($lista_de_encomendas_resetadas)->get();

        return $this->index($listas);
    }

    public function gerarRelatorio($id)
    {
        $dados = DB::table('por_destino_geral as pdi')
            ->join('lanchas as l', 'l.id', 'pdi.lancha_id') 
            ->where('pdi.id', $id)
            ->select('l.nome as nome_lancha', 'pdi.data', 'pdi.lancha_id')
            ->first();

        $relatorios_encomenda = DB::table('por_destino_geral as pdi')
            ->join('encomendas as e', function($join) {
                $join->on('e.data', 'pdi.data')
                    ->on('e.lancha_id', 'pdi.lancha_id');
            })
            ->join('municipios as m', 'm.id', 'e.municipio_id')
            ->where([
                'pdi.data' => $dados->data,
                'pdi.lancha_id' => $dados->lancha_id
            ])
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                m.nome as nome_municipio
            ")
            ->orderByDesc('m.nome');

        $relatorios_reset_encomendas = DB::table('por_destino_geral as pdi')
            ->join('reset_encomendas as e', function($join) {
                $join->on('e.data', 'pdi.data')
                    ->on('e.lancha_id', 'pdi.lancha_id');
            })
            ->join('municipios as m', 'm.id', 'e.municipio_id')
            ->where([
                'pdi.data' => $dados->data,
                'pdi.lancha_id' => $dados->lancha_id
            ])
            ->selectRaw("
                e.id,
                e.descricao ,
                e.remetente,
                e.cpf ,
                e.tel_remetente ,
                e.destinatario ,
                e.cpf_destinatario ,
                e.tel_destinatario,
                m.nome as nome_municipio
            ")
            ->orderByDesc('m.nome');

        $relatorios = $relatorios_encomenda->union($relatorios_reset_encomendas)->get();

        return \PDF::loadView('relatorios.pdf.por_destino.geral', compact('relatorios', 'dados'))->setPaper('a4', 'landscape')->stream('imprimir-destinos.pdf');
    }
}
