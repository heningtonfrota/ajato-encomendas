<?php

namespace App\Http\Controllers\Admin;

use App\Models\EncomendaDeletada;
use App\Http\Controllers\Controller;
use App\Models\Encomenda;
use App\Models\Lancha;
use App\Models\Municipio;
use Illuminate\Http\Request;

class EncomendaDeletadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'sistema.encomendas_deletadas.lista_deletados',
            [
                'user' => Auth()->User(),
                'encomendas_deletadas' => EncomendaDeletada::orderByDesc('id')->paginate(12)
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EncomendaDeletada  $encomendaDeletada
     * @return \Illuminate\Http\Response
     */
    public function show($id, EncomendaDeletada $encomendaDeletada)
    {
        return view(
            'sistema.encomendas_deletadas.vizualizar',
            [
                'user' => Auth()->User(),
                'encomenda' => $encomendaDeletada->find($id),
                'lancha' => Lancha::find($encomendaDeletada->lancha_id),
                'lanchaTodas' => Lancha::where('status', 'A')->get(),
                'municipio' => Municipio::find($encomendaDeletada->municipio_id),
                'municipioTodos' => Municipio::where('status', 'A')->get()
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EncomendaDeletada  $encomendaDeletada
     * @return \Illuminate\Http\Response
     */
    public function edit(EncomendaDeletada $encomendaDeletada)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EncomendaDeletada  $encomendaDeletada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EncomendaDeletada $encomendaDeletada)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EncomendaDeletada  $encomendaDeletada
     * @return \Illuminate\Http\Response
     */
    public function destroy(EncomendaDeletada $encomendaDeletada)
    {
        //
    }

    public function retornarEncomenda($id, EncomendaDeletada $encomendaDeletada)
    {
        $encomenda = $encomendaDeletada->find($id);

        $insert = collect($encomenda)->except(['id', 'id_deletado', 'user_id_deleted', 'data_criacao', 'deleted_at'])->toArray();

        $insert['id'] = $encomenda->id_deletado;
        $insert['created_at'] = $encomenda->data_criacao;

        Encomenda::create($insert);

        $encomenda->delete();

        return redirect()->route('sistema.encomendas_deletadas.index');
    }

    public function imprimirRecibo($id, EncomendaDeletada $encomendaDeletada)
    {
        $encomenda = $encomendaDeletada->find($id);
        $value = $encomenda->valor;

        $uppercase = 0;

        if (strpos($value, ",") > 0) {
            $value = str_replace(".", "", $value);
            $value = str_replace(",", ".", $value);
        }
        $singular = ["centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão"];
        $plural = ["centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões"];

        $c = ["", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
        $d = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"];
        $d10 = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"];
        $u = ["", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove"];

        $z = 0;

        $value = number_format($value, 2, ".", ".");
        $integer = explode(".", $value);
        $cont = count($integer);
        for ($i = 0; $i < $cont; $i++)
            for ($ii = strlen($integer[$i]); $ii < 3; $ii++)
                $integer[$i] = "0" . $integer[$i];

        $fim = $cont - ($integer[$cont - 1] > 0 ? 1 : 2);
        $rt = '';
        for ($i = 0; $i < $cont; $i++) {
            $value = $integer[$i];
            $rc = (($value > 100) && ($value < 200)) ? "cento" : $c[$value[0]];
            $rd = ($value[1] < 2) ? "" : $d[$value[1]];
            $ru = ($value > 0) ? (($value[1] == 1) ? $d10[$value[2]] : $u[$value[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                $ru) ? " e " : "") . $ru;
            $t = $cont - 1 - $i;
            $r .= $r ? " " . ($value > 1 ? $plural[$t] : $singular[$t]) : "";
            if (
                $value == "000"
            )
                $z++;
            elseif ($z > 0)
                $z--;
            if (($t == 1) && ($z > 0) && ($integer[0] > 0))
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                    ($integer[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        if (!$uppercase) {
            $extenso = trim($rt ? $rt : "zero");
        } elseif ($uppercase == "2") {
            $extenso = trim(strtoupper($rt) ? strtoupper(strtoupper($rt)) : "Zero");
        } else {
            $extenso = trim(ucwords($rt) ? ucwords($rt) : "Zero");
        }

        return \PDF::loadView(
            'sistema.encomenda.recibo',
            [
                'user' => Auth()->User(),
                'encomenda' => $encomenda,
                'extenso' => $extenso
            ])
            ->setPaper('a4')
            ->stream('recibo.pdf');
    }

    public function imprimirCupom($id, EncomendaDeletada $encomendaDeletada)
    {
        return \PDF::loadView(
            'sistema.encomenda.cupom', [
                'user' => Auth()->User(),
                'encomenda' => $encomendaDeletada->find($id),
                'diaHora' => date("Y-m-d H:i:s")
            ])
            ->setPaper([0, 0, 300, 560])
            ->stream('cupom.pdf');
    }

    public function pesquisarListaEncomenda(Request $request)
    {
        $filtros = $request->except('_token');

        $encomenda = new EncomendaDeletada;

        $encomendas_deletadas = $encomenda->pesquisar($filtros);

        return view('sistema.encomendas_deletadas.lista_deletados', [
            'user' => Auth()->User(),
            'encomendas_deletadas' => $encomendas_deletadas,
            'filtros' => $filtros
        ]);
    }
}
