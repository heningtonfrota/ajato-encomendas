<?php

use App\Models\Lancha;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePorLanchasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('por_lanchas', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Lancha::class);
            $table->float('porcentagem_ajato');
            $table->float('porcentagem_lancha');
            $table->float('valor_total');
            $table->float('valor_pago');
            $table->float('valor_fpg');
            $table->date('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('por_lanchas');
    }
}
