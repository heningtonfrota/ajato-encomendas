@extends('sistema.layout.index')

@section('titulo', 'Configuração das Lanchas')

@section('conteudo')
<div class="container">
  <h1 class="text-center">Lista das Lanchas</h1>
  <div class="table-responsive">
    <table class="table table-sm table-bordered table-striped table-dark">
      <thead>
        <tr class="text-center">
          <th scope="col">ID</th>
          <th scope="col">Nome</th>
          <th scope="col">Status</th>
          <th scope="col">Ações</th>
        </tr>
      </thead>
      <tbody>
      @foreach($lanchas as $lancha)
        <tr class="text-center">
          <td>{{ $lancha->id }}</td>
          <td>{{ $lancha->nome }}</td>
          <td>{{ $lancha->status == 'A' ? 'Ativo' : 'Inativo' }}</td>
          <td>
            <a class="text-warning" href="{{ route('sistema.configuracoes.lancha.alterar_status', $lancha->id) }}" title="Editar">
              <i class="fas fa-edit"></i>
            </a>
          </td>
        </tr>
      @endforeach

      </tbody>
    </table>
  </div>
</div>
<div class="d-flex justify-content-center">
  @if(isset($filtros))
    {{ $lanchas->appends($filtros)->links() }}
  @else
    {{ $lanchas->links() }}
  @endif

</div>
<div class="d-flex justify-content-center">
  <a href="{{route('sistema.home')}}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar </a>
</div>
@endsection