@extends('sistema.layout.index')

@section('conteudo')

    <div class="container-fluid">

        <h1 class="text-center">Encomendas Soberana</h1>

        <div class="table-responsive text-truncate">
            <table class="table table-sm table-bordered table-striped table-dark">
                <thead>
                    <tr class="text-center">
                        <th scope="col" style="width: 15px;">#</th>
                        <th scope="col">Descrição do Objeto</th>
                        <th scope="col">Remetente</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Tel. Remetente</th>
                        <th scope="col">Destinatário</th>
                        <th scope="col">Cidade Destino</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Lancha</th>
                        <th scope="col">Status</th>
                        <th scope="col">Data</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($encomenda as $encomendas)
                        @php
                            $lancha = $encomendas->find($encomendas->id)->relLancha;
                            $municipio = $encomendas->find($encomendas->id)->relMunicipio;
                        @endphp
                        <tr class="text-center">
                            @if($lancha->id == 7)
                              <td>{{ $encomendas->id }}</td>
                              <td>{{ $encomendas->descricao }}</td>
                              <td>{{ $encomendas->remetente }}</td>
                              <td>{{ $encomendas->cpf }}</td>
                              <td>{{ $encomendas->tel_remetente }}</td>
                              <td>{{ $encomendas->destinatario }}</td>
                              <td>{{ $municipio->nome }}</td>
                              <td>R$ {{ number_format( $encomendas->valor, 2) }}</td>
                              <td>{{ $lancha->nome }}</td>
                              <td>{{ $encomendas->status }}</td>
                              <td>{{ \Carbon\Carbon::parse( $encomendas->created_at )->format('d/m/Y') }}</td>
                            @endif
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">
                                <h2>Nenhuma Encomenda Localizada!</h2>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        {{ $encomenda->links() }}
    </div>
    <div class="d-flex justify-content-center">
        <a href="{{ route('sistema.lancha.index') }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar </a>
    </div>
@endsection
