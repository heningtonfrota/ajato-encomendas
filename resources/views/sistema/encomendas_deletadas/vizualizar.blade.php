@extends('sistema.layout.index')

@section('titulo', 'Registro de Encomenda Deletada')

@section('conteudo')
  <div class="card container-fluid bg-dark text-white col-sm">
        <h2 class="card-header text-center">Registro de Encomenda Deletada <u>Nº {{ $encomenda->id_deletado }}</u></h2>

        <h2 class="card-header text-center">
            Deletada por: {{ $user->name }}
            em: {{ \Carbon\Carbon::parse($encomenda->deleted_at)->format('d/m/Y H:i:s') }}
        </h2>

        <div class="card-body">
            <form>
                @php
                    $lancha = $encomenda->find($encomenda->id)->relLancha;
                    $municipio = $encomenda->find($encomenda->id)->relMunicipio;
                    $user = $encomenda->find($encomenda->id)->relUser;
                @endphp

                <div class="row text-center">
                    <div class="col-sm-10 pb-3">
                        <label>
                            <strong>
                                <h4>Descrição</h4>
                            </strong>
                        </label>

                        <input
                            disabled
                            type="text"
                            class="form-control text-center text-uppercase"
                            name="descricao"
                            style="font-size: 25px"
                            value="{{$encomenda->descricao}}"
                        >
                    </div>

                    <div class="col-sm-2 pb-3">
                        <label>
                            <strong>
                                <h4>Volume</h4>
                            </strong>
                        </label>

                        <input
                            disabled
                            type="text"
                            class="form-control text-center"
                            name="volume"
                            style="font-size: 25px"
                            value="{{$encomenda->volume}}"
                        >
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Remetente</h4>
                            </strong>
                        </label>

                        <input
                            disabled
                            type="text"
                            class="form-control text-center text-uppercase"
                            name="remetente"
                            style="font-size: 25px"
                            value="{{$encomenda->remetente}}"
                        >
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>C.P.F</h4>
                            </strong>
                        </label>

                        <input
                            disabled
                            type="text"
                            class="form-control text-center"
                            name="cpf"
                            style="font-size: 25px"
                            value="{{$encomenda->cpf}}"
                        >
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Telefone do Remetente</h4>
                            </strong>
                        </label>

                        <input
                            disabled
                            type="text"
                            class="form-control text-center"
                            name="tel_remetente"
                            style="font-size: 25px"
                            value="{{$encomenda->tel_remetente}}"
                        >
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Destinatario</h4>
                            </strong>
                        </label>

                        <input
                            disabled
                            type="text"
                            class="form-control text-center text-uppercase"
                            name="destinatario"
                            style="font-size: 25px"
                            value="{{$encomenda->destinatario}}"
                        >
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Lancha</h4>
                            </strong>
                        </label>

                        <select
                            disabled
                            class="form-control py-0 text-center"
                            name="lancha_id"
                            style="font-size: 25px"
                        >
                            <option selected disabled value="$lancha->id">{{ $lancha->nome }}</option>

                            @foreach($lanchaTodas as $lanchas)
                                <option value="{{ $lanchas->id }}">{{ $lanchas->nome }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Cidade de Destino</h4>
                            </strong>
                        </label>

                        <select
                            disabled
                            class="form-control py-0 text-center"
                            name="municipio_id"
                            style="font-size: 25px"
                        >
                            <option selected disabled value="{{$municipio->id}}">{{$municipio->nome}}</option>

                            @foreach($municipioTodos as $municipio)
                                <option value="{{ $municipio->id }}">{{ $municipio->nome }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Data</h4>
                            </strong>
                        </label>
                        <input
                            disabled
                            type="text"
                            class="form-control text-center"
                            name="data"
                            style="font-size: 25px"
                            value="{{\Carbon\Carbon::parse( $encomenda->data )->format('Y/m/d')}}"
                        >
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Valor</h4>
                            </strong>
                        </label>
                        <input
                            disabled
                            type="text"
                            class="form-control text-center"
                            name="valor"
                            style="font-size: 25px"
                            value="{{ number_format( $encomenda->valor, 2) }}"
                        >
                    </div>

                    <div class="col-sm-4 pb-3">
                        <label>
                            <strong>
                                <h4>Status</h4>
                            </strong>
                        </label>

                        <select
                            disabled
                            class="form-control py-0 text-center"
                            name="status"
                            style="font-size: 25px"
                        >
                            <option selected value="{{$encomenda->status}}">{{$encomenda->status}}</option>
                                <option value="Pago">Pago</option>
                                <option value="FPG">FPG</option>
                        </select>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <a
                        class="btn btn-secondary text-white p-2 mr-3"
                        href="{{route('sistema.encomendas_deletadas.retornar_encomenda', $encomenda->id)}}"
                        title="Retornar Encomenda Deletada"
                    ><i class="fas fa-arrow-left"></i> Restaurar</a>

                    <a
                        class="btn btn-warning text-white p-2 mr-3"
                        href="{{route('sistema.encomendas_deletadas.recibo', $encomenda->id)}}"
                        target="_blank"
                    ><i class="fas fa-file"></i> Recibo</a>

                    <a
                        class="btn btn-info text-white p-2 mr-3"
                        href="{{route('sistema.encomendas_deletadas.cupom', $encomenda->id)}}"
                        target="_blank"
                    ><i class="fas fa-file-alt"></i> Cupom</a>

                    <a
                        class="btn btn-danger p-2 mr-3"
                        href="{{route('sistema.encomendas_deletadas.index')}}"
                    ><i class="fas fa-arrow-left"></i> Voltar</a>
                </div>
            </form>
        </div>
    </div>

@endsection
