<?php

namespace App\Http\Controllers\Admin\EncomendaRecebidaLancha;

use App\Http\Controllers\Controller;
use App\Models\EncomendaRecebidaLancha;
use App\Models\Lancha;
use App\Models\Municipio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use WGenial\NumeroPorExtenso\NumeroPorExtenso;

class EncomendaRecebidaLanchaController extends Controller
{
    private $lanchas, $municipios;
    const REGRAS = [
        'lancha_id' => ['required'],
        'data_entrada' => ['required'],
        'descricao' => ['required'],
        'destinatario' => ['required'],
        'valor' => ['required'],
        'status' => ['required'],
        'entregue' => ['required'],
        'recebedor_nome' => [ 'required_if:entregue,true' ],
        'recebedor_contato' => [ 'required_if:entregue,true' ],
    ];

    const MENSAGENS = [
        'required' => 'O campo :attribute é obrigatorio!',
        'required_if' => 'O campo :attribute é obrigatorio!',
    ];

    public function __construct(Request $request)
    {
        $this->lanchas = Lancha::get();
        $this->municipios = Municipio::get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $encomendas = $this->getDadosListaDeEncomendasRecebidas();

        return Inertia::render('EncomendasRecebidasLancha/Index', [
            'usuario' => auth()->user(),
            'lanchas' => $this->lanchas,
            'municipios' => $this->municipios,
            'encomendas_recebidas' => $encomendas
        ]); 
    }

    private function getDadosListaDeEncomendasRecebidas(object $filtros = null)
    {
        $query = $this->buscarDadosListaEncomendaRecebidaLancha();

        $encomendas = $query->when(
                isset($filtros),
                function($query) use ($filtros) {
                    $query->where([
                        ['erl.lancha_id', $filtros->lancha_id],
                        ['erl.data_entrada', $filtros->data_entrada],
                    ]);
                }
            )
            ->orderByDesc('id')
            ->get();

        return $encomendas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('EncomendasRecebidasLancha/NovaEncomendaRecebida', [
            'usuario' => auth()->user(),
            'lanchas' => $this->lanchas,
            'municipios' => $this->municipios
        ]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['user_id'] = auth()->user()->id;

        $validator = Validator::make($request->all(), self::REGRAS, self::MENSAGENS);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        EncomendaRecebidaLancha::create($request->all());

        return Redirect::route('encomendas_recebidas_lancha.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editar_item = EncomendaRecebidaLancha::find($id);

        return Inertia::render('EncomendasRecebidasLancha/EditarEncomendaRecebida', [
            'usuario' => auth()->user(),
            'lanchas' => $this->lanchas,
            'municipios' => $this->municipios,
            'editar_item' => $editar_item
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->entregue) {
            $request['nome_usuario_entrega'] = auth()->user()->name;
            $request['data_entrega'] = Carbon::now();
        }

        $editar_item = EncomendaRecebidaLancha::find($id);

        $validator = Validator::make($request->all(), self::REGRAS, self::MENSAGENS);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $editar_item->update($request->all());

        return Redirect::route('encomendas_recebidas_lancha.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EncomendaRecebidaLancha::find($id)->delete();
        return Redirect::route('encomendas_recebidas_lancha.index');
    }

    public function imprimirRecibo($id, $reset_id = null)
    {
        $query = $this->buscarDadosListaEncomendaRecebidaLancha();

        $encomenda = $query
            ->where('erl.id', $id)
            ->first();

        $extenso = new NumeroPorExtenso;
        $extenso = $extenso->converter($encomenda->valor);

        $dados = (object) [
            'id' => $encomenda->id,
            'recebedor' => $encomenda->recebedor_nome,
            'cpf' => '',
            'valor' => $encomenda->valor,
            'extenso' => $extenso,
            'lancha' => $encomenda->nome_lancha,
            'municipio' => $encomenda->nome_municipio
        ];

        return \PDF::loadView('encomendas_recebidas.recibo', ['dados' => $dados])
            ->setPaper('a4')
            ->stream('recibo.pdf');
    }

    public function relatorioLanchaData($encomendas_relatorio = null)
    {
        $lista_relatorios = DB::select("
            select 
                *,
                concat(d.entregues, '/', d.total_recebido) as status
            from (
                SELECT 
                    erl.lancha_id,
                    l.nome as nome_lancha, 
                    erl.data_entrada,
                    CASE WHEN erl.pago_lancha = 1 THEN 1 ELSE 0 END AS pago_lancha,
                    COUNT(CASE WHEN erl.entregue = 1 THEN 1 ELSE NULL END) AS entregues, 
                    COUNT(CASE WHEN erl.entregue = 0 THEN 1 ELSE NULL END) AS nao_entregues,
                    count(erl.entregue) as total_recebido
                FROM 
                    encomenda_recebida_lanchas erl 
                    JOIN lanchas l ON l.id = erl.lancha_id 
                WHERE
                    erl.deleted_at IS NULL
                GROUP BY 
                    l.nome,
                    erl.lancha_id,
                    erl.data_entrada,
                    erl.pago_lancha
            ) as d
            order by
                d.data_entrada desc
        ");

        return Inertia::render('EncomendasRecebidasLancha/Relatorios/EncomendasRecebidas', [
            'usuario' => auth()->user(),
            'lanchas' => $this->lanchas,
            'lista_relatorios' => $lista_relatorios,
            'encomendas_relatorio' => $encomendas_relatorio
        ]);
    }

    public function relatorioEncomendasLista(Request $request)
    {
        $dados = $this->getDadosListaDeEncomendasRecebidas((object) $request->all());
        return $this->relatorioLanchaData($dados);
    }

    private function buscarDadosListaEncomendaRecebidaLancha()
    {
        $dados = DB::table('encomenda_recebida_lanchas AS erl')
            ->join('lanchas AS l', 'l.id', 'erl.lancha_id')
            ->leftJoin('municipios AS m', 'm.id', 'erl.municipio_id')
            ->join('users As u', 'u.id', 'erl.user_id')
            ->selectRaw("
                erl.*, 
                case 
                    when erl.entregue = 1 and erl.data_entrega is not null and erl.nome_usuario_entrega is not null 
                        then concat('SIM - ', date_format(erl.data_entrega, '%d/%m/%y %H:%i'), ' - ', erl.nome_usuario_entrega)
                    when erl.entregue = 1 
                        then 'SIM'
                    else 'NÃO' 
                end as entregue_formatado,
                l.nome as nome_lancha,
                m.nome as nome_municipio,
                u.name as nome_usuario
            ")
            ->whereDeletedAt(null);

        return $dados;
    }

    public function relatorioEncomendasGerarRelatorio(Request $request)
    {
        $data_formatada = Carbon::createFromFormat('d/m/Y', $request->data_entrada)->format('Y-m-d');
        $lancha = Lancha::find($request->lancha_id);

        $query = $this->buscarDadosListaEncomendaRecebidaLancha();

        $dados = $query->where([
                ["lancha_id", $request->lancha_id],
                ["data_entrada", $data_formatada]
            ])
            ->get();
            
        $totais = $this->buscarTotaisRelatorio((object) ['lancha_id' => $request->lancha_id, 'data_entrada' => $data_formatada]);

        $pdf = \PDF::loadView('relatorios.pdf.encomendas_recebidas_lancha.modelo', [
                'nome_lancha' => $lancha->nome,
                'data_entrada' => $request->data_entrada,
                'totais' => $totais,
                'dados' => $dados
            ])
            ->setPaper('a4', 'landscape');
            
        return $pdf->stream();
    }

    private function buscarTotaisRelatorio($filtros)
    {
        $porcentagem = $filtros->lancha_id == 1 ? 5 : 10;
        $query = "
            select 
                d.total_entregue,
                d.porcentagem_ajato,
                (d.total_entregue - d.porcentagem_ajato) as diferenca
            from (
                select
                    sum(valor) as total_entregue,
                    (sum(valor) * {$porcentagem}) / 100 as porcentagem_ajato
                from
                    encomenda_recebida_lanchas v1
                where
                    v1.lancha_id = {$filtros->lancha_id}
                    and v1.data_entrada = '{$filtros->data_entrada}'
                    and v1.entregue = 1
                    and v1.deleted_at is null
            ) as d
        ";

        $dados = DB::selectOne($query);

        return $dados;
    }

    public function relatorioEncomendasAlterarStatus(Request $request)
    {
        DB::table('encomenda_recebida_lanchas AS erl')
            ->whereLanchaId($request->lancha_id)
            ->whereDataEntrada($request->data_entrada)
            ->update(['pago_lancha' => $request->pago_lancha]);

        return $this->relatorioLanchaData();
    }
}
