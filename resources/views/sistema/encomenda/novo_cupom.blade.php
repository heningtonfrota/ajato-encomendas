<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="utf-8">

        <link rel="sortcut icon" href="img/ajato_mini.png" type="image/x-icon" />

        <title>Etiqueta nº {{ $encomenda->id }}</title>

        <style>
            body {
                margin: 0;
                padding: 0;
                page-break-after: always;
                font-weight: bolder;
            }
            div {
                margin: 0;
                padding: 0;
            }
            .divisor {
                width: 300;
                height: 1px;
                background-color: #000;
                margin: 5 0 5 -15;
            }
            .text-center { text-align: center; }
            .text-uppercase { text-transform: uppercase; }
            .mt-2 { margin-top: 2; }
            .mt-3 { margin-top: 3; }
            .mt-4 { margin-top: 4; }
            .ml-10 { margin-left: 10; }
            .font-10 { font-size: 10pt; }
            .font-14 { font-size: 14pt; }
            .font-16 { font-size: 16pt; }
            .float-left { float: left; }
            .float-right { float: right; }
            .clear-both { clear: both; }
            .box-padrao { width: 250 !important }
            p { margin: 0; }
            .h-40 { height: 40; }
            .h-30 { height: 30; }
        </style>
    </head>
    <body>
        <div class="box-padrao">
            <div class="float-left">
                <img src="img/ajato_etiqueta.png" alt="" width="70">
            </div>
            <div class="float-left">
                <p class="ml-10 font-16">AJATO ENCOMENDAS</p>
                <p class="ml-10 font-16">CNPJ: 40.105.601/0001-83</p>
                <p class="ml-10 font-16">FONE: (92) 99434-1616</p>
            </div>
            <div class="clear-both"></div>
        </div>

        <div class="divisor"></div>

        <div class="box-padrao">
            <p class="text-center font-16 text-uppercase">Cadastro de Encomendas</p>
        </div>

        <div class="divisor"></div>

        <div class="box-padrao">
            <p class="text-center font-14">ENCOMENDA NÚMERO: {{ $encomenda->id }}</p>
            <p class="text-center font-14">DEST: {{ $encomenda->destinatario }}</p>
        </div>

        <div class="divisor"></div>

        <div class="box-padrao">
            <p class="font-10">LANCHA: <strong>{{ $encomenda->nome_lancha }}</strong></p>
            <p class="font-10 mt-3">SAÍDA DA LANCHA: <strong>{{ $encomenda->saida_lancha }}</strong></p>
            <p class="font-10 mt-3">DESTINO: <strong>{{ $encomenda->nome_municipio }}</strong></p>
            <p class="font-10 mt-3">DESCRIÇÃO: <strong>{{ $encomenda->descricao }}</strong></p>
            <p class="font-10 mt-3">REMETENTE: <strong>{{ $encomenda->remetente }}</strong></p>
            <p class="font-10 mt-3">CADASTRO: <strong>{{ $encomenda->data_cadastro }}</strong></p>
            <p class="font-10 mt-3">SETOR DE ENCOMENDAS - TERMINAL AJATO</p>
            <p class="font-10 mt-3">REGISTRADO POR: <strong>{{ $encomenda->relUser->name ?? '' }}</strong></p>
        </div>

        <div class="divisor"></div>

        <div class="box-padrao">
            <p class="text-center font-16">VALOR: R$<strong>{{ $encomenda->valor_formatado }}</strong></p>
            <p class="text-center font-16">STATUS: <strong>{{ $encomenda->status }}</strong></p>
        </div>

        <div class="divisor"></div>

        <div class="box-padrao">
            <p class="font-10">
                EMISSÃO: <strong>{{ $encomenda->dia }}</strong> 
                HORA: <strong>{{ $encomenda->hora }}</strong>
            </p>
            <p class="font-10 mt-4">* Taxa de cancelamento/retirada de encomenda após a postagem é de R$ 10,00 reais por volume.</p>
            <p class="font-10 mt-2">* Encomendas precisam ser retiradas diretamente na lancha.</p>
            <p class="font-10 mt-2">* Após o prazo de 30 dias corridos a contar da data de emissão desse comprovante, encomendas esquecidas serão descartadas após esse período. Sem aviso prévio. </p>
            <p class="font-10 mt-2">Desenvolva seu sistema: contato@codenoxus.com</p>
        </div>
    </body>
</html>
