<?php

namespace App\Http\Controllers\Admin\Relatorio\TodosDestinosDataLancha;

use App\Http\Controllers\Controller;
use App\Models\Lancha;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Codedge\Fpdf\Fpdf\Fpdf;

class TodosDestinosDataLanchaController extends Controller
{
    function gerarPdf($data, $lancha) {
        // Crie uma instância do FPDF
        $pdf = new Fpdf();
        // Defina a margem esquerda como zero
        $pdf->SetLeftMargin(4);
        // Adicione uma página
        $pdf->AddPage();    
        // Buscar dados da tabela   
        $dados = $this->buscarDados($data, $lancha);
        $model_lancha = Lancha::find($lancha);
        // Contador para não gerar pagina em banco
        $count = count($dados);

        // Adicione a imagem à esquerda
        $pdf->Image(asset('img/ajato_laranja.png'), 5, 10, 15);
        // Defina o cabeçalho com a codificação UTF-8
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(0, 7, utf8_decode($model_lancha->nome), 0, 1, 'C');
        $pdf->Cell(0, 7, utf8_decode('Viagem Dia: ' . Carbon::createFromFormat('Y-m-d', $data)->format('d/m/Y')), 0, 1, 'C');
        // Margem entre o cabeçalho e a tabela (10mm de espaço)
        $pdf->Cell(0, 5, '', 0, 1);
        
        foreach ($dados as $encomendas) {
            // Cabeçalho da tabela com background cinza
            $pdf->SetFillColor(192, 192, 192);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(8, 6, utf8_decode('ID'), 1, 0, 'C', true); // Defina o preenchimento para true
            $pdf->Cell(65, 6, utf8_decode('Descrição'), 1, 0, 'C', true);
            $pdf->Cell(41, 6, utf8_decode('Remetente'), 1, 0, 'C', true); 
            $pdf->Cell(41, 6, utf8_decode('Destinatário'), 1, 0, 'C', true); 
            $pdf->Cell(22, 6, utf8_decode('Destino'), 1, 0, 'C', true); 
            $pdf->Cell(13, 6, utf8_decode('Valor'), 1, 0, 'C', true); 
            $pdf->Cell(12, 6, utf8_decode('Status'), 1, 1, 'C', true); // O último parâmetro define o final da linha com preenchimento

            foreach ($encomendas as $e) {
                if ($e->id) {
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->Cell(8, 6, utf8_decode($e->id), 1, 0, 'R'); // Defina o preenchimento para true
                    $pdf->Cell(65, 6, $this->validarString($e->descricao, 34), 1, 0, 'L');
                    $pdf->Cell(41, 6, $this->validarString($e->remetente, 20), 1, 0, 'L'); 
                    $pdf->Cell(41, 6, $this->validarString($e->destinatario, 20), 1, 0, 'L'); 
                    $pdf->Cell(22, 6, $this->validarString($e->municipio_nome, 13), 1, 0, 'R'); 
                    $pdf->Cell(13, 6, number_format($e->valor, 2, ',', '.'), 1, 0, 'R'); 
                    $pdf->Cell(12, 6, utf8_decode($e->status), 1, 1,  'C');
                } else {
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Cell(177, 6, $this->validarString($e->descricao, 34), 1, 0, 'R');
                    $pdf->Cell(25, 6, number_format($e->valor, 2, ',', '.'), 1, 1, 'R');
                }
            }

            if (--$count > 0) $pdf->Ln(); // Pular para a próxima linha
        }

        // Saída do PDF (pode ser para download, visualização ou salvamento)
        $pdf->Output();
        exit;
    }

    private function validarString($string, $tamanho) 
    {
        return strlen($string) > $tamanho ? substr(utf8_decode($string), 0, $tamanho) . '...' : utf8_decode($string);
    }

    public function buscarDados($data, $lancha)
    {
        $query = "
            select
                e.reset_id as id,
                e.lancha_id,
                e.descricao,
                e.remetente,
                e.destinatario,
                m.ordem,
                m.nome as municipio_nome,
                e.valor,
                e.status
            from reset_encomendas e 
            inner join `municipios` as `m` on `m`.`id` = `e`.`municipio_id`
            where (`e`.`lancha_id` = {$lancha} and `e`.`data` = '{$data}')
            union
            select
                e.id,
                e.lancha_id,
                e.descricao,
                e.remetente,
                e.destinatario,
                m.ordem,
                m.nome as municipio_nome,
                e.valor,
                e.status
            from `encomendas` as `e`
            inner join `municipios` as `m` on `m`.`id` = `e`.`municipio_id`
            where (`e`.`lancha_id` = {$lancha} and `e`.`data` = '{$data}')
            union
            select 
                a.id,
                a.lancha_id,
                a.descricao,
                a.remetente,
                a.destinatario,
                a.ordem,
                a.municipio_nome,
                sum(a.valor) as valor,
                a.status
            from 
            (
                select
                    '' as id,
                    et.lancha_id,
                    'TOTAL FPG' as descricao,
                    '' as remetente,
                    '' as destinatario,
                    mt.ordem,
                    mt.nome as municipio_nome,
                    sum(case when et.status = 'FPG' then et.valor else 0 end) as valor,
                    '' as status
                from `reset_encomendas` as `et`
                inner join `municipios` as `mt` on `mt`.`id` = `et`.`municipio_id`
                where (`et`.`lancha_id` = {$lancha} and `et`.`data` = '{$data}')
                group by
                    `municipio_nome`,
                    `mt`.`ordem`,
                    `et`.`lancha_id`
                union
                select
                    '' as id,
                    et.lancha_id,
                    'TOTAL FPG' as descricao,
                    '' as remetente,
                    '' as destinatario,
                    mt.ordem,
                    mt.nome as municipio_nome,
                    sum(case when et.status = 'FPG' then et.valor else 0 end) as valor,
                    '' as status
                from `encomendas` as `et`
                inner join `municipios` as `mt` on `mt`.`id` = `et`.`municipio_id`
                where (`et`.`lancha_id` = {$lancha} and `et`.`data` = '{$data}')
                group by
                    `municipio_nome`,
                    `mt`.`ordem`,
                    `et`.`lancha_id`
            ) a
            group by 
                a.id,
                a.descricao,
                a.remetente,
                a.destinatario,
                a.lancha_id,
                a.ordem,
                a.municipio_nome,
                a.status
            order by ordem, id desc
        ";
        $dados_query = DB::select($query);
        $collection = collect($dados_query)->sortBy([
            ['ordem', 'asc'], 
            ['id', 'desc']
        ]);
        $grouped = $collection->groupBy('municipio_nome'); 
        $dados = $grouped->toArray();
        return $dados;
    }

    public function geraRelatorio($data, $lancha) 
    { 
        $dados = $this->buscarDados($data, $lancha);
        $total_fpg = DB::table('encomendas')->where(['lancha_id' => $lancha, 'data' => $data, 'status' => 'FPG'])->sum('valor');
        $lancha = Lancha::find($lancha);
        return \PDF::loadView(
            'relatorios.pdf.todos_destinos_data_lancha', 
            compact('total_fpg', 'lancha', 'data', 'dados')
        )
        ->setPaper('a4', 'landscape')
        ->stream('todos-destinos.pdf');
    }
}
