<!DOCTYPE html>
<html lang="pt-br">
  <head><meta charset="gb18030">
    <!-- Meta tags Obrigatórias -->

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="sortcut icon" href="{{asset('img/ajato_mini.png')}}" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Place your kit's code here -->
    <script src="https://kit.fontawesome.com/37f525ac36.js" crossorigin="anonymous"></script>

    <title>Ajato Encomendas</title>
  </head>
  <body>
    <div class="container">
      <div class="mx-auto col-sm-6 text-center" style="margin-top: 15%;">
          <img src="{{ asset('img/ajato_laranja.png')}}" class="rounded mx-auto d-block" alt="Ajato Encomendas">
          <h2>Levamos suas encomendas com segurança e rapidez</h2>
          <a href="{{ route('login') }}" class="btn btn-lg btn-warning">Entrar <i class="fas fa-sign-in-alt"></i></a>
      </div>
    </div>
    <div class="container-fluid text-center bg-dark text-white fixed-bottom">
      <p class="mb-0">2020 Copyright © Desenvolvido por: <a href="#" class="text-info">Henington Frota</a> & <a href="#" class="text-info">Erik Lima </a></p>
    </div>
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
