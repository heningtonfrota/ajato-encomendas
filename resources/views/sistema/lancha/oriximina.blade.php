@extends('sistema.layout.index')

@section('titulo', 'Ajato Encomendas - Horarios')

@section('conteudo')
  <div class="container">
    <h1 class="text-center">Horário da Lancha Oriximiná</h1>
    <table class="table table-sm table-bordered table-dark">
      <thead>
          <tr class="text-center">
            <th><h3><i>Origem</i></h3></th>
            <th><h3><i>Destino</i></h3></th>
            <th><h3><i>Dias</i></h3></th>
            <th><h3><i>Horarios</i></h3></th>
          </tr>
      </thead>
      <tbody>
          @forelse($horario as $horarios)
            @if($horarios->id >= 83 && $horarios->id <= 88)
              <tr class="text-center">
                  <td>{{ $horarios->origem }}</td>
                  <td class="text-truncate" style="max-width: 250px;">{{ $horarios->destino }}</td>
                  <td class="text-truncate" style="max-width: 250px;">{{ $horarios->dia }}</td>
                  <td>{{ $horarios->horario }}</td>
              </tr>
            @endif
          @empty
              <tr>
                  <td colspan="6">
                      <h2>Nenhum Horário Localizado!</h2>
                  </td>
              </tr>
          @endforelse
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-content-center">
      <a href="{{route('sistema.lancha.index')}}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar </a>
  </div>
@endsection
