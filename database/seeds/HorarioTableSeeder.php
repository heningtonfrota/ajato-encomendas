<?php

use Illuminate\Database\Seeder;
use App\Models\Horario;

class HorarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DESTINOS DA LANCHA AJATO 2000 // 1-6
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'CODAJÁS',
                'dia'       => '2ª, 4ª e Sáb',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'COARI',
                'dia'       => '2ª, 4ª e Sáb',
                'horario'   => '12:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'TEFÉ',
                'dia'       => '2ª, 4ª e Sáb',
                'horario'   => '15:00',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'COARI',
                'dia'       => '3ª, 5ª, Dom',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'CODAJÁS',
                'dia'       => '3ª, 5ª, Dom',
                'horario'   => '09:30',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'MANAUS',
                'dia'       => '3ª, 5ª, Dom',
                'horario'   => '11:30',
            ]);
        }
        // Destinos da LANCHA Crystal // 7-28  
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'CADAJÁS',
                'dia'       => 'TERÇA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'COARI',
                'dia'       => 'TERÇA',
                'horario'   => '12:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'TEFÉ',
                'dia'       => 'TERÇA',
                'horario'   => '14:30',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'FONTE BOA',
                'dia'       => 'QUARTA',
                'horario'   => '19:30',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'JUTAÍ',
                'dia'       => 'QUARTA',
                'horario'   => '01:30',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'TONANTINS',
                'dia'       => 'QUARTA',
                'horario'   => '04:30',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'STO. ANTONIO IÇA',
                'dia'       => 'QUARTA',
                'horario'   => '07:30',
            ]);

            Horario::create([
                'origem'   => 'STO. ANTONIO IÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'QUARTA',
                'horario'   => '08:30',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'QUARTA',
                'horario'   => '09:00',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'QUARTA',
                'horario'   => '11:30',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'TABATINGA',
                'dia'       => 'QUARTA',
                'horario'   => '15:30',
            ]);

            Horario::create([
                'origem'   => 'TABATINGA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'QUINTA',
                'horario'   => '09:00',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'QUINTA',
                'horario'   => '09:30',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'QUINTA',
                'horario'   => '13:00',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'STO ANTONIO IÇA',
                'dia'       => 'QUINTA',
                'horario'   => '15:00',
            ]);

            Horario::create([
                'origem'   => 'STO ANTONIO IÇA',
                'destino'   => 'TONANTINS',
                'dia'       => 'QUINTA',
                'horario'   => '17:00',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'JUTAÍ',
                'dia'       => 'QUINTA',
                'horario'   => '18:00',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'FONTE BOA',
                'dia'       => 'QUINTA',
                'horario'   => '20:30',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'TEFÉ',
                'dia'       => 'QUINTA',
                'horario'   => '23:00',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'COARI',
                'dia'       => 'QUINTA',
                'horario'   => '05:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'CODAJÁS',
                'dia'       => 'QUINTA',
                'horario'   => '08:30',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'MANAUS',
                'dia'       => 'QUINTA',
                'horario'   => '11:30',
            ]);
        }
        // DESTINOS DA LANCHA GLORIA DE DEUS III // 29-52
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'CADAJÁS',
                'dia'       => 'SEXTA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'COARI',
                'dia'       => 'SEXTA',
                'horario'   => '12:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'TEFÉ',
                'dia'       => 'SEXTA',
                'horario'   => '15:00',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'ALVARÃES',
                'dia'       => 'SEXTA',
                'horario'   => '19:30',
            ]);

            Horario::create([
                'origem'   => 'ALVARÃES',
                'destino'   => 'FONTE BOA',
                'dia'       => 'SEXTA',
                'horario'   => '20:30',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'JUTAÍ',
                'dia'       => 'SÁBADO',
                'horario'   => '01:30',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'TONANTINS',
                'dia'       => 'SÁBADO',
                'horario'   => '04:30',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'STO. ANTONIO IÇA',
                'dia'       => 'SÁBADO',
                'horario'   => '07:30',
            ]);

            Horario::create([
                'origem'   => 'STO. ANTONIO IÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'SÁBADO',
                'horario'   => '08:30',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'SÁBADO',
                'horario'   => '09:00',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'SÁBADO',
                'horario'   => '11:30',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'TABATINGA',
                'dia'       => 'SÁBADO',
                'horario'   => '15:30',
            ]);

            Horario::create([
                'origem'   => 'TABATINGA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'DOMINGO',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'DOMINGO',
                'horario'   => '08:30',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'DOMINGO',
                'horario'   => '12:30',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'STO ANTONIO IÇA',
                'dia'       => 'DOMINGO',
                'horario'   => '14:30',
            ]);

            Horario::create([
                'origem'   => 'STO ANTONIO IÇA',
                'destino'   => 'TONANTINS',
                'dia'       => 'DOMINGO',
                'horario'   => '16:30',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'JUTAÍ',
                'dia'       => 'DOMINGO',
                'horario'   => '17:30',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'FONTE BOA',
                'dia'       => 'DOMINGO',
                'horario'   => '21:00',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'ALVARÃES',
                'dia'       => 'SEGUNDA',
                'horario'   => '23:30',
            ]);

            Horario::create([
                'origem'   => 'ALVARÃES',
                'destino'   => 'TEFÉ',
                'dia'       => 'SEGUNDA',
                'horario'   => '04:00',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'COARI',
                'dia'       => 'SEGUNDA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'CODAJÁS',
                'dia'       => 'SEGUNDA',
                'horario'   => '09:30',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'MANAUS',
                'dia'       => 'SEGUNDA',
                'horario'   => '12:00',
            ]); 
        }
        //DESTINOS DA LANCHA MADAME CRIS // 53-74
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'CADAJÁS',
                'dia'       => 'TERÇA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'COARI',
                'dia'       => 'TERÇA',
                'horario'   => '12:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'TEFÉ',
                'dia'       => 'TERÇA',
                'horario'   => '14:30',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'FONTE BOA',
                'dia'       => 'QUARTA',
                'horario'   => '19:00',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'JUTAÍ',
                'dia'       => 'QUARTA',
                'horario'   => '01:00',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'TONANTINS',
                'dia'       => 'QUARTA',
                'horario'   => '03:30',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'STO. ANTONIO IÇA',
                'dia'       => 'QUARTA',
                'horario'   => '06:15',
            ]);

            Horario::create([
                'origem'   => 'STO. ANTONIO IÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'QUARTA',
                'horario'   => '07:30',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'QUARTA',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'QUARTA',
                'horario'   => '10:20',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'TABATINGA',
                'dia'       => 'QUARTA',
                'horario'   => '14:30',
            ]);

            Horario::create([
                'origem'   => 'TABATINGA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'QUINTA',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'QUINTA',
                'horario'   => '08:30',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'QUINTA',
                'horario'   => '12:00',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'STO ANTONIO IÇA',
                'dia'       => 'QUINTA',
                'horario'   => '14:00',
            ]);

            Horario::create([
                'origem'   => 'STO ANTONIO IÇA',
                'destino'   => 'TONANTINS',
                'dia'       => 'QUINTA',
                'horario'   => '15:30',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'JUTAÍ',
                'dia'       => 'QUINTA',
                'horario'   => '16:30',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'FONTE BOA',
                'dia'       => 'QUINTA',
                'horario'   => '19:30',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'TEFÉ',
                'dia'       => 'QUINTA',
                'horario'   => '22:00',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'COARI',
                'dia'       => 'QUINTA',
                'horario'   => '02:30',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'CODAJÁS',
                'dia'       => 'QUINTA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'MANAUS',
                'dia'       => 'QUINTA',
                'horario'   => '09:00',
            ]);
        }
        //DESTINOS DA LANCHA MISSONE // 75-82
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'NOVA OLINDA',
                'dia'       => 'TERÇA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'BORBA',
                'dia'       => 'TERÇA',
                'horario'   => '11:00',
            ]);

            Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => 'TERÇA',
                'horario'   => '13:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA ARIPUANÃ',
                'destino'   => 'MANICORÉ',
                'dia'       => 'TERÇA',
                'horario'   => '17:00',
            ]);

            Horario::create([
                'origem'   => 'MANICORÉ',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => 'QUARTA',
                'horario'   => '05:30',
            ]);

            Horario::create([
                'origem'   => 'NOVO ARIPUANÃ',
                'destino'   => 'BORBA',
                'dia'       => 'QUARTA',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVA OLINDA',
                'dia'       => 'QUARTA',
                'horario'   => '11:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'MANAUS',
                'dia'       => 'QUARTA',
                'horario'   => '13:00',
            ]);
        }
        //DESTINOS DA LANCHA ORIXIMINÁ // 83-88
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'PARINTINS',
                'dia'       => 'SEGUNDA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'PARINTINS',
                'destino'   => 'JURUTI',
                'dia'       => 'SEGUNDA',
                'horario'   => '14:00',
            ]);

            Horario::create([
                'origem'   => 'JURUTI',
                'destino'   => 'ORIXIMINÁ',
                'dia'       => 'SEGUNDA',
                'horario'   => '17:00',
            ]);

            Horario::create([
                'origem'   => 'ORIXIMINÁ',
                'destino'   => 'JURUTI',
                'dia'       => 'SEGUNDA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'JURUTI',
                'destino'   => 'PARINTINS',
                'dia'       => 'SEGUNDA',
                'horario'   => '07:00',
            ]);

            Horario::create([
                'origem'   => 'PARINTINS',
                'destino'   => 'MANAUS',
                'dia'       => 'SEGUNDA',
                'horario'   => '08:00',
            ]);
        }
        // DESTINOS DA LANCHA SOBERANA // 89-112
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'CADAJÁS',
                'dia'       => 'DOMINGO',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'COARI',
                'dia'       => 'DOMINGO',
                'horario'   => '12:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'TEFÉ',
                'dia'       => 'DOMINGO',
                'horario'   => '15:30',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'ALVARÃES',
                'dia'       => 'DOMINGO',
                'horario'   => '20:00',
            ]);

            Horario::create([
                'origem'   => 'ALVARÃES',
                'destino'   => 'FONTE BOA',
                'dia'       => 'DOMINGO',
                'horario'   => '21:00',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'JUTAÍ',
                'dia'       => 'SEGUNDA',
                'horario'   => '02:00',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'TONANTINS',
                'dia'       => 'SEGUNDA',
                'horario'   => '05:00',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'STO. ANTONIO IÇA',
                'dia'       => 'SEGUNDA',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'STO. ANTONIO IÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'SEGUNDA',
                'horario'   => '09:00',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'SEGUNDA',
                'horario'   => '11:30',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'SEGUNDA',
                'horario'   => '13:00',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'TABATINGA',
                'dia'       => 'SEGUNDA',
                'horario'   => '17:30',
            ]);

            Horario::create([
                'origem'   => 'TABATINGA',
                'destino'   => 'BENJAMIN CONSTANT',
                'dia'       => 'TERÇA',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'BENJAMIN CONSTANT',
                'destino'   => 'S. P. OLIVENÇA',
                'dia'       => 'TERÇA',
                'horario'   => '08:30',
            ]);

            Horario::create([
                'origem'   => 'S. P. OLIVENÇA',
                'destino'   => 'AMATURÁ',
                'dia'       => 'TERÇA',
                'horario'   => '13:00',
            ]);

            Horario::create([
                'origem'   => 'AMATURÁ',
                'destino'   => 'STO ANTONIO IÇA',
                'dia'       => 'TERÇA',
                'horario'   => '15:00',
            ]);

            Horario::create([
                'origem'   => 'STO ANTONIO IÇA',
                'destino'   => 'TONANTINS',
                'dia'       => 'TERÇA',
                'horario'   => '16:00',
            ]);

            Horario::create([
                'origem'   => 'TONANTINS',
                'destino'   => 'JUTAÍ',
                'dia'       => 'TERÇA',
                'horario'   => '17:00',
            ]);

            Horario::create([
                'origem'   => 'JUTAÍ',
                'destino'   => 'FONTE BOA',
                'dia'       => 'TERÇA',
                'horario'   => '21:00',
            ]);

            Horario::create([
                'origem'   => 'FONTE BOA',
                'destino'   => 'ALVARÃES',
                'dia'       => 'QUARTA',
                'horario'   => '23:30',
            ]);

            Horario::create([
                'origem'   => 'ALVARÃES',
                'destino'   => 'TEFÉ',
                'dia'       => 'QUARTA',
                'horario'   => '04:00',
            ]);

            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'COARI',
                'dia'       => 'QUARTA',
                'horario'   => '05:00',
            ]);

            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'CODAJÁS',
                'dia'       => 'QUARTA',
                'horario'   => '09:30',
            ]);

            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'MANAUS',
                'dia'       => 'QUARTA',
                'horario'   => '12:00',
            ]); 
        }
        //DESTINOS DA LANCHA TAIS HOLANDA // 113-120
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'NOVA OLINDA',
                'dia'       => 'TERÇA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'BORBA',
                'dia'       => 'TERÇA',
                'horario'   => '11:00',
            ]);

            Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => 'TERÇA',
                'horario'   => '13:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA ARIPUANÃ',
                'destino'   => 'MANICORÉ',
                'dia'       => 'TERÇA',
                'horario'   => '17:00',
            ]);

            Horario::create([
                'origem'   => 'MANICORÉ',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => 'QUARTA',
                'horario'   => '05:30',
            ]);

            Horario::create([
                'origem'   => 'NOVO ARIPUANÃ',
                'destino'   => 'BORBA',
                'dia'       => 'QUARTA',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVA OLINDA',
                'dia'       => 'QUARTA',
                'horario'   => '11:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'MANAUS',
                'dia'       => 'QUARTA',
                'horario'   => '13:00',
            ]);
        }
        //DESTINOS DA LANCHA ZÉ HOLANDA // 121-128
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'NOVA OLINDA',
                'dia'       => 'TERÇA',
                'horario'   => '06:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'BORBA',
                'dia'       => 'TERÇA',
                'horario'   => '10:30',
            ]);

            Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => 'TERÇA',
                'horario'   => '12:30',
            ]);

            Horario::create([
                'origem'   => 'NOVA ARIPUANÃ',
                'destino'   => 'MANICORÉ',
                'dia'       => 'TERÇA',
                'horario'   => '16:30',
            ]);

            Horario::create([
                'origem'   => 'MANICORÉ',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => 'QUARTA',
                'horario'   => '05:30',
            ]);

            Horario::create([
                'origem'   => 'NOVO ARIPUANÃ',
                'destino'   => 'BORBA',
                'dia'       => 'QUARTA',
                'horario'   => '08:00',
            ]);

            Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVA OLINDA',
                'dia'       => 'QUARTA',
                'horario'   => '11:00',
            ]);

            Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'MANAUS',
                'dia'       => 'QUARTA',
                'horario'   => '12:30',
            ]);
        }
        //DESTINOS DA LANCHA LANCHA LIMA DE ABREU // 129-138
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'TEFÉ',
                'dia'       => '',
                'horario'   => '05:00',
            ]);
            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'ALVARÃES',
                'dia'       => '',
                'horario'   => '06:00',
            ]);
            Horario::create([
                'origem'   => 'ALVARÃES',
                'destino'   => 'UARINI',
                'dia'       => '',
                'horario'   => '10:30',
            ]);
            Horario::create([
                'origem'   => 'UARINI',
                'destino'   => 'MARAÃ',
                'dia'       => '',
                'horario'   => '12:00',
            ]);
            Horario::create([
                'origem'   => 'MARAA',
                'destino'   => 'JAPURÁ',
                'dia'       => '',
                'horario'   => '17:00',
            ]);
            Horario::create([
                'origem'   => 'JAPURÁ',
                'destino'   => 'MARAA',
                'dia'       => '',
                'horario'   => '11:00',
            ]);
            Horario::create([
                'origem'   => 'MARAÃ',
                'destino'   => 'ALVARÃES',
                'dia'       => '',
                'horario'   => '16:00',
            ]);
            Horario::create([
                'origem'   => 'ALVARÃES',
                'destino'   => 'TEFÉ',
                'dia'       => '',
                'horario'   => '17:00',
            ]);
            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'MANAUS',
                'dia'       => '',
                'horario'   => '05:00',
            ]);
        }
        //DESTINOS DA LANCHA BELISSIMA // 139-149
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'CODAJÁS',
                'dia'       => 'QUARTA',
                'horario'   => '12:00',
            ]);
            Horario::create([
                'origem'   => 'CODAJÁS',
                'destino'   => 'COARI',
                'dia'       => 'QUARTA',
                'horario'   => '15:30',
            ]);
            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'TEFÉ',
                'dia'       => 'QUARTA',
                'horario'   => '20:30',
            ]);
            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'JURUÁ',
                'dia'       => 'QUINTA',
                'horario'   => '08:00',
            ]);
            Horario::create([
                'origem'   => 'JURÚA',
                'destino'   => 'CARAUARI',
                'dia'       => 'QUINTA',
                'horario'   => '17:30',
            ]);
            Horario::create([
                'origem'   => 'CARAUARI',
                'destino'   => 'JURUÁ',
                'dia'       => 'SEXTA',
                'horario'   => '11:00',
            ]);
            Horario::create([
                'origem'   => 'JURUÁ',
                'destino'   => 'TEFÉ',
                'dia'       => 'SÁBADO',
                'horario'   => '05:00',
            ]);
            Horario::create([
                'origem'   => 'TEFÉ',
                'destino'   => 'COARI',
                'dia'       => 'SÁBADO',
                'horario'   => '09:00',
            ]);
            Horario::create([
                'origem'   => 'COARI',
                'destino'   => 'CODAJÁS',
                'dia'       => 'SÁBADO',
                'horario'   => '12:00',
            ]);
            Horario::create([
                'origem'   => 'CPDAJÁS',
                'destino'   => 'MANAUS',
                'dia'       => 'SÁBADO',
                'horario'   => '17:30',
            ]);
        }
        //DESTINOS DA LANCHA PÉROLA // 150-158
        {
            Horario::create([
                'origem'   => 'MANAUS',
                'destino'   => 'NOVA OLINDA',
                'dia'       => '2ª E 5ª',
                'horario'   => '11:00',
            ]);
            Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'BORBA',
                'dia'       => '2ª E 5ª',
                'horario'   => '13:30',
            ]);
            Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => '2ª E 5ª',
                'horario'   => '17:00',
            ]);
            Horario::create([
                'origem'   => 'NOVO ARIPUANÃ',
                'destino'   => 'MANICORÉ',
                'dia'       => '2ª E 5ª',
                'horario'   => '21:00',
            ]);
             Horario::create([
                'origem'   => 'MANICORÉ',
                'destino'   => 'NOVO ARIPUANÃ',
                'dia'       => '3ª E SÁBADO',
                'horario'   => '08:00',
            ]);
             Horario::create([
                'origem'   => 'NOVO ARIPUANÃ',
                'destino'   => 'BORBA',
                'dia'       => '3ª E SÁBADO',
                'horario'   => '11:00',
            ]);
             Horario::create([
                'origem'   => 'BORBA',
                'destino'   => 'NOVA OLINDA',
                'dia'       => '3ª E SÁBADO',
                'horario'   => '13:30',
            ]);
             Horario::create([
                'origem'   => 'NOVA OLINDA',
                'destino'   => 'MANAUS',
                'dia'       => '3ª E SÁBADO',
                'horario'   => '18:30',
            ]);
        }
    }

}
