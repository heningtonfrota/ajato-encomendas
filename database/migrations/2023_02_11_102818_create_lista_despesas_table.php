<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListaDespesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lista_despesas', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('relatorio_id')->length(10)->unsigned();
            $table->foreign('relatorio_id')->references('id')->on('relatorios');

            $table->text('descricao');
            $table->float('valor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lista_despesas');
    }
}
