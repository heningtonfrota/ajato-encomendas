@extends('sistema.layout.index')

@section('titulo', 'Cadastro de Encomendas')

@section('conteudo')

<div class="container-fluid d-flex justify-content-center">
    <div class="card container col-sm bg-dark">
        <h2 class="card-header text-white text-center">Registro de Encomenda</h2>
        <div class="card-body">
            <form action="{{route('encomenda.cadastro')}}" method="post">
                @csrf
                <div class="form-row my-3">
                    <div class="col-sm-2">
                        <input type="number" class="form-control" value="{{ $remetente->cpf ?? '' }}" name="remetente[cpf]" placeholder="CPF/CNPJ Remetente">
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" value="{{ $remetente->tel_remetente ?? '' }}" name="remetente[tel_remetente]" placeholder="Telefone Do Remetente">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control mb-2" value="{{ $remetente->remetente ?? '' }}" name="remetente[remetente]" placeholder="Nome do Remetente">
                    </div>
                </div>

                <div class="form-row my-3">
                    <div class="col-sm-2">
                        <input type="number" class="form-control" value="{{ $destinatario->cpf_destinatario ?? '' }}" name="destinatario[cpf_destinatario]" placeholder="CPF/CNPJ Destinatário">
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" value="{{ $destinatario->tel_destinatario ?? '' }}" name="destinatario[tel_destinatario]" placeholder="Telefone Do Destinatário">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control mb-2" value="{{ $destinatario->destinatario ?? '' }}" name="destinatario[destinatario]" placeholder="Nome do Destinatário">
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

            @isset ($remetente)
                <div class="alert alert-success" role="alert">
                    <h5>
                        Remetente: {{ $remetente->remetente ?? 'Não Encontrado' }} 
                        - CPF: {{ $remetente->cpf ?? 'Não Encontrado' }} 
                        - Telefone: {{ $remetente->tel_remetente ?? 'Não Encontrado' }}, 
                        E FORAM PREENCHIDOS AUTOMATICAMENTE!
                    </h5>
                </div>
            @endisset

            @isset ($destinatario)
                <div class="alert alert-success" role="alert">
                    <h5>
                        Destinatário: {{ $destinatario->destinatario ?? 'Não Encontrado' }} 
                        - CPF: {{ $destinatario->cpf_destinatario ?? 'Não Encontrado' }} 
                        - Telefone: {{ $destinatario->tel_destinatario ?? 'Não Encontrado' }}, 
                        E FORAM PREENCHIDOS AUTOMATICAMENTE!
                    </h5>
                </div>
            @endisset

            <hr class="bg-white">

            <form name="formCad" id="formCad" method="post" action="{{route('encomenda.store')}}">
                @csrf
                <div class="row">
                    <div class="col-sm-10">
                        <span class="text-white">Informações da Encomenda</span>
                        <input type="text" name="descricao" class="form-control mb-2" id="validationCustom01" placeholder="Descrição" required>
                    </div>
                    <div class="col-sm-2">
                        <span class="text-white">Volume</span>
                        <input type="text" name="volume" class="form-control mb-2" id="validationCustom08" placeholder="" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <span class="text-white">Nome do Remetente</span>
                        <input type="text" name="remetente" value="{{ $remetente->remetente ?? '' }}" class="form-control mb-2 font-weight-bold" required>
                    </div>
                    <div class="col-sm">
                        <span class="text-white">CPF/CNPJ</span>
                        <input type="text" name="cpf" value="{{ $remetente->cpf ?? '' }}" class="form-control mb-2 font-weight-bold" required>
                    </div>
                    <div class="col-sm">
                        <span class="text-white">Tel. Remetente</span>
                        <input type="text" name="tel_remetente" value="{{ $remetente->tel_remetente ?? '' }}" class="form-control mb-2 font-weight-bold" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <span class="text-white">Nome do Destinatário</span>
                        <input type="text" name="destinatario" value="{{ $destinatario->destinatario ?? '' }}" class="form-control mb-2" placeholder="Recomendado o Uso de até 18 Caracteres" required>
                    </div>
                    <div class="col-sm">
                        <span class="text-white">CPF/CNPJ do Destinatário</span>
                        <input type="text" name="cpf_destinatario" value="{{ $destinatario->cpf_destinatario ?? '' }}"class="form-control mb-2" maxlength="14">
                    </div>
                    <div class="col-sm">
                        <span class="text-white">Tel. do Destinatário</span>
                        <input type="text" name="tel_destinatario" value="{{ $destinatario->tel_destinatario ?? '' }}" class="form-control mb-2" maxlength="15">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <span class="text-white">Lancha de Transporte</span>
                        <select class="form-control mb-2" name="lancha" id="validationCustom08" required>
                            <option selected disabled value="">Selecione a Lancha...</option>
                            @foreach($lancha as $lanchas)
                                <option value="{{ $lanchas->id }}">{{ $lanchas->nome }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm">
                        <span class="text-white">Destino da Lancha</span>
                        <select class="form-control mb-2" name="cidade_destino" id="validationCustom08" required>
                            <option selected disabled value="">Selecione o Destino</option>
                            @foreach($municipio as $municipios)
                                <option value="{{ $municipios->id }}">{{ $municipios->nome }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <span class="text-white">Forma do Pagamento</span>
                        <select class="form-control mb-2" name="status" id="validationCustom08" required>
                            <option selected disabled value="">Selecione o Pagamento...</option>
                            <option value="Pago">Pago</option>
                            <option value="FPG">FPG</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <span class="text-white">Valor da Encomenda</span>
                        <input type="text" name="valor" class="form-control mb-2" id="validationCustom07" placeholder="Valor. Ex: 1,200.55" required>
                    </div>
                    <div class="col-sm-4  pb-3">
                        <span class="text-white">Data do Envio</span>
                        <input type="date" name="data" class="form-control mb-2" id="validationCustom05" placeholder="Data" required>
                    </div>
                </div>
                <button class="btn btn-warning btn-lg btn-block" type="submit">Registrar 
                    <i class="fas fa-arrow-circle-right"></i>
                </button>
            </form>
        </div>
        <small class="text-center text-white pb-3">Todos os campos são obrigatorios!</small>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
    $(function () {
        $("#assunto").autocomplete({
            source: '{{route('autocomplete')}}'
        });
    });
</script>
@endsection