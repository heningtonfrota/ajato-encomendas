<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Encomenda;
use App\Models\Relatorio;
use App\Models\Lancha;
use App\Models\Municipio;
use App\Models\ResetEncomendas;
use App\User;
use Carbon\Carbon;

class RelatorioController extends Controller
{
  public function __construct(Lancha $lancha, Municipio $municipio)
  {
    $this->objMunicipio = $municipio->where('status', 'A');
    $this->objLancha = $lancha->where('status', 'A');
  }

  public function index()
  {
    $user = Auth()->User();
    return view('sistema.relatorio.index', compact('user'));
  }

  public function historico()
  {
    $user = Auth()->User();
    $historico = Relatorio::orderBy('id', 'DESC')->paginate(10);
    return view('sistema.relatorio.historico', compact('user', 'historico'));
  }

  public function show($id)
  {
    $user = Auth()->User();
    $relatorio = Relatorio::find($id);
    $dia = $relatorio->data;
    $id_lancha = $relatorio->id_lancha;
    $lancha_id = Encomenda::where('data', $dia)->where('lancha_id', $id_lancha)->value('lancha_id');
    if ($id_lancha == NULL) {
      $encomenda = Encomenda::where('data', $dia)->orderBy('id', 'DESC')->paginate(500);
    } else {
      $encomenda = Encomenda::where('data', $dia)->where('lancha_id', $id_lancha)->orderBy('id', 'DESC')->paginate(500);
    }
    $nome_lancha = Lancha::where('id', $id_lancha)->value('nome');
    return view('sistema.relatorio.vizualizar', compact('user', 'relatorio', 'dia', 'encomenda', 'id_lancha', 'nome_lancha'));
  }

  public function relatorioView(Request $req)
  {
    $form_data = $req->data;
    $user = Auth()->User();
    $lancha = $this->objLancha->get();

    $encomenda          = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->get();
    $reset_encomendas   = ResetEncomendas::orderBy('reset_id', 'DESC')->where('data', $form_data)->get();
    $all_encomendas     = $reset_encomendas->mergeRecursive($encomenda);

    $encomenda_total    = $all_encomendas->sum('valor');
    $encomenda_pago     = $all_encomendas->where('status', "Pago")->sum('valor');
    $encomenda_receber  = $all_encomendas->where('status', "FPG")->sum('valor');
    $ajato10            = $all_encomendas->sum('valor') * 0.1;
    $canoeiro           = $all_encomendas->where('status', "Pago")->sum('valor') - $ajato10;

    if ($canoeiro < 0) {
      $canoeiro = $encomenda_receber - $ajato10;
    }
    return view('sistema.relatorio.relatorio', compact('user', 'all_encomendas', 'lancha', 'encomenda_total', 'encomenda_pago', 'encomenda_receber', 'ajato10', 'canoeiro', 'form_data'));
  }

  public function relatorio()
  {
    $user = Auth()->User();
    $lancha = $this->objLancha->get();
    $encomenda = Encomenda::orderBy('id', 'DESC')->paginate(15);

    $encomenda_total    = 0;
    $encomenda_pago     = 0;
    $encomenda_receber  = 0;
    $ajato10            = 0;
    $canoeiro           = 0;

    return view('sistema.relatorio.relatorio', compact('user', 'encomenda', 'lancha', 'encomenda_total', 'encomenda_pago', 'encomenda_receber', 'ajato10', 'canoeiro'));
  }

  public function relatorioDestinoView(Request $req)
  {
    $user             = Auth()->User();
    $lancha           = $this->objLancha->get();
    $municipio        = $this->objMunicipio->get();
    $form_data        = $req->data;
    $form_lancha      = $req->lancha;
    $form_municipio   = $req->municipio;

    $encomenda_lancha     = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('lancha_id');
    $encomenda_data       = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('data');
    $encomenda_municipio  = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('municipio_id');

    $reset_lancha     = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('lancha_id');
    $reset_data       = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('data');
    $reset_municipio  = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->value('municipio_id');

    if ($form_municipio == $encomenda_municipio || $form_municipio == $reset_municipio) {
      $encomenda  = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->get();
      $reset      = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->where('municipio_id', $form_municipio)->get();
      $all_encomenda = $encomenda->mergeRecursive($reset);
      $mensagem = false;
    } else {
      $encomenda = NULL;
      $mensagem = true;
    }

    $ref_lancha      = Lancha::where('id', $form_lancha)->value('nome');
    $ref_municipio   = Municipio::where('id', $form_municipio)->value('nome');
    return view('sistema.relatorio.relatorio_destino', compact('user', 'lancha', 'municipio', 'all_encomenda', 'form_data', 'form_lancha', 'form_municipio', 'ref_lancha', 'mensagem', 'ref_municipio'));
  }

  public function relatorioDestino()
  {
    $user = Auth()->User();
    $lancha = $this->objLancha->get();
    $municipio = $this->objMunicipio->get();
    $encomenda = NULL;
    $mensagem = true;
    return view('sistema.relatorio.relatorio_destino', compact('user', 'lancha', 'municipio', 'encomenda'));
  }
  public function relatorioDestinoTodosView(Request $req)
  {
    $user = Auth()->User();
    $lancha = $this->objLancha->get();
    $form_data       = $req->data;
    $form_lancha     = $req->lancha;
    $encomenda            = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->paginate(500);
    $encomenda_lancha     = Encomenda::orderBy('id', 'DESC')->where('lancha_id', $form_lancha)->value('lancha_id');
    $encomenda_data       = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->value('data');

    $reset_lancha     = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->value('lancha_id');
    $reset_data       = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->value('data');
    $reset_municipio  = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->value('municipio_id');

    if ($form_data == $encomenda_data) {
        $encomenda    = Encomenda::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
        $reset        = ResetEncomendas::orderBy('id', 'DESC')->where('data', $form_data)->where('lancha_id', $form_lancha)->get();
        $all_encomenda = $encomenda->mergeRecursive($reset);
        $mensagem = false;
    } else {
        $all_encomenda = null;
        $encomenda = NULL;
        $mensagem = true;
    }
    $ref_lancha = Lancha::where('id', $form_lancha)->value('nome');
    return view('sistema.relatorio.relatorio_destino_todos', compact('user', 'lancha', 'all_encomenda', 'form_data', 'form_lancha', 'ref_lancha', 'mensagem'));
  }

  public function relatorioDestinoTodos()
  {
    $user = Auth()->User();
    $lancha = $this->objLancha->get();
    $encomenda = NULL;
    return view('sistema.relatorio.relatorio_destino_todos', compact('user', 'lancha', 'encomenda'));
  }

    public function relatorioValorRecebido(Request $request)
    {
        $user = Auth()->User();
        $usuarios = User::where('id', '!=', 1)->get();
        $lanchas = $this->objLancha->get();

        return view('sistema.relatorio.valores_recebidos', compact('user', 'usuarios','lanchas'));
    }

    public function buscarRelatorioValorRecebido(Request $request)
    {
        $request->validate([
            'usuario' => ['required'],
            'data_inicial' => ['required']
        ]);

        $user = Auth()->User();
        $usuarios = User::where('id', '!=', 1)->get();
        $lanchas = $this->objLancha->get();

        $dados_encomendas = Encomenda::where('user_id', $request->usuario)
            ->when(
                isset($request->lancha),
                function ($query) use ($request) {
                    return $query->where('lancha_id', $request->lancha);
                }
            )
            ->when(
                isset($request->data_final),
                function ($query) use ($request) {
                    return $query->whereBetween('data', [$request->data_inicial, $request->data_final]);
                },
                function ($query) use ($request) {
                    return $query->where('data', $request->data_inicial);
                }
            )
            ->selectRaw('status, sum(valor) as valor')
            ->groupBy('status')
            ->get();

        $dados_reset = ResetEncomendas::where('user_id', $request->usuario)
            ->when(isset($request->lancha), function ($query) use ($request) {
                return $query->where('lancha_id', $request->lancha);
            })
            ->when(
                isset($request->data_final),
                function ($query) use ($request) {
                    return $query->whereBetween('data', [$request->data_inicial, $request->data_final]);
                },
                function ($query) use ($request) {
                    return $query->where('data', $request->data_inicial);
                }
            )
            ->selectRaw('status, sum(valor) as valor')
            ->groupBy('status')
            ->get();

        $dados = (object)[
            'usuario' => User::find($request->usuario)->name,
            'periodo_inicial' => Carbon::parse($request->data_inicial)->format('d/m/Y'),
            'periodo_final' => Carbon::parse($request->data_final)->format('d/m/Y'),
        ];

        return view('sistema.relatorio.valores_recebidos', compact('user', 'usuarios', 'lanchas', 'dados', 'dados_reset', 'dados_encomendas'));
    }
}
