<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelatoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relatorios', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->integer('id_lancha')->nullable();
            $table->double('total_diario', 100, 2);
            $table->double('total_pago', 100, 2);
            $table->double('total_receber', 100, 2);
            $table->double('porcentagem', 100, 2);
            $table->double('embarcacao', 100, 2);
            $table->date('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relatorios');
    }
}
