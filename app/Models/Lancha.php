<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lancha extends Model
{
  protected $table = 'lanchas';

  protected $fillable = ['nome'];

  public function relEncomenda()
  {
    return $this->hasMany('App\Models\Encomenda', 'lancha_id');
  }
}
