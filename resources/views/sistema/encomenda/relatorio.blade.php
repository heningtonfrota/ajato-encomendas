@extends('sistema.layout.index')

@section('titulo', 'Relatorio')

@section('conteudo')
    <div class="card container col-sm-8">
        <h1 class="card-header text-center">Relatório Por Lancha</h1>

        <form name="formCad" class="pt-3 m-auto" id="formCad" method="post"
            action="{{ route('encomenda.relatorioview') }}">
            @csrf
            <div class="form-row">
                <div class="col-sm-5">
                    <input type="date" name="data" class="form-control" placeholder="Data">
                </div>
                <div class="col-sm-5">
                    <select class="form-control" name="lancha" id="validationCustom08" required>
                        <option selected disabled value="">Selecione a Lancha...</option>
                        @foreach ($lancha as $lanchas)
                            <option value="{{ $lanchas->id }}">{{ $lanchas->nome }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-warning" type="submit"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>

        <div class="row my-2">
            <div class="@if ($encomenda_total == 0) col-md-12 @else col-md-7 @endif col-sm-12 border-right">
                @if ($encomenda_total != 0)
                    @foreach ($all_encomendas as $encomendas)
                        @php
                            $lancha = $encomendas->find($encomendas->id)->relLancha;
                        @endphp
                    @endforeach
                    <hr>
                    <h2 class="text-center m-0">Lancha: {{ $lancha->nome }}</h2>
                    <hr>
                    <form class="pb-3">
                        <div class="row text-center">
                            <div class="col-sm-12">
                                <label class="font-weight-bold h5">Valor Total</label>
                                <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg"
                                    value="R$ {{ number_format($encomenda_total, 2) }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label class="font-weight-bold h5">Pagamento na Entrega</label>
                                <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg"
                                    value="R$ {{ number_format($encomenda_receber, 2) }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label class="font-weight-bold h5">Pago</label>
                                <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg"
                                    value="R$ {{ number_format($encomenda_pago, 2) }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                @if ($lancha->nome == 'AJATO 2000')
                                    <label class="font-weight-bold h5">Ajato 5%</label>
                                @else
                                    <label class="font-weight-bold h5">Ajato 10%</label>
                                @endif
                                <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg"
                                    value="R$ {{ number_format($ajato10, 2) }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label class="font-weight-bold h5">Valor das Embarcações</label>
                                <input type="text" style="font-size: 30px;" class="text-center form-control form-control-lg"
                                    value="R$ {{ number_format($canoeiro, 2) }}" readonly>
                            </div>
                        </div>
                    </form>
                @elseif(isset($form_lancha) && $encomenda_total == 0)
                    <h3 class="text-center bg-danger my-3 p-2">Seleção não retornou resultado!</h3>
                @else
                    <h3 class="text-center bg-light my-3 p-2">Selecione a Data e Lancha nos campos acima!</h3>
                @endif

                <ul class="nav justify-content-center py-2">
                    <li><a href="{{ route('sistema.relatorio.index') }}" class="btn btn-danger btn-lg mx-4"><i
                                class="fas fa-arrow-left"></i> Voltar</a></li>
                    @if (isset($form_data))
                        <li>
                            <form id="form-gerar-relatorio">
                                @csrf
                                <input type="hidden" name="data" value="{{ $form_data }}">
                                <input type="hidden" name="lancha" value="{{ $form_lancha }}">
                                @if (isset($form_data) && $encomenda_total != 0)
                                    <button
                                        class="btn btn-info btn-lg mx-4"
                                        id="load1"
                                        data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing Order"
                                        onclick="submitRelatorio(event)">Gerar Relatório <i class="far fa-file-pdf"></i>
                                    </button>
                                @endif
                            </form>
                        </li>
                    @endif
                </ul>
            </div>

            <div class="col-md-5 col-sm-12">
                @if(isset($form_lancha) && $encomenda_total != 0)
                    <hr>
                    <h2 class="text-center m-0">Lista de Despesas</h2>
                    <hr>
                    <div id="contacts">
                        <div class="row mx-auto pb-2">
                            <input type="hidden" id="id-field" />
                            <input type="text" id="valor-field" class="col-5 py-1" placeholder="Valor" />
                            <input type="text" id="descricao-field" class="col-5 py-1" placeholder="Descrição" />
                            <button id="add-btn" class="col-2 btn btn-success"><i class="fas fa-plus"></i></button>
                            <button id="edit-btn" class="col-2 btn btn-warning"><i class="fas fa-edit"></i></button>
                        </div>

                        <table class="col-md-12 col-sm-6 table table-sm mx-auto">
                            <thead>
                                <tr>
                                    <th class="sort" data-sort="valor">Valor</th>
                                    <th class="sort" data-sort="descricao">Descricao</th>
                                    <th class="sort" colspan="2">Ações</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if (isset($form_data) && $encomenda_total != 0)
            <div class="d-flex justify-content-center py-2">
                <h3 class="text-center text-uppercase">
                    Relatorio referente as Encomendas de Todas as Lanchas na Data:
                    <i>{{ \Carbon\Carbon::parse($form_data)->format('d/m/Y') }}</i>
                </h3>
            </div>
        @endif
    </div>
@endsection

@push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#valor-field').mask('000.000.000.000.000,00', {reverse: true});
        });

        var options = {
            valueNames: ['id', 'valor', 'descricao'],
            item: '<tr><td class="id" style="display:none;"></td><td class="valor" width="20%"></td><td class="descricao"></td><td class="edit" width="10%"><button class="btn btn-warning"><i class="fas fa-edit edit-item-btn"></i></button></td><td class="remove" width="10%"><button class="remove-item-btn btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>'
        };

        // Init list
        var contactList = new List('contacts', options);

        var idField = $('#id-field'),
            valorField = $('#valor-field'),
            descricaoField = $('#descricao-field'),
            addBtn = $('#add-btn'),
            editBtn = $('#edit-btn').hide(),
            removeBtns = $('.remove-item-btn'),
            editBtns = $('.edit-item-btn');

        var list = JSON.parse('<?php echo $despesas; ?>');
        var tabela_registro_relatorio = JSON.parse('<?php echo $relatorio; ?>');

        list.forEach(l => {
            contactList.add({
                id: l.id,
                valor: l.valor,
                descricao: l.descricao
            });

            clearFields();
            refreshCallbacks();
        });

        // Sets callbacks to the buttons in the list
        refreshCallbacks();

        addBtn.click(function() {
            const ultimo_max_id = list.length > 0 ? list[list.length - 1].id + 1 : 1;
            const item = {
                id: ultimo_max_id,
                relatorio_id: tabela_registro_relatorio.id,
                valor: parseFloat(valorField.val()),
                descricao: descricaoField.val()
            }

            if (item.valor == '' || item.descricao == '') return;

            contactList.add(item);
            list.push(item);

            clearFields();
            refreshCallbacks();

            axios.post(`/encomenda/lista-despesas`, { item });
        });

        editBtn.click(function() {
            var item = contactList.get('id', idField.val())[0];
            objIndex = list.findIndex((obj => obj.id == idField.val()));

            list[objIndex].valor = valorField.val();
            list[objIndex].descricao = descricaoField.val();

            item.values({
                id: idField.val(),
                valor: valorField.val(),
                descricao: descricaoField.val()
            });

            clearFields();
            editBtn.hide();
            addBtn.show();

            axios.put(`/encomenda/lista-despesas/atualizar/${list[objIndex].id}`, { item: list[objIndex] });
        });



        function refreshCallbacks() {
            // Needed to add new buttons to jQuery-extended object
            removeBtns = $(removeBtns.selector);
            editBtns = $(editBtns.selector);

            removeBtns.click(function() {
                var itemId = $(this).closest('tr').find('.id').text();
                contactList.remove('id', itemId);
                list = list.filter(list => list.id != itemId);
                axios.delete(`/encomenda/lista-despesas/deletar/${itemId}`);
            });

            editBtns.click(function() {
                var itemId = $(this).closest('tr').find('.id').text();
                var itemValues = contactList.get('id', itemId)[0].values();

                idField.val(itemValues.id);
                valorField.val(itemValues.valor);
                descricaoField.val(itemValues.descricao);

                editBtn.show();
                addBtn.hide();
            });
        }

        function clearFields() {
            idField.val('');
            valorField.val('');
            descricaoField.val('');
        }

        function submitRelatorio(e) {
            e.preventDefault();
            const dados = {
                _token: document.querySelectorAll("#form-gerar-relatorio > input")[0].value,
                data: document.querySelectorAll("#form-gerar-relatorio > input")[1].value,
                lancha: document.querySelectorAll("#form-gerar-relatorio > input")[2].value,
                listaDespesa: list
            };
            axios.get("{{ route('pdf.relatorio_individual') }}", {
                params: dados
            })
            .then((response) => {
                window.open(response.request.responseURL);
            });
        }
    </script>
@endpush
