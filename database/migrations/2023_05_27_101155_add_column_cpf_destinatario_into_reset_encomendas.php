<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCpfDestinatarioIntoResetEncomendas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reset_encomendas', function (Blueprint $table) {
            $table->string('cpf_destinatario', 14)->nullable()->after('tel_remetente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reset_encomendas', function (Blueprint $table) {
            $table->dropColumn('cpf_destinatario');
        });
    }
}
