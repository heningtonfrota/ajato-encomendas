@extends('sistema.layout.index')

@section('titulo', 'Configuração das Lanchas')

@section('conteudo')
	<div class="mx-0">
		<div class="float-left">
			<h1 class="pl-2">Gestão de usuários</h1>
		</div>

		<div class="float-left pl-2 py-2">			
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal" onclick="limparUsuarioDoModal()">
				<i class="fas fa-plus"></i> 
				<span class="ml-1">Novo</span>
			</button>
			
			<!-- Modal -->
			<div class="modal fade" id="modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="modalLabel">Cadastro de Usuário</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="fecharModal()">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
	
						<div class="modal-body">
							<form id="form-action">
								<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}" />

								<div class="form-group">
									<label for="nome">Nome</label>
									<input type="text" class="form-control" value="{{ old('name') }}" name="name" id="nome" placeholder="Maria da Silva">
								</div>
	
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" class="form-control" value="{{ old('email') }}" name="email" id="email" placeholder="mariasilva@ajatoencomendas">
								</div>
	
								<div class="form-group">
									<label for="password">Senha</label>
									<input type="password" class="form-control" value="{{ old('password') }}" name="password" id="password">
								</div>

								<div class="form-group">
									<div class="form-check">
									  <input class="form-check-input" type="checkbox" id="is_admin" name="is_admin">
									  <label class="form-check-label" for="is_admin">
										Perfil Administrador?
									  </label>
									</div>
								</div>

								<div class="form-group">
									<div class="form-check">
									  <input class="form-check-input" type="checkbox" id="status" name="status">
									  <label class="form-check-label" for="status">
										Perfil Ativo?
									  </label>
									</div>
								</div>
							</form>
						</div>
	
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="fecharModal()">Fechar</button>
							<button type="submit" id="btn-form-submit" class="btn btn-success">Salvar</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="float-right pt-2 pr-2">
			@if(isset($filtros))
				{{ $usuarios->appends($filtros)->links() }}
			@else
				{{ $usuarios->links() }}
			@endif
		</div>
	</div>
	
	<div class="clearfix"></div>

	<div id="tabela-gestao-usuarios">		
		<table>
			<thead>
				<tr>
					<th>Nome</th>
					<th>Email</th>
					<th>Admin</th>
					<th>Status</th>
					<th>Criação</th>
					<th>Atualização</th>
					<th>Ações</th>
				</tr>
			</thead>
	
			<tbody>
				@foreach ($usuarios as $usuario)
					<tr>
						<td style="width: 25%">{{ $usuario->name }}</td>
						<td style="width: 25%">{{ $usuario->email }}</td>
						<td style="width: 5%">{{ $usuario->is_admin == 'S' ? 'Sim' : 'Não' }}</td>
						<td style="width: 5%">{{ $usuario->status == 'A' ? 'Ativo' : 'Inativo' }}</td>
						<td style="width: 15%">{{ \Carbon\Carbon::parse($usuario->created_at)->format('d/m/Y h:m:s') }}</td>
						<td style="width: 15%">{{ \Carbon\Carbon::parse($usuario->updated_at)->format('d/m/Y h:m:s') }}</td>
						<td style="width: 10%">
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal" onclick="editarUsuario({{ $usuario }})">
								<i class="fas fa-edit"></i>
							</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection

@push('js')
	<script>
		let editando = false;
		let user_id = 0;

		$( "#btn-form-submit" ).click(function() {	
			let url = editando ? `{{ route('sistema.configuracoes.editar_usuario') }}` : "{{ route('sistema.configuracoes.novo_usuario') }}";
			
			$("#form-action").attr('action', url);

			$("#form-action").submit();
		});
		
		function fecharModal() {
			editando = false;
			limparUsuarioDoModal();
			$("#modal").modal('toggle');
		}
		
		function editarUsuario(usuario) {
			editando = true;
			user_id = usuario.id;

			$("#user_id").remove();
			$("#form-action").append(`<input type="hidden" id="user_id" name="user_id" value="${usuario.id}" />`);

			$("#btn-form-submit").html('Atualizar');
			$("#nome").val(usuario.name); 
			$("#email").val(usuario.email); 

			let is_admin = usuario.is_admin == 'S' ? true : false;
			let status = usuario.status == 'A' ? true : false;
			$("#is_admin").prop('checked', is_admin); 
			$("#status").prop('checked', status); 
		}

		function limparUsuarioDoModal() {
			$("#nome").val('');
			$("#email").val('');
			$("#password").val('');

			$("#is_admin").prop('checked', false); 
			$("#status").prop('checked', false); 
		}
	</script>
@endpush

@push('estilos-local')
	<style>
		#tabela-gestao-usuarios {
			height: calc(100vh - 160px);
			border: 2px solid rgb(177, 177, 177, 0.5);
			border-radius: 10px !important;
		}
		
		#tabela-gestao-usuarios table {
			width: 100%;
		}

		#tabela-gestao-usuarios table thead {
			border-bottom: 2px solid rgb(177, 177, 177, 0.5);
		}
		
		#tabela-gestao-usuarios table thead tr th, 
		#tabela-gestao-usuarios table tbody tr td {
			height: 58.5px;
			padding-left: 10px;
		}

		#tabela-gestao-usuarios tr:nth-child(even) {
			background-color: #f2f2f2;
		}
	</style>
@endpush