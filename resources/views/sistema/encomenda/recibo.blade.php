 <!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <link rel="stylesheet" href="css/recibo.css">
    <link rel="sortcut icon" href="img/ajato_mini.png" type="image/x-icon" />

    <title>Etiqueta nº {{$encomenda->reset_id ? $encomenda->reset_id : $encomenda->id}}</title>
  </head>
  <body>
    @php
        $lancha = $encomenda->find($encomenda->id)->relLancha;
        $municipio = $encomenda->find($encomenda->id)->relMunicipio;
    @endphp
    <div>
      <h2 id="remetente">{{ Str::limit($encomenda->remetente, 24)}}</h2>
      <h2 id="cpf">{{$encomenda->cpf}}</h2>
      <h2 id="pg">R$ {{number_format( $encomenda->valor, 2) }}</h2>
      <h2 id="envio">ENVIO DE ENCOMENDA Nº {{$encomenda->reset_id ? $encomenda->reset_id : $encomenda->id}}</h2>
      <h2 id="envio2">NA LANCHA {{$lancha->nome}} - DESTINO: {{$municipio->nome}}</h2>
      <h2 id="extenso">{{$extenso}}</h2>
    </div>
  </body>
</html>
