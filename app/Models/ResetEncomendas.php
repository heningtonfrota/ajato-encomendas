<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResetEncomendas extends Model
{
    protected $fillable = ['reset_id', 'lancha_id', 'municipio_id', 'user_id', 'descricao', 'volume', 'remetente', 'cpf', 'tel_remetente', 'destinatario', 'valor', 'status', 'data'];
    protected $guarded = [];

    public static function pesquisar(array $filtro)
    {
        $resultado = ResetEncomendas::where(function ($query) use ($filtro) {
            if (isset($filtro['id'])) {
                $query->where('reset_id', 'LIKE', "%{$filtro['id']}%");
            }
            if (isset($filtro['destinatario'])) {
                $query->where('destinatario', 'LIKE', "%{$filtro['destinatario']}%");
            }
        })
            ->paginate();
        return $resultado;
    }

    public function relLancha()
    {
        return $this->hasOne('App\Models\Lancha', 'id', 'lancha_id');
    }

    public function relMunicipio()
    {
        return $this->hasOne('App\Models\Municipio', 'id', 'municipio_id');
    }
    
    public function relUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
