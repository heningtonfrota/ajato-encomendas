<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
      return view('sistema.welcome');
    }

    public function inicial()
    {
      $user = Auth()->User();
      return view('sistema.home', compact('user'));
    }

    public function logout()
    {
      Auth::logout();
      return view('sistema.welcome');
    }
}
