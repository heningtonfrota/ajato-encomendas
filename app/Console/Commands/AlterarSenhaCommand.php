<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class AlterarSenhaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alterar:senha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para alterar senha, deve ser informado id do usuario e a nova senha';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $usuarios = User::get(['id', 'name']);

        foreach ($usuarios as $key => $usuario) {
            $this->info($usuario['id'] . ' - ' . $usuario['name']);
        }

        $id_usuario = $this->ask('Informe o id do usuario?');

        $nova_senha = $this->ask('Informe a nova senha: ');

        $atualizar_usuario = User::find($id_usuario);

        $atualizar_usuario->password = bcrypt($nova_senha);

        $atualizar_usuario->save();

        $this->info("Senha alterada com sucesso!");
    }
}
