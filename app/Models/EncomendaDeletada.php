<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EncomendaDeletada extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id_deletado',
        'lancha_id',
        'municipio_id',
        'user_id',
        'descricao',
        'volume',
        'remetente',
        'cpf',
        'tel_remetente',
        'destinatario',
        'valor',
        'status',
        'data',
        'user_id_deleted',
        'data_criacao',
        'deleted_at'
    ];

    public function relLancha()
    {
        return $this->hasOne('App\Models\Lancha', 'id', 'lancha_id');
    }

    public function relMunicipio()
    {
        return $this->hasOne('App\Models\Municipio', 'id', 'municipio_id');
    }

    public function relUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function relUserDelete()
    {
        return $this->hasOne('App\User', 'id', 'user_id_deleted');
    }

    public function pesquisar(Array $filtro)
    {
        $resultado = $this->where(function ($query) use ($filtro) {
            if (isset($filtro['id'])) {
                $query->where('id', 'LIKE', "%{$filtro['id']}%");
            }

            if (isset($filtro['id_encomenda'])) {
                $query->where('id_deletado', 'LIKE', "%{$filtro['id_encomenda']}%");
            }

            if (isset($filtro['destinatario'])) {
                $query->where('destinatario', 'LIKE', "%{$filtro['destinatario']}%");
            }
        })
        ->paginate();

        return $resultado;
    }
}
