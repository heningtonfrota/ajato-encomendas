<?php

use App\Http\Controllers\Admin\Configuracoes\ConfiguracoesController;
use App\Http\Controllers\Admin\Encomenda\ListaDespesaController;
use App\Http\Controllers\Admin\EncomendaRecebidaLancha\EncomendaRecebidaLanchaController;
use App\Http\Controllers\Admin\LanchaController;
use App\Http\Controllers\Admin\Relatorio\PorDestino\GeralController;
use App\Http\Controllers\Admin\Relatorio\PorDestino\IndividualController;
use App\Http\Controllers\Admin\Relatorio\TodosDestinosDataLancha\TodosDestinosDataLanchaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Admin\HomeController@index')->name('sistema.welcome');

Auth::routes();

Route::middleware('auth')->group(function() {
  Route::get('/home', 'Admin\HomeController@inicial')->name('sistema.home');
  Route::get('/sair', 'Admin\HomeController@logout')->name('sistema.sair');

  Route::prefix('encomendas_recebidas_lancha')->name('encomendas_recebidas_lancha.')->group(function() {
    Route::prefix('relatorio')->group(function() {
      Route::get('gerar-relatorio', [EncomendaRecebidaLanchaController::class, 'relatorioEncomendasGerarRelatorio'])->name('relatorio.gerar_relatorio');
      Route::get('alterar-status', [EncomendaRecebidaLanchaController::class, 'relatorioEncomendasAlterarStatus'])->name('relatorio.alterar_status');
      Route::get('lista', [EncomendaRecebidaLanchaController::class, 'relatorioEncomendasLista'])->name('relatorio.lista');
      Route::get('', [EncomendaRecebidaLanchaController::class, 'relatorioLanchaData'])->name('relatorio');
    });
    
    Route::get('imprimir-recibo/{id}', [EncomendaRecebidaLanchaController::class, 'imprimirRecibo'])->name('recibo');
  });

  Route::resource('encomendas_recebidas_lancha', 'Admin\EncomendaRecebidaLancha\EncomendaRecebidaLanchaController');
  Route::resource('lista_despesa', 'Admin\Relatorio\ListaDespesa\ListaDespesaController');
  
  Route::prefix('relatorio')->name('relatorio.')->group(function() {
    Route::get('por_lancha/gerar/{id}', 'Admin\Relatorio\PorLanchaController@gerarRelatorio')->name('por_lancha.gerar');
    Route::resource('por_lancha', 'Admin\Relatorio\PorLanchaController');
    
    Route::prefix('por_destino')->name('por_destino.')->group(function () {
      Route::get('individual/gerar/{id}', [IndividualController::class, 'gerarRelatorio'])->name('individual.gerar');
      Route::get('individual/lista_de_encomendas', [IndividualController::class, 'getListaDeEncomendas'])->name('individual.lista_de_encomendas');
      Route::resource('individual', 'Admin\Relatorio\PorDestino\IndividualController');
      
      Route::get('geral/gerar/{id}', [GeralController::class, 'gerarRelatorio'])->name('geral.gerar');
      Route::get('geral/lista_de_encomendas', [GeralController::class, 'getListaDeEncomendas'])->name('geral.lista_de_encomendas');
      Route::resource('geral', 'Admin\Relatorio\PorDestino\GeralController');
    });

    Route::prefix('todos-destinos-por-data-lancha')->group(function () {
      Route::get('gerar-relatorio/{data}/{lancha}', [TodosDestinosDataLanchaController::class, 'geraRelatorio']);
      // Route::get('gerar-relatorio/{data}/{lancha}', [TodosDestinosDataLanchaController::class, 'gerarPdf']);
    });
  });

  Route::get('lista-de-lanchas', [LanchaController::class, 'ListaDeLanchas']);
});


Route::prefix('configuracoes')->middleware(['configs'])->group(function ()
{
  Route::get('usuarios', [ConfiguracoesController::class, 'usuarios'])->name('sistema.configuracoes.usuarios');
  Route::get('usuarios/novo', [ConfiguracoesController::class, 'novoUsuario'])->name('sistema.configuracoes.novo_usuario');
  Route::get('usuarios/editar', [ConfiguracoesController::class, 'editarUsuario'])->name('sistema.configuracoes.editar_usuario');

  Route::get('lanchas', [ConfiguracoesController::class, 'lanchas'])->name('sistema.configuracoes.lanchas');
  Route::get('lanchas/{id_lancha}/alterar-status', [ConfiguracoesController::class, 'alterarStatusLancha'])->name('sistema.configuracoes.lancha.alterar_status');

  Route::get('municipios', [ConfiguracoesController::class, 'municipios'])->name('sistema.configuracoes.municipios');
  Route::get('municipios/{id_municipio}/alterar-status', [ConfiguracoesController::class, 'alterarStatusMunicipio'])->name('sistema.configuracoes.municipio.alterar_status');
});


Route::get('/encomenda/cadastro',                            'Admin\EncomendaController@create')->name('sistema.encomenda.cadastro');
Route::any('/encomenda/cadastro/pesquisar',                  'Admin\EncomendaController@pesquisarCadastro')->name('encomenda.cadastro');
Route::get('/encomenda/cadastro/auto',                       'Admin\EncomendaController@autocomplete')->name('autocomplete');
Route::post('/encomenda/lista',                              'Admin\EncomendaController@store')->name('encomenda.store');
Route::get('/encomenda/lista',                               'Admin\EncomendaController@index')->name('sistema.encomenda.lista');
Route::any('/encomenda/lista/pesquisar',                     'Admin\EncomendaController@pesquisarListaEncomenda')->name('encomenda.pesquisar');
Route::get('/encomenda/vizualizar/{id}',                     'Admin\EncomendaController@show')->name('sistema.encomenda.vizualizar');
Route::get('/encomenda/vizualizar/{id}/editar',              'Admin\EncomendaController@edit')->name('sistema.encomenda.editar');
Route::put('/encomenda/vizualizar/{id}',                     'Admin\EncomendaController@update')->name('sistema.encomenda.update');
Route::get('/encomenda/deletar/{id}',                        'Admin\EncomendaController@destroy')->name('sistema.encomenda.deletar');
Route::get('/encomenda/vizualizar-relatorio-individual',     'Admin\EncomendaController@relatorio')->name('sistema.encomenda.relatorio');
Route::post('/encomenda/vizualizar-relatorio-individual',    'Admin\EncomendaController@relatorioView')->name('encomenda.relatorioview');

Route::prefix('encomenda')->group(function () {
    Route::prefix('lista-despesas')->group(function () {
        Route::post('', [ListaDespesaController::class, 'store'])->name('encomenda.lista_despesas.store');
        Route::put('atualizar/{id}', [ListaDespesaController::class, 'update'])->name('encomenda.lista_despesas.update');
        Route::delete('deletar/{id}', [ListaDespesaController::class, 'destroy'])->name('encomenda.lista_despesas.destroy');
    });
});

Route::any('/encomendas-deletadas/lista/pesquisar',                     'Admin\EncomendaDeletadaController@pesquisarListaEncomenda')->name('encomenda_deletada.pesquisar');
Route::get('/encomendas-deletadas/deletar',                             'Admin\EncomendaDeletadaController@index')->name('sistema.encomendas_deletadas.index');
Route::get('/encomenda-deletadas/visualizar/{id}',                      'Admin\EncomendaDeletadaController@show')->name('sistema.encomendas_deletadas.visualizar');
Route::get('/encomenda-deletadas/retornar_encomenda/{id}',              'Admin\EncomendaDeletadaController@retornarEncomenda')->name('sistema.encomendas_deletadas.retornar_encomenda');
Route::get('/encomendas-deletadas/imprimir-recibo/{id}',                'Admin\EncomendaDeletadaController@imprimirRecibo')->name('sistema.encomendas_deletadas.recibo');
Route::get('/encomendas-deletadas/imprimir-cupom/{id}',                 'Admin\EncomendaDeletadaController@imprimirCupom')->name('sistema.encomendas_deletadas.cupom');

Route::get('/relatorio/valores-recebidos',                          'Admin\RelatorioController@relatorioValorRecebido')->name('sistema.relatorio.valores_recebidos_index');
Route::post('/relatorio/valores-recebidos',                         'Admin\RelatorioController@buscarRelatorioValorRecebido')->name('sistema.relatorio.valores_recebidos');

Route::get('/relatorio/destinos',                                   'Admin\RelatorioController@relatorioDestino')->name('sistema.relatorio.relatorio_destino');
Route::post('/relatorio/destinos',                                  'Admin\RelatorioController@relatorioDestinoView')->name('relatorio.relatorio_destino_view');
Route::get('/relatorio/destinos-todos',                             'Admin\RelatorioController@relatorioDestinoTodos')->name('sistema.relatorio.relatorio_destino_todos');
Route::post('/relatorio/destinos-todos',                            'Admin\RelatorioController@relatorioDestinoTodosView')->name('relatorio.relatorio_destino_todos_view');
Route::get('/relatorio',                                            'Admin\RelatorioController@index')->name('sistema.relatorio.index');
Route::get('/relatorio/historico',                                  'Admin\RelatorioController@historico')->name('sistema.relatorio.historico');
Route::get('/relatorio/vizualizar/{id}',                            'Admin\RelatorioController@show')->name('sistema.relatorio.vizualizar');
Route::get('/relatorio-todas-as-lanchas',                           'Admin\RelatorioController@relatorio')->name('sistema.relatorio.relatorio');
Route::post('/relatorio-todas-as-lanchas',                          'Admin\RelatorioController@relatorioView')->name('relatorio.todas_lancha');


Route::get('/encomenda/vizualizar-relatorio-individual/download-pdf', 'Admin\PdfController@relatorioIndividual')->name('pdf.relatorio_individual');
Route::get('/encomenda/vizualizar-relatorio-todas-as-lanchas/download-pdf', 'Admin\PdfController@todasAsLanchas')->name('pdf.todas_as_lanchas');
Route::get('/relatorios/destinos/imprimir-pdf', 'Admin\PdfController@imprimirDestinosPdf')->name('pdf.imprimir_destino_pdf');
Route::get('/relatorios/todos-os-destinos/imprimir-pdf', 'Admin\PdfController@imprimirTodosDestinosPdf')->name('pdf.imprimir_todos_destino_pdf');

Route::get('/encomendas/imprimir-etiqueta/{id}/{reset_id?}', 'Admin\EncomendaController@imprimirEtiqueta')->name('sistema.encomenda.etiqueta');
Route::get('/encomendas/imprimir-cupom/{id}/{reset_id?}', 'Admin\EncomendaController@imprimirCupom')->name('sistema.encomenda.cupom');
Route::get('/encomendas/imprimir-declaracao_conteudo/{id}/{reset_id?}', 'Admin\EncomendaController@imprimirDeclaracaoConteudo')->name('sistema.encomenda.declaracao_conteudo');
Route::get('/encomendas/imprimir-recibo/{id}/{reset_id?}', 'Admin\EncomendaController@imprimirRecibo')->name('sistema.encomenda.recibo');

Route::get('/lanchas', 'Admin\LanchaController@index')->name('sistema.lancha.index');

Route::get('/horario/ajato2000',        'Admin\LanchaController@ajato2000')->name('sistema.lancha.ajato2000');
Route::get('/horario/crystal',          'Admin\LanchaController@crystal')->name('sistema.lancha.crystal');
Route::get('/horario/gloria-de-deus',   'Admin\LanchaController@gloriaDeDeus')->name('sistema.lancha.gloria_de_deus');
Route::get('/horario/madame-cris',      'Admin\LanchaController@madameCris')->name('sistema.lancha.madame_cris');
Route::get('/horario/missone',          'Admin\LanchaController@missone')->name('sistema.lancha.missone');
Route::get('/horario/oriximina',        'Admin\LanchaController@oriximina')->name('sistema.lancha.oriximina');
Route::get('/horario/soberana',         'Admin\LanchaController@Soberana')->name('sistema.lancha.soberana');
Route::get('/horario/tais-holanda',     'Admin\LanchaController@taisHolanda')->name('sistema.lancha.tais_holanda');
Route::get('/horario/ze-holanda',       'Admin\LanchaController@zeHolanda')->name('sistema.lancha.ze_holanda');
Route::get('/horario/lima-de-abreu',    'Admin\LanchaController@limaDeAbreu')->name('sistema.lancha.lima_de_abreu');
Route::get('/horario/belissima',        'Admin\LanchaController@belissima')->name('sistema.lancha.belissima');
Route::get('/horario/perola',           'Admin\LanchaController@perola')->name('sistema.lancha.perola');

Route::get('/lanchas/encomenda/ajato2000',        'Admin\LanchaController@encomendaAjato2000')->name('sistema.lancha.encomenda.ajato2000');
Route::get('/lanchas/encomenda/crystal',          'Admin\LanchaController@encomendaCrystal')->name('sistema.lancha.encomenda.crystal');
Route::get('/lanchas/encomenda/gloria-de-deus',   'Admin\LanchaController@encomendaGloriaDeDeus')->name('sistema.lancha.encomenda.gloria_de_deus');
Route::get('/lanchas/encomenda/madame-cris',      'Admin\LanchaController@encomendaMadameCris')->name('sistema.lancha.encomenda.madame_cris');
Route::get('/lanchas/encomenda/missone',          'Admin\LanchaController@encomendaMissone')->name('sistema.lancha.encomenda.missone');
Route::get('/lanchas/encomenda/oriximina',        'Admin\LanchaController@encomendaOriximina')->name('sistema.lancha.encomenda.oriximina');
Route::get('/lanchas/encomenda/soberana',         'Admin\LanchaController@encomendaSoberana')->name('sistema.lancha.encomenda.soberana');
Route::get('/lanchas/encomenda/tais-holanda',     'Admin\LanchaController@encomendaTaisHolanda')->name('sistema.lancha.encomenda.tais_holanda');
Route::get('/lanchas/encomenda/ze-holanda',       'Admin\LanchaController@encomendaZeHolanda')->name('sistema.lancha.encomenda.ze_holanda');
Route::get('/horario/encomenda/lima-de-abreu',    'Admin\LanchaController@encomendaLimaDeAbreu')->name('sistema.lancha.encomenda.lima_de_abreu');
Route::get('/horario/encomenda/belissima',        'Admin\LanchaController@encomendaBelissima')->name('sistema.lancha.encomenda.belissima');
Route::get('/horario/encomenda/perola',           'Admin\LanchaController@encomendaPerola')->name('sistema.lancha.encomenda.perola');

Route::get('/reset-id',                           'Admin\ResetEncomendasController@index')->name('sistema.reset.index');
Route::get('/reset-id/reset-encomendas/',         'Admin\ResetEncomendasController@resetEncomendas')->name('sistema.reset.reset-encomenda');
Route::put('/reset-id/vizualizar/{id}',           'Admin\ResetEncomendasController@update')->name('sistema.reset.update');
Route::get('/reset-id/vizualizar/{id}',           'Admin\ResetEncomendasController@show')->name('sistema.reset.vizualizar');
Route::get('/reset/deletar/{id}',                 'Admin\ResetEncomendasController@destroy')->name('sistema.reset.deletar');
Route::any('/reset/lista/pesquisar',              'Admin\ResetEncomendasController@pesquisarListaReset')->name('reset.pesquisar');
