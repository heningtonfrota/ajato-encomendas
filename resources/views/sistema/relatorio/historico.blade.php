@extends('sistema.layout.index')

@section('titulo', 'Ajato Encomendas - Relatorio')

@section('conteudo')
<div class="container">
  <h1 class="text-center">Historico de Relatório Gerado</h1>
  <div class="d-flex justify-content-center">
    <table class="table table-sm table-bordered table-striped table-dark col-sm-6">
      <thead class="thead-dark">
        <tr class="text-center">
          <th width="5%">ID</th>
          <th>Nome</th>
          <th>Data</th>
          <th>Total Diário</th>
          <th width="15%">Ação</th>
        </tr>
      </thead>
      <tbody>
        @foreach($historico as $historicos)
          <tr class="text-center">
            <th scope="row">{{$historicos->id}}</th>
            <td>{{$historicos->nome}}</td>
            <td>{{\Carbon\Carbon::parse( $historicos->data )->format('d/m/Y')}}</td>
            <td>R$ {{ number_format( $historicos->total_diario, 2) }}</td>
            <td><a class="text-warning" href="{{route('sistema.relatorio.vizualizar', $historicos->id, $historicos->nome)}}"><i class="fas fa-eye"></i></a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="d-flex justify-content-center">
      {{ $historico->links() }}
  </div>
</div>
@endsection
