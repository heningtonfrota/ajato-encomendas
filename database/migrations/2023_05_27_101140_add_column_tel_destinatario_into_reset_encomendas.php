<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTelDestinatarioIntoResetEncomendas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reset_encomendas', function (Blueprint $table) {
            $table->string('tel_destinatario', 14)->nullable()->after('destinatario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reset_encomendas', function (Blueprint $table) {
            $table->dropColumn('tel_destinatario');
        });
    }
}
